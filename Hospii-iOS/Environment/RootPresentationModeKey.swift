//
//  RootPresentationModeKey.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import Foundation
import SwiftUI

struct RootPresentationModeKey: EnvironmentKey {
    static let defaultValue: Binding<RootPresentationMode> = .constant(RootPresentationMode())
}

extension EnvironmentValues {
    var rootPresentationMode: Binding<RootPresentationMode> {
        get { return self[RootPresentationModeKey.self] }
        set { self[RootPresentationModeKey.self] = newValue }
    }
}

typealias RootPresentationMode = Bool

public extension RootPresentationMode {
    mutating func dismiss() {
        toggle()
    }
}
