//
//  NotificationEvents.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/21/21.
//

import Foundation

enum NotificationEvents {
    case locationRequestClosed

    var event: String {
        switch self {
        case .locationRequestClosed: return "locationRequestClosed"
        }
    }
}
