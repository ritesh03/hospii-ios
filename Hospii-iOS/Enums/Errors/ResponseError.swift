//
//  ResponseError.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import CoreStore
import Foundation
import Moya

enum ResponseError: Error {
    case networkError(error: MoyaError)
    case dbError(error: CoreStoreError)
}
