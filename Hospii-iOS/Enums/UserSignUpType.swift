//
//  UserSignUpType.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import Foundation

enum UserSignUpTypes: CaseIterable {
    case user
    case hairstylist
}

extension UserSignUpTypes {
    var title: String {
        switch self {
        case .user:
            return "Need a hairstylist"
        case .hairstylist:
            return "I'm a hairstylist"
        }
    }
}
