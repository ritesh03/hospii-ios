//
//  ResultLayoutType.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/21/21.
//

import Foundation

enum ResultLayoutType: CaseIterable {
    case grid
    case list
}
