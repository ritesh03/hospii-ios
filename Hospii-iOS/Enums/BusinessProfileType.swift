//
//  BusinessProfileType.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Foundation

enum BusinessProfileType: CaseIterable {
    case about
    case gallery
}

extension BusinessProfileType {
    var title: String {
        switch self {
        case .about:
            return "About"
        case .gallery:
            return "Gallery"
        }
    }
}
