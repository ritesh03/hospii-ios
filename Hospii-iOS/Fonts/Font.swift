//
//  Font.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/13/21.
//

import Foundation
import SwiftUI
import UIKit

@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public enum CustomFont {
    static func proximaNovaBold(withSize size: CGFloat) -> Font {
        Font.custom("ProximaNova-Bold", size: size)
    }

    static func proximaNovaLight(withSize size: CGFloat) -> Font {
        Font.custom("ProximaNova-Light", size: size)
    }

    static func proximaNovaLightItalic(withSize size: CGFloat) -> Font {
        Font.custom("ProximaNova-LightIt", size: size)
    }

    static func proximaNovaSemibold(withSize size: CGFloat) -> Font {
        Font.custom("ProximaNova-Semibold", size: size)
    }

    static func proximaNovaRegular(withSize size: CGFloat) -> Font {
        Font.custom("ProximaNova-Regular", size: size)
    }

    static func proximaNovaRegularItalic(withSize size: CGFloat) -> Font {
        Font.custom("ProximaNova-RegularIt", size: size)
    }
}

@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
extension Font {
    static let primaryText: Font = CustomFont.proximaNovaRegular(withSize: 16)
    static let subText: Font = CustomFont.proximaNovaRegular(withSize: 14)
    static let sectionHeader: Font = CustomFont.proximaNovaBold(withSize: 32)
    static let sectionSubtitle: Font = CustomFont.proximaNovaRegular(withSize: 18)
    static let textFieldLabel: Font = CustomFont.proximaNovaSemibold(withSize: 14)
    static let textFieldErrorMessage: Font = CustomFont.proximaNovaRegular(withSize: 13)
    static let textFieldMessage: Font = CustomFont.proximaNovaSemibold(withSize: 16)
    static let actionButton: Font = CustomFont.proximaNovaBold(withSize: 16)
    static let tabViewTitle: Font = CustomFont.proximaNovaBold(withSize: 11)
    static let listSectionHeader: Font = CustomFont.proximaNovaBold(withSize: 16)
    static let subTextSemibold: Font = CustomFont.proximaNovaSemibold(withSize: 13)
    static let listSectionTitle: Font = CustomFont.proximaNovaSemibold(withSize: 18)
    static let navTitle: Font = CustomFont.proximaNovaSemibold(withSize: 20)
    static let alertBannerText: Font = CustomFont.proximaNovaRegular(withSize: 14)
    static let subTextBold: Font = CustomFont.proximaNovaBold(withSize: 14)
}

enum CustomViewStyle {
    case primaryText
    case subText
    case subTxtGray
    case sectionHeader
    case sectionSubtitle
    case textFieldLabel
    case textFieldErrorMessage
    case textFieldMessage
    case actionButton
    case tabViewTitle
    case listSectionHeader
    case subTextSemibold
    case listSectionTitle
    case navTitle
    case alertBannerText
    case subTextBold
}

extension View {
    @ViewBuilder func style(_ style: CustomViewStyle) -> some View {
        switch style {
        case .primaryText:
            font(.primaryText).foregroundColor(R.color.text()!.suColor)
        case .subText:
            font(.subText).foregroundColor(R.color.text()!.suColor)
        case .subTxtGray:
            font(.subText).foregroundColor(R.color.gray119()!.suColor)
        case .sectionHeader:
            font(.sectionHeader).foregroundColor(R.color.text()!.suColor)
        case .sectionSubtitle:
            font(.sectionSubtitle).foregroundColor(R.color.gray119()!.suColor)
        case .textFieldLabel:
            font(.textFieldLabel).foregroundColor(R.color.gray119()!.suColor)
        case .textFieldErrorMessage:
            font(.textFieldErrorMessage).foregroundColor(R.color.red()!.suColor)
        case .textFieldMessage:
            font(.textFieldMessage).foregroundColor(R.color.text()!.suColor)
        case .actionButton:
            font(.actionButton).foregroundColor(R.color.text()!.suColor)
        case .tabViewTitle:
            font(.tabViewTitle)
        case .listSectionHeader:
            font(.listSectionHeader).foregroundColor(R.color.text()!.suColor)
        case .subTextSemibold:
            font(.subTextSemibold).foregroundColor(R.color.text()!.suColor)
        case .listSectionTitle:
            font(.listSectionTitle).foregroundColor(R.color.text()!.suColor)
        case .navTitle:
            font(.navTitle).foregroundColor(R.color.text()!.suColor)
        case .alertBannerText:
            font(.subText).foregroundColor(R.color.textLight()!.suColor)
        case .subTextBold:
            font(.subTextBold).foregroundColor(R.color.text()!.suColor)
        }
    }
}
