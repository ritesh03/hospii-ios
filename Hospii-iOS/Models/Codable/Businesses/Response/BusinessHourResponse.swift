//
//  BusinessHourResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/12/21.
//

import Foundation

struct BusinessHourResponse: Codable {
    let weekDay: Int

    let openTime: String

    let closeTime: String

    let isClosed: Bool
}
