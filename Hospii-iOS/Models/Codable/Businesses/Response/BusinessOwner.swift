//
//  BusinessOwner.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/13/21.
//

import Foundation

struct BusinessOwner: Codable {
    let id: Int

    let username: String?

    let firstName: String?

    let lastName: String?

    let status: String

    let phoneNumber: String?

    let phoneNumberCountryCode: String?

    let isPhoneNumberVerified: Bool

    let email: String?

    let isEmailVerified: Bool

    let profilePhoto: PhotoResponse?

    let claimProfile: ClaimProfile?
}
