//
//  BusinessAddressResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/12/21.
//

import Foundation

struct BusinessAddressResponse: Codable {
    let address: String

    let address2: String?

    let city: String?

    let state: String?

    let code: String?

    let latitude: Double?

    let longitude: Double?
}
