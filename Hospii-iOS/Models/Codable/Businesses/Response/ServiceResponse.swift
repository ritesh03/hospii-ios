//
//  ServiceResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/6/21.
//

import Foundation

public struct ServiceResponse: Codable, Equatable {
    public static func == (lhs: ServiceResponse, rhs: ServiceResponse) -> Bool {
        lhs.id == rhs.id && lhs.group == rhs.group && lhs.service == rhs.service && lhs.icon == rhs.icon && lhs.isActive == rhs.isActive
    }

    let id: Int

    let group: String?

    let service: String?

    let icon: String?

    let isActive: Bool?

    var insertOrder: Int?
}

extension ServiceResponse {
    static func fromCoreData(service: ServiceMO) -> ServiceResponse {
        return .init(
            id: Int(service.key),
            group: service.group,
            service: service.service,
            icon: service.icon,
            isActive: service.isActive
        )
    }
}
