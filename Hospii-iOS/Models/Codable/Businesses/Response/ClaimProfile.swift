//
//  ClaimProfile.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 2/26/22.
//

import Foundation

struct ClaimProfile: Codable {
    let username: String?
}
