//
//  PhotoResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/12/21.
//

import Foundation

struct PhotoResponse: Codable {
    let imageUrl: String
}
