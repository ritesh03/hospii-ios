//
//  CreateBusinessResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import Foundation

struct CreateBusinessResponse: Codable {
    let id: Int

    let name: String?

    let about: String?

    let address: String?

    let address2: String?

    let city: String?

    let state: String?

    let code: String?

    let phoneNumber: String?

    let phoneNumberCountryCode: String?

    let phoneNumberExtension: String?
}
