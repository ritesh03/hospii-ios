//
//  UserBusinessResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/6/21.
//

import Foundation

public struct UserBusinessResponse: Codable, Equatable {
    public static func == (lhs: UserBusinessResponse, rhs: UserBusinessResponse) -> Bool {
        lhs.id == rhs.id
    }

    let id: Int

    let name: String

    let businessAbout: String?

    let type: String?

    let status: String?

    let phoneNumber: String?

    let phoneNumberExtension: String?

    let phoneNumberCountryCode: String?

    let isPhoneNumberVerified: Bool

    let coverPhoto: PhotoResponse?

    let userBusinessHours: [BusinessHourResponse]?

    let userAddress: BusinessAddressResponse

    let __serviceFilters__: [ServiceResponse]?

    let user: BusinessOwner?

    let distance: Double? // in km
}
