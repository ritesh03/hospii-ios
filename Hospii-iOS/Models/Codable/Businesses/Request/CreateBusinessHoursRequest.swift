//
//  CreateBusinessHoursRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import Foundation

struct CreateBusinessHoursRequest: Codable {
    let businessId: Int

    let hours: [BusinessHourRequest]
}
