//
//  UpdateActiveServicesRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/9/21.
//

import Foundation

struct UpdateActiveServicesRequest: Codable {
    let ids: [Int]
}
