//
//  BusinessHourRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import Foundation

struct BusinessHourRequest: Codable {
    let weekDay: Int

    let openTime: String

    let closeTime: String

    let isClosed: Bool

    let miscellaneous: String?
}
