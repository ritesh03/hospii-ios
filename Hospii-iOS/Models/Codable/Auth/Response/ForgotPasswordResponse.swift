//
//  ForgotPasswordResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import Foundation

struct ForgotPasswordResponse: Codable, Equatable {
    public static func == (lhs: ForgotPasswordResponse, rhs: ForgotPasswordResponse) -> Bool {
        lhs.token == rhs.token
    }

    let token: String
}
