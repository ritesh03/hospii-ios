//
//  Tokens.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import Foundation
struct Tokens: Codable {
    let tokenType: String

    let accessToken: String

    let refreshToken: String

    let expiresIn: Int

    enum CodingKeys: String, CodingKey {
        case refreshToken = "refresh_token"
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
    }
}
