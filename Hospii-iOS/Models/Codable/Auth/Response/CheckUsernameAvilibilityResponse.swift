//
//  CheckUsernameAvilibilityResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import Foundation

struct CheckUsernameAvilibilityResponse: Codable {
    let availability: Bool
}
