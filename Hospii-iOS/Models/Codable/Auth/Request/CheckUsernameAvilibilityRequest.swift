//
//  CheckUsernameAvilibilityRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import Foundation

struct CheckUsernameAvilibilityRequest: Codable {
    let username: String
}
