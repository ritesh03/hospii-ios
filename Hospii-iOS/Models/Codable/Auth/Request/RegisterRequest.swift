//
//  RegisterRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import Foundation

struct RegisterRequest: Codable {
    let firstName: String

    let lastName: String

    let username: String

    let email: String

    let password: String

    let countryCode: String

    let phoneNumber: String

    let isHairstylist: Bool
}
