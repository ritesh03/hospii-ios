//
//  PutPasswordRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import Foundation

struct PutPasswordRequest: Codable {
    var password: String = ""

    var newPassword: String = ""
}
