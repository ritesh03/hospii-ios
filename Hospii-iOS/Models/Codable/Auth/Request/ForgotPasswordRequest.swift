//
//  ForgotPasswordRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import Foundation

struct ForgotPasswordRequest: Codable {
    var code: String? = nil

    var email: String = ""

    var phoneNumber: String = ""
}
