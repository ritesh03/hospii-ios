//
//  ResetPasswordRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import Foundation

struct ResetPasswordRequest: Codable {
    var token: String? = nil

    var newPassword: String = ""
}
