//
//  LoginRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/17/21.
//

import Foundation

struct LoginRequest: Codable {
    let username: String

    let password: String

    let grantType: String
}
