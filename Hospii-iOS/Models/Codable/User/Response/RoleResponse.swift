//
//  RoleResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import Foundation

struct RoleResponse: Codable {
    let role: String
}
