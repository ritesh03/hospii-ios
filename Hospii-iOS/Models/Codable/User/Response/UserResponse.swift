//
//  User.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/3/21.
//

import Foundation

public struct UserResponse: Codable {
    let id: Int

    let username: String?

    let firstName: String?

    let lastName: String?

    let status: String

    let phoneNumber: String?

    let phoneNumberCountryCode: String?

    let isPhoneNumberVerified: Bool

    let email: String?

    let isEmailVerified: Bool

    let __roles__: [RoleResponse]

    let profilePhoto: MediaResponse?

    let __userBusinesses__: [UserBusinessResponse]?
}

extension UserResponse {
    static func fromCoreData(user: UserMO) -> UserResponse {
        return .init(
            id: Int(user.key),
            username: user.username,
            firstName: user.firstName,
            lastName: user.lastName,
            status: user.status ?? "",
            phoneNumber: user.phoneNumber,
            phoneNumberCountryCode: user.phoneNumberCountryCode,
            isPhoneNumberVerified: user.isPhoneNumberVerified,
            email: user.email,
            isEmailVerified: user.isEmailVerified,
            __roles__: user.getRoles() ?? [],
            profilePhoto: user.getPhoto(),
            __userBusinesses__: user.getUserBusinesses() ?? []
        )
    }
}
