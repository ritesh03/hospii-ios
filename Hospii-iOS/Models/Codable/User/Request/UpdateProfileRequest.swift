//
//  UpdateProfileRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import Foundation

struct UpdateProfileRequest: Codable {
    let firstName: String

    let lastName: String

    let email: String

    let countryCode: String

    let phoneNumber: String
}
