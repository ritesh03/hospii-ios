//
//  ErrorResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/1/21.
//

import Foundation

struct ErrorResponse: Codable {
    let statusCode: Int?
    let message: String?
    let error: String?
}
