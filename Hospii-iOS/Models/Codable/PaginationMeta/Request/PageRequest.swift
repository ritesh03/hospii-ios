//
//  PageRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/29/22.
//

import Foundation

struct PageRequest: Codable {
    var page: Int = 1

    var pageSize: Int? = 12

    var noMore: Bool = false
}
