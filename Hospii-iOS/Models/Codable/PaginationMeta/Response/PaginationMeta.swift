//
//  PaginationMeta.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/12/21.
//

import Foundation

struct PaginationMeta: Codable {
    let currentPage: Int

    let itemCount: Int
}
