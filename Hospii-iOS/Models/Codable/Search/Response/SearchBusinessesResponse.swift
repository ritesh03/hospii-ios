//
//  SearchBusinessesResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/21/21.
//

import Foundation

struct SearchBusinessesResponse: Codable, Equatable {
    public static func == (lhs: SearchBusinessesResponse, rhs: SearchBusinessesResponse) -> Bool {
        lhs.data == rhs.data
    }

    var data: [UserBusinessResponse]

    let meta: PaginationMeta
}
