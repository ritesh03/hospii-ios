//
//  SearchFeedsResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/11/21.
//

import Foundation

struct SearchFeedsResponse: Codable, Equatable {
    public static func == (lhs: SearchFeedsResponse, rhs: SearchFeedsResponse) -> Bool {
        lhs.data == rhs.data
    }

    var data: [PostResponse]

    let meta: PaginationMeta
}
