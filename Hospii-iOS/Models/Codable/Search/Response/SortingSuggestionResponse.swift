//
//  SortingSuggestionResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/3/22.
//

import Foundation

public struct SortingSuggestionResponse: Codable, Equatable {
    public static func == (lhs: SortingSuggestionResponse, rhs: SortingSuggestionResponse) -> Bool {
        lhs.suggestion == rhs.suggestion && lhs.displayName == rhs.displayName
    }

    let suggestion: String

    let displayName: String

    var insertOrder: Int?
}

extension SortingSuggestionResponse {
    static func fromCoreData(sortingSuggestion: SortingSuggestionMO) -> SortingSuggestionResponse {
        return .init(suggestion: sortingSuggestion.suggestion ?? "", displayName: sortingSuggestion.displayName ?? "", insertOrder: Int(sortingSuggestion.insertOrder))
    }
}
