//
//  SuggestionsResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/11/21.
//

import Foundation

struct SuggestionsResponse: Codable {
    let tags: [TagResponse]?

    let businesses: [UserBusinessResponse]?
}
