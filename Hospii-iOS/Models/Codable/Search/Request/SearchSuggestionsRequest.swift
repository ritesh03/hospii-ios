//
//  SearchSuggestionsRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/11/21.
//

import Foundation

struct SearchSuggestionsRequest: Codable {
    var keyword: String = ""

    var latitude: Double? = nil

    var longitude: Double? = nil

    var location: String? = nil
}
