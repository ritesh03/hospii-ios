//
//  SearchRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/11/21.
//

import Foundation

struct SearchRequest: Codable, Equatable {
    public static func == (lhs: SearchRequest, rhs: SearchRequest) -> Bool {
        lhs.tags == rhs.tags &&
            lhs.services == rhs.services &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude &&
            lhs.page == rhs.page &&
            lhs.noMore == rhs.noMore &&
            lhs.location == rhs.location &&
            lhs.selectedItemId == rhs.selectedItemId &&
            lhs.range == rhs.range
    }

    var tags: [String] = []

    var services: Set<String> = []

    var sort: String? = nil

    var latitude: Double? = nil

    var longitude: Double? = nil

    var page: Int = 1

    var noMore: Bool = false

    var location: String? = nil

    var selectedItemId: Int = -1

    var range: Double = 10
}
