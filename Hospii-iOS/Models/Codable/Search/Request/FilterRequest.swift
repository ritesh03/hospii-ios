//
//  FilterRequest.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/26/21.
//

import Foundation

struct FilterRequest {
    var range: Double = 10

    var selectedServices: [ServiceResponse] = []
}
