//
//  PostsResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/13/22.
//

import Foundation

struct PostsResponse: Codable, Equatable {
    public static func == (lhs: PostsResponse, rhs: PostsResponse) -> Bool {
        lhs.data == rhs.data
    }

    var data: [PostResponse]

    let meta: PaginationMeta
}
