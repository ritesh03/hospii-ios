//
//  MediasResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/20/21.
//

import Foundation

struct MediasResponse: Codable, Equatable {
    public static func == (lhs: MediasResponse, rhs: MediasResponse) -> Bool {
        lhs.data == rhs.data
    }

    var data: [MediaResponse]

    let meta: PaginationMeta
}
