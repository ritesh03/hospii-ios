//
//  MediaResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/5/21.
//

import Foundation

public struct MediaResponse: Codable, Equatable {
    public static func == (lhs: MediaResponse, rhs: MediaResponse) -> Bool {
        lhs.id == rhs.id && lhs.imageUrl == rhs.imageUrl && lhs.createdAt == rhs.createdAt
    }

    let id: Int

    let imageUrl: String

    let createdAt: String?

    let width: Double

    let height: Double
}

extension MediaResponse {
    static func fromCoreData(media: MediaMO) -> MediaResponse {
        return .init(
            id: Int(media.key),
            imageUrl: media.imageUrl ?? "",
            createdAt: media.createdAt,
            width: media.width,
            height: media.height
        )
    }
}
