//
//  PostResponse.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/13/22.
//

import Foundation

public struct PostResponse: Codable, Equatable {
    public static func == (lhs: PostResponse, rhs: PostResponse) -> Bool {
        lhs.id == rhs.id &&
            lhs.caption == rhs.caption &&
            lhs.about == rhs.about &&
            lhs.createdAt == rhs.createdAt &&
            lhs.__medias__ == rhs.__medias__
    }

    let id: Int

    let caption: String?

    let about: String?

    let business: UserBusinessResponse?

    let createdAt: String?

    let __medias__: [MediaResponse]?
}

extension PostResponse {
    static func fromCoreData(post: PostMO) -> PostResponse {
        return .init(id: Int(post.key),
                     caption: post.caption,
                     about: post.about,
                     business: post.getBusiness(),
                     createdAt: post.createdAt,
                     __medias__: post.getMedias())
    }
}
