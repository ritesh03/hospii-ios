//
//  GetPostsReqeust.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 2/4/22.
//

import Foundation

struct GetPostsRequest: Codable, Equatable {
    public static func == (lhs: GetPostsRequest, rhs: GetPostsRequest) -> Bool {
        lhs.page == rhs.page &&
            lhs.noMore == rhs.noMore &&
            lhs.selectedItemId == rhs.selectedItemId &&
            lhs.business == rhs.business
    }

    var business: UserBusinessResponse

    var page = 1

    var noMore: Bool = false

    var selectedItemId: Int = -1
}
