//
//  ServiceMO+CoreDataClass.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/6/21.
//
//

import CoreData
import CoreStore

@objc(ServiceMO)
public class ServiceMO: NSManagedObject, ImportableUniqueObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ServiceMO> {
        return NSFetchRequest<ServiceMO>(entityName: "ServiceMO")
    }

    @NSManaged public var key: Int64
    @NSManaged public var group: String?
    @NSManaged public var service: String?
    @NSManaged public var icon: String?
    @NSManaged public var isActive: Bool

    public typealias ImportSource = ServiceResponse
    public typealias UniqueIDType = Int64

    public class var uniqueIDKeyPath: String {
        return #keyPath(ServiceMO.key)
    }

    public var uniqueIDValue: Int64 {
        get { return key }
        set { key = newValue }
    }

    public class func uniqueID(from source: ImportSource, in _: BaseDataTransaction) throws -> Int64? {
        return Int64(source.id)
    }

    public func update(from source: ImportSource, in _: BaseDataTransaction) throws {
        key = Int64(source.id)
        group = source.group
        service = source.service
        icon = source.icon
        if let isActive = source.isActive {
            self.isActive = isActive
        }
    }

    public static func shouldUpdate(from _: ImportSource, in _: BaseDataTransaction) -> Bool {
        return true
    }
}

extension ServiceMO: Identifiable {}
