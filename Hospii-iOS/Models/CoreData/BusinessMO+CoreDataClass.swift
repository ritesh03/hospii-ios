//
//  BusinessMO+CoreDataClass.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/12/21.
//
//

import CoreData
import CoreStore
import Foundation

@objc(BusinessMO)
public class BusinessMO: NSManagedObject, ImportableUniqueObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<BusinessMO> {
        return NSFetchRequest<BusinessMO>(entityName: "BusinessMO")
    }

    @NSManaged public var key: Int64
    @NSManaged public var name: String?
    @NSManaged public var businessAbout: String?
    @NSManaged public var type: String?
    @NSManaged public var status: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var phoneNumberExtension: String?
    @NSManaged public var phoneNumberCountryCode: String?
    @NSManaged public var isPhoneNumberVerified: Bool
    @NSManaged public var userBusinessHours: String?
    @NSManaged public var userAddress: String?
    @NSManaged public var serviceFilters: String?
    @NSManaged public var coverPhoto: String?

    fileprivate lazy var encoder = JSONEncoder()
    fileprivate lazy var jsonDecoder = JSONDecoder()

    public typealias ImportSource = UserBusinessResponse
    public typealias UniqueIDType = Int64

    public class var uniqueIDKeyPath: String {
        return #keyPath(BusinessMO.key)
    }

    public var uniqueIDValue: Int64 {
        get { return key }
        set { key = newValue }
    }

    public class func uniqueID(from source: ImportSource, in _: BaseDataTransaction) throws -> Int64? {
        return Int64(source.id)
    }

    public func update(from source: ImportSource, in _: BaseDataTransaction) throws {
        key = Int64(source.id)
        name = source.name
        businessAbout = source.businessAbout
        type = source.type
        status = source.status
        phoneNumber = source.phoneNumber
        phoneNumberExtension = source.phoneNumberExtension
        phoneNumberCountryCode = source.phoneNumberCountryCode
        isPhoneNumberVerified = source.isPhoneNumberVerified
        addBusinessHours(source.userBusinessHours ?? [])
        addAddress(source.userAddress)
        addServiceFilters(source.__serviceFilters__ ?? [])
        addPhoto(source.coverPhoto)
    }

    public static func shouldUpdate(from _: ImportSource, in _: BaseDataTransaction) -> Bool {
        return true
    }

    func addBusinessHours(_ request: [BusinessHourResponse]) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                userBusinessHours = jsonString
            }
        } catch {}
    }

    func getBusinessHours() -> [BusinessHourResponse]? {
        do {
            guard let userBusinessHours = userBusinessHours, let data = userBusinessHours.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode([BusinessHourResponse].self, from: data)
            return result
        } catch {
            return nil
        }
    }

    func addAddress(_ request: BusinessAddressResponse) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                userAddress = jsonString
            }
        } catch {}
    }

    func getAddress() -> BusinessAddressResponse? {
        do {
            guard let userAddress = userAddress, let data = userAddress.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode(BusinessAddressResponse.self, from: data)
            return result
        } catch {
            return nil
        }
    }

    func addServiceFilters(_ request: [ServiceResponse]) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                serviceFilters = jsonString
            }
        } catch {}
    }

    func getServiceFilters() -> [ServiceResponse]? {
        do {
            guard let serviceFilters = serviceFilters, let data = serviceFilters.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode([ServiceResponse].self, from: data)
            return result
        } catch {
            return nil
        }
    }

    func addPhoto(_ request: PhotoResponse?) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                coverPhoto = jsonString
            }
        } catch {}
    }

    func getPhoto() -> PhotoResponse? {
        do {
            guard let coverPhoto = coverPhoto, let profilePhotoData = coverPhoto.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode(PhotoResponse.self, from: profilePhotoData)
            return result
        } catch {
            return nil
        }
    }
}

extension BusinessMO: Identifiable {}
