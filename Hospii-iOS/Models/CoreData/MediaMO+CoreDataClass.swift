//
//  MediaMO+CoreDataClass.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/12/21.
//
//

import CoreData
import CoreStore
import Foundation

@objc(MediaMO)
public class MediaMO: NSManagedObject, ImportableUniqueObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<MediaMO> {
        return NSFetchRequest<MediaMO>(entityName: "MediaMO")
    }

    @NSManaged public var key: Int64
    @NSManaged public var imageUrl: String?
    @NSManaged public var createdAt: String?
    @NSManaged public var width: Double
    @NSManaged public var height: Double

    fileprivate lazy var encoder = JSONEncoder()
    fileprivate lazy var jsonDecoder = JSONDecoder()

    public typealias ImportSource = MediaResponse
    public typealias UniqueIDType = Int64

    public class var uniqueIDKeyPath: String {
        return #keyPath(MediaMO.key)
    }

    public var uniqueIDValue: Int64 {
        get { return key }
        set { key = newValue }
    }

    public class func uniqueID(from source: ImportSource, in _: BaseDataTransaction) throws -> Int64? {
        return Int64(source.id)
    }

    public func update(from source: ImportSource, in _: BaseDataTransaction) throws {
        key = Int64(source.id)
        imageUrl = source.imageUrl
        createdAt = source.createdAt
        width = source.width
        height = source.height
    }

    public static func shouldUpdate(from _: ImportSource, in _: BaseDataTransaction) -> Bool {
        return true
    }
}

extension MediaMO: Identifiable {}
