//
//  PostMO+CoreDataClass.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/13/22.
//
//

import CoreData
import CoreStore
import Foundation

@objc(PostMO)
public class PostMO: NSManagedObject, ImportableUniqueObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<PostMO> {
        return NSFetchRequest<PostMO>(entityName: "PostMO")
    }

    @NSManaged public var key: Int64
    @NSManaged public var caption: String?
    @NSManaged public var about: String?
    @NSManaged public var business: String?
    @NSManaged public var createdAt: String?
    @NSManaged public var medias: String?

    fileprivate lazy var encoder = JSONEncoder()
    fileprivate lazy var jsonDecoder = JSONDecoder()

    public typealias ImportSource = PostResponse
    public typealias UniqueIDType = Int64

    public class var uniqueIDKeyPath: String {
        return #keyPath(PostMO.key)
    }

    public var uniqueIDValue: Int64 {
        get { return key }
        set { key = newValue }
    }

    public class func uniqueID(from source: ImportSource, in _: BaseDataTransaction) throws -> Int64? {
        return Int64(source.id)
    }

    public func update(from source: ImportSource, in _: BaseDataTransaction) throws {
        key = Int64(source.id)
        createdAt = source.createdAt
        caption = source.caption
        addBusiness(source.business)
        addMedias(source.__medias__)
    }

    public static func shouldUpdate(from _: ImportSource, in _: BaseDataTransaction) -> Bool {
        return true
    }

    func addBusiness(_ request: UserBusinessResponse?) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                business = jsonString
            }
        } catch {}
    }

    func getBusiness() -> UserBusinessResponse? {
        do {
            guard let business = business, let businessData = business.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode(UserBusinessResponse.self, from: businessData)
            return result
        } catch {
            return nil
        }
    }

    func addMedias(_ request: [MediaResponse]?) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                medias = jsonString
            }
        } catch {}
    }

    func getMedias() -> [MediaResponse]? {
        do {
            guard let medias = medias, let data = medias.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode([MediaResponse].self, from: data)
            return result
        } catch {
            return nil
        }
    }
}

extension PostMO: Identifiable {}
