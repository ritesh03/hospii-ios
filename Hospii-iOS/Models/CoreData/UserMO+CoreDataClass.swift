//
//  UserMO+CoreDataClass.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//
//

import CoreData
import CoreStore

@objc(UserMO)
public class UserMO: NSManagedObject, ImportableUniqueObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserMO> {
        return NSFetchRequest<UserMO>(entityName: "UserMO")
    }

    @NSManaged public var key: Int64

    @NSManaged public var username: String?

    @NSManaged public var firstName: String?

    @NSManaged public var lastName: String?

    @NSManaged public var status: String?

    @NSManaged public var phoneNumber: String?

    @NSManaged public var phoneNumberCountryCode: String?

    @NSManaged public var isPhoneNumberVerified: Bool

    @NSManaged public var email: String?

    @NSManaged public var isEmailVerified: Bool

    @NSManaged public var roles: String?

    @NSManaged public var isProfessional: Bool

    @NSManaged public var profilePhoto: String?

    @NSManaged public var hasBusiness: Bool

    @NSManaged public var userBusinesses: String?

    fileprivate lazy var encoder = JSONEncoder()
    fileprivate lazy var jsonDecoder = JSONDecoder()

    public typealias ImportSource = UserResponse
    public typealias UniqueIDType = Int64

    public class var uniqueIDKeyPath: String {
        return #keyPath(UserMO.key)
    }

    public var uniqueIDValue: Int64 {
        get { return key }
        set { key = newValue }
    }

    public class func uniqueID(from source: ImportSource, in _: BaseDataTransaction) throws -> Int64? {
        return Int64(source.id)
    }

    public func update(from source: ImportSource, in _: BaseDataTransaction) throws {
        key = Int64(source.id)
        username = source.username
        firstName = source.firstName
        lastName = source.lastName
        status = source.status
        phoneNumber = source.phoneNumber
        phoneNumberCountryCode = source.phoneNumberCountryCode
        isPhoneNumberVerified = source.isPhoneNumberVerified
        email = source.email
        isEmailVerified = source.isEmailVerified
        addRoles(source.__roles__)
        isProfessional = source.__roles__.first { $0.role == "professional" } != nil
        addPhoto(source.profilePhoto)
        hasBusiness = !(source.__userBusinesses__ ?? []).isEmpty
        addUserBusinesses(source.__userBusinesses__ ?? [])
    }

    public static func shouldUpdate(from _: ImportSource, in _: BaseDataTransaction) -> Bool {
        return true
    }

    func addRoles(_ request: [RoleResponse]) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                roles = jsonString
            }
        } catch {}
    }

    func getRoles() -> [RoleResponse]? {
        do {
            guard let roles = roles, let rolesData = roles.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode([RoleResponse].self, from: rolesData)
            return result
        } catch {
            return nil
        }
    }

    func addPhoto(_ request: MediaResponse?) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                profilePhoto = jsonString
            }
        } catch {}
    }

    func getPhoto() -> MediaResponse? {
        do {
            guard let profilePhoto = profilePhoto, let profilePhotoData = profilePhoto.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode(MediaResponse.self, from: profilePhotoData)
            return result
        } catch {
            return nil
        }
    }

    func addUserBusinesses(_ request: [UserBusinessResponse]) {
        do { let jsonData = try encoder.encode(request)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                userBusinesses = jsonString
            }
        } catch {}
    }

    func getUserBusinesses() -> [UserBusinessResponse]? {
        do {
            guard let userBusinesses = userBusinesses, let data = userBusinesses.data(using: .utf8) else {
                return nil
            }
            let result = try jsonDecoder.decode([UserBusinessResponse].self, from: data)
            return result
        } catch {
            return nil
        }
    }
}

extension UserMO: Identifiable {}
