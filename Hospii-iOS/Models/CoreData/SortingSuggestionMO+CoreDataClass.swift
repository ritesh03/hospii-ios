//
//  SortingSuggestionMO+CoreDataClass.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/3/22.
//
//

import CoreData
import CoreStore
import Foundation

@objc(SortingSuggestionMO)
public class SortingSuggestionMO: NSManagedObject, ImportableUniqueObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<SortingSuggestionMO> {
        return NSFetchRequest<SortingSuggestionMO>(entityName: "SortingSuggestionMO")
    }

    @NSManaged public var suggestion: String?
    @NSManaged public var displayName: String?
    @NSManaged public var insertOrder: Int64

    fileprivate lazy var encoder = JSONEncoder()
    fileprivate lazy var jsonDecoder = JSONDecoder()

    public typealias ImportSource = SortingSuggestionResponse
    public typealias UniqueIDType = String

    public class var uniqueIDKeyPath: String {
        return #keyPath(SortingSuggestionMO.suggestion)
    }

    public var uniqueIDValue: String {
        get { return suggestion ?? "" }
        set { suggestion = newValue }
    }

    public class func uniqueID(from source: ImportSource, in _: BaseDataTransaction) throws -> String? {
        return source.suggestion
    }

    public func update(from source: ImportSource, in _: BaseDataTransaction) throws {
        suggestion = source.suggestion
        displayName = source.displayName
        insertOrder = Int64(source.insertOrder ?? 0)
    }

    public static func shouldUpdate(from _: ImportSource, in _: BaseDataTransaction) -> Bool {
        return true
    }
}

extension SortingSuggestionMO: Identifiable {}
