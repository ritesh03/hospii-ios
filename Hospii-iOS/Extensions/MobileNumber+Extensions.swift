//
// Copyright © Hospii Inc. All rights reserved.
//

import Foundation

typealias MobileNumber = String

enum DialingCode: String {
    case us = "1"
}

extension MobileNumber {
    var sanitize: String {
        components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }

    func addDialingCode(dialingCode: DialingCode) -> MobileNumber {
        "+" + dialingCode.rawValue + sanitize // The + symbol is needed
    }

    func removeDialingCode(dialingCode: DialingCode) -> MobileNumber {
        replacingOccurrences(of: "+" + dialingCode.rawValue, with: "")
    }

    func mobileFormat(dialingCode: DialingCode) -> MobileNumber {
        let cleanPhoneNumber = removeDialingCode(dialingCode: dialingCode)
        let mask = "(XXX) XXX - XXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
}
