//
//  Bundle+Extensions.swift
//  Hospii-iOS
//
//  Created by Chesong Lee on 1/27/22.
//

import Foundation

extension Bundle {
    var appVersion: String? {
        let key = "CFBundleShortVersionString"
        let possibleVersion = infoDictionary?[key] as? String
        var possibleSHA = ""

        #if STAGING
            // The sha is added via a build script.
            let shaKey = "CFBundleVersion"
            possibleSHA = "("
            possibleSHA += infoDictionary?[shaKey] as? String ?? ""
            possibleSHA += ")"
        #endif

        return (possibleVersion ?? "") + possibleSHA
    }
}
