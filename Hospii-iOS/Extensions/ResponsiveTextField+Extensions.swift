//
//  ResponsiveTextField+Extensions.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/23/21.
//

import Combine
import Foundation
import ResponsiveTextField
import SwiftUI
import UIKit

public extension ResponsiveTextField.Configuration {
    static let noCorrection = Self {
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.spellCheckingType = .no
    }

    static let emailField = Self {
        $0.keyboardType = .emailAddress
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.spellCheckingType = .no
        $0.clearButtonMode = .whileEditing
    }

    static let phoneNumberVerification = Self {
        $0.keyboardType = .numberPad
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.spellCheckingType = .no
    }

    static let words = Self {
        $0.autocapitalizationType = .words
        $0.autocorrectionType = .no
        $0.spellCheckingType = .no
    }
}
