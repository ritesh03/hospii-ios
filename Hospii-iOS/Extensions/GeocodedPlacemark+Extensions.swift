//
//  GeocodedPlacemark+Extensions.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/20/21.
//

import Foundation
import MapboxGeocoder

extension GeocodedPlacemark {
    func formattedAddress(compact: Bool = false) -> String {
        let streetAddress = formattedName
        let city = postalAddress?.city ?? ""
        let state = postalAddress?.state ?? ""
        let zipCode = postalAddress?.postalCode ?? ""
        let formattedAddress = "\(streetAddress)\(compact ? " " : "\n")\(city) \(state), \(zipCode)"
        return formattedAddress
    }
}
