//
// Copyright © Hospii Inc. All rights reserved.
//

import Foundation

extension String {
    func validateText(type: ValidationType) -> Error? {
        switch type {
        case .firstName, .lastName:
            let validator = NameValidation()
            return validator.validate(self)
        case .email:
            let validator = EmailValidation()
            return validator.validate(self)
        case .phoneNumber:
            let validator = PhoneNumberValidation()
            return validator.validate(self)
        case .password:
            let validator = PasswordValidation()
            return validator.validate(self)
        case .username:
            let validator = UsernameValidation()
            return validator.validate(self)
        case .oneTimeCode:
            let validator = OneTimeCodeValidation()
            return validator.validate(self)
        case .businessName:
            let validator = BusinessNameValidation()
            return validator.validate(self)
        case .businessAddress:
            let validator = BusinessAddressValidation()
            return validator.validate(self)
        case .businessPhoneNumber:
            let validator = PhoneNumberValidation()
            return validator.validate(self)
        }
    }

    var trailingSpacesTrimmed: String {
        var newString = self

        while newString.last?.isWhitespace == true {
            newString = String(newString.dropLast())
        }

        return newString
    }

    var boolValue: Bool {
        (self as NSString).boolValue
    }

    // Assuming all phone number is 10 digits without dialing code
    // This also works because we only support US numbers
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == count && count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }

    var isNumeric: Bool { CharacterSet(charactersIn: self).isSubset(of: CharacterSet.decimalDigits) }

    // Supports different numbers format (Chinese, Hindi, Arabic, etc)
    var isDigits: Bool {
        guard !isEmpty else { return false }
        let containsNonNumbers = contains { !$0.isNumber }
        return !containsNonNumbers
    }
}
