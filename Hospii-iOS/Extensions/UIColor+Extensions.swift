//
//  UIColorEx.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import SwiftUI
import UIKit

extension UIColor {
    /// The SwiftUI color associated with the receiver.
    var suColor: Color { Color(self) }
}
