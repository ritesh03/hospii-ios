//
//  Double+Extensions.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/16/21.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func roundedString(toPlaces places: Int) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = places
        return formatter.string(from: NSNumber(value: self))
    }
}
