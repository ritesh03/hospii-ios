//
//  UIImageEx.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import SwiftUI
import UIKit

extension UIImage {
    /// The SwiftUI color associated with the receiver.
    var suImage: Image { Image(uiImage: self) }
}
