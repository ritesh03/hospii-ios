//
//  PhoneNumber+Extensions.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/22/21.
//

import Foundation
import PhoneNumberKit

#if os(iOS)
    import UIKit

    public extension PhoneNumber {
        /**
         Calls the phone number.
         */
        func call() {
            guard let url = URL(string: "tel://" + numberString) else { return }
            UIApplication.shared.open(url)
        }
    }

#elseif os(macOS)
    import AppKit

    public extension PhoneNumber {
        /**
         Calls the phone number.
         */
        func call() {
            guard let url = URL(string: "tel://" + numberString) else { return }
            NSWorkspace.shared.open(url)
        }
    }
#endif
