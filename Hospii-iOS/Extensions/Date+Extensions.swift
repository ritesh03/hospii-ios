//
//  Date+Extensions.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/22/21.
//

import Foundation

extension Date {
    func dayNumberOfWeek() -> Int? {
        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
}
