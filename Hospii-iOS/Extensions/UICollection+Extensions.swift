//
//  UICollection+Extensions.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/1/21.
//

import Foundation
import UIKit

extension Collection {
    /// Get at index object
    ///
    /// - Parameter index: Index of object
    /// - Returns: Element at index or nil
    func get(_ index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Array {
    func group<U: Hashable>(by key: (Element) -> U) -> [[Element]] {
        // keeps track of what the integer index is per group item
        var indexKeys = [U: Int]()

        var grouped = [[Element]]()
        for element in self {
            let key = key(element)

            if let ind = indexKeys[key] {
                grouped[ind].append(element)
            } else {
                grouped.append([element])
                indexKeys[key] = grouped.count - 1
            }
        }
        return grouped
    }
}
