//
//  SericeFiltersViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/6/21.
//

import Moya
import SwiftUI
import Valet

// MARK: - ServiceFiltersViewModel

extension ServiceFiltersView {
    class ViewModel: ObservableObject {
        // State
        @Published var servicesResponse: Loadable<[ServiceResponse]>

        @Published var selection: Set<Int> = []

        @Published var updateServicesResponse: Loadable<Response>

        @Published var isPresentedSuccessAlert: Bool = false

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer,
             servicesResponse: Loadable<[ServiceResponse]> = .notRequested,
             updateServicesResponse: Loadable<Response> = .notRequested)
        {
            self.container = container
            _updateServicesResponse = .init(initialValue: updateServicesResponse)
            _servicesResponse = .init(initialValue: servicesResponse)

            cancelBag.collect {
                $updateServicesResponse
                    .sink { [weak self] response in
                        switch response {
                        case .loaded:
                            self?.isPresentedSuccessAlert = true
                        default:
                            break
                        }
                    }

                $servicesResponse
                    .sink { [weak self] response in
                        switch response {
                        case let .loaded(services):
                            for service in services.filter({ $0.isActive == true }) {
                                self?.selection.insert(service.id)
                            }
                        default:
                            break
                        }
                    }
            }
        }

        func loadServicesFilters() {
            container.services.businessesService.getActiveServices(response: loadableSubject(\.servicesResponse))
        }

        func updateServiceFilters() {
            container.services.businessesService.updateActiveServices(response: loadableSubject(\.updateServicesResponse), request: UpdateActiveServicesRequest(ids: Array(selection)))
        }

        func selectFilter(isChecked: Bool, id: Int) {
            if isChecked {
                selection.insert(id)
            } else {
                selection.remove(id)
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
