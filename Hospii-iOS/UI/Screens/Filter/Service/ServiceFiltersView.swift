//
//  ServiceFiltersView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import Introspect
import SwiftUI

struct ServiceFiltersView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    R.image.back()!.suImage
                }
                .padding(.top, 20)
                .padding(.horizontal, 20)

                Spacer()
            }

            content

            Spacer()
        }
        .onLoad {
            viewModel.loadServicesFilters()
        }
        .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.5, tapToDismiss: true, offsetY: 60) {
            AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.signUpUserProfileViewAlertTitle())
        }
    }

    private var content: AnyView {
        switch viewModel.servicesResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last))
        case let .loaded(services): return AnyView(loadedView(services, showLoading: false))
        case let .cache(services): return AnyView(loadedView(services, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Loading Content

private extension ServiceFiltersView {
    var notRequestedView: some View {
        EmptyView()
    }

    func loadingView(_ previouslyLoaded: [ServiceResponse]?) -> some View {
        if let services = previouslyLoaded {
            return AnyView(loadedView(services, showLoading: true))
        } else {
            return AnyView(
                HStack {
                    Spacer()

                    ActivityIndicatorView().padding(32)

                    Spacer()
                }
            )
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.loadServicesFilters()
        })
    }
}

// MARK: - Displaying Content

private extension ServiceFiltersView {
    func loadedView(_ services: [ServiceResponse], showLoading _: Bool) -> some View {
        let groupedServices = services.group(by: { $0.group ?? "" })
//        var groupedServices1 = [[ServiceResponse]]()
//        for service in groupedServices {
//            let services = service.filter { response in
//                response.group != "Price range"
//            }
//            if services.count > 0 {
//                groupedServices1.append(services)
//            }
//        }
        
        return VStack(alignment: .leading) {
            ScrollView {
                LazyVStack(alignment: .leading) {
                    ForEach(groupedServices.indices) { index in
                        Section(header: Text(groupedServices.get(index)?.first?.group ?? "").style(.listSectionTitle)) {
                            ForEach(groupedServices[index], id: \.id) { service in
                                FilterItem(title: service.service ?? "", image: service.icon ?? "", isChecked: viewModel.selection.contains(service.id)) { isChecked in
                                    viewModel.selectFilter(isChecked: isChecked, id: service.id)
                                }
                                .padding(.top, 2)
                            }
                        }
                        .padding(.top)
                        .padding(.horizontal)
                    }
                }
                .padding(.top, 0)
            }

            RoundedButton(style: .primaryButton, isLoading: viewModel.updateServicesResponse.isLoading, title: R.string.localizable.signUpBusinessProfileViewUpdateButtonTitle()) {
                viewModel.updateServiceFilters()
            }
            .padding(.horizontal)
            .disabled(viewModel.updateServicesResponse.isLoaded)
        }
    }
}

struct ServiceFiltersView_Previews: PreviewProvider {
    static var previews: some View {
        ServiceFiltersView(viewModel: .init(container: .preview))
    }
}
