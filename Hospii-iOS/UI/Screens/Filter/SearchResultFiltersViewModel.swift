//
//  SearchResultFiltersViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/21/21.
//

import Moya
import SwiftUI
import Valet

struct SearchResultFilters {
    var openNow: Bool = false
}

// MARK: - ServiceFiltersViewModel

extension SearchResultFiltersView {
    class ViewModel: ObservableObject {
        // State
        @Published var servicesResponse: Loadable<[ServiceResponse]>

        @Published var searchResultFilters = SearchResultFilters()

        @Published var searchRequest: SearchRequest

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer
        let minimumValue = 1
        let maximumvalue = 20

        init(container: DIContainer) {
            self.container = container
            let appState = container.appState

            _searchRequest = .init(initialValue: appState.value.userData.searchRequest)
            _servicesResponse = .init(initialValue: appState[\.userData.services])

            cancelBag.collect {
                $servicesResponse
                    .sink { [weak self] response in
                        switch response {
                        case let .loaded(services):
                            for service in services.filter({ $0.isActive == true }) {
                                self?.searchRequest.services.insert(service.service ?? "")
                            }
                        default:
                            break
                        }
                    }

                $searchRequest
                    .sink { appState[\.userData.searchRequest] = $0 }

                appState.map(\.userData.searchRequest)
                    .removeDuplicates()
                    .weakAssign(to: \.searchRequest, on: self)

                $servicesResponse
                    .filter { $0.isLoaded }
                    .sink { appState[\.userData.services] = $0 }

                appState.map(\.userData.services)
                    .removeDuplicates()
                    .weakAssign(to: \.servicesResponse, on: self)
            }
        }

        func loadServicesFilters() {
            container.services.businessesService.getServices(response: loadableSubject(\.servicesResponse))
        }

        func selectFilter(isChecked: Bool, service: String) {
            if isChecked {
                searchRequest.services.insert(service)
            } else {
                searchRequest.services.remove(service)
            }
        }

        func resetFilters() {
            searchRequest.range = 10
            searchRequest.services.removeAll()
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
