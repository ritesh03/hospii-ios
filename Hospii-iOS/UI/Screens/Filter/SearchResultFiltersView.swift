//
//  SearchResultFiltersView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/21/21.
//

import SwiftUI

struct SearchResultFiltersView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    R.image.back()!.suImage
                }
                .padding(.top, 20)
                .padding(.horizontal)

                Text(R.string.localizable.searchResultFiltersViewPageTitleText())
                    .style(.navTitle)
                    .lineLimit(1)
                    .padding(.top, 20)

                Spacer()

                Button(action: {
                    viewModel.resetFilters()
                }) {
                    Text(R.string.localizable.searchResultFiltersViewResetAllButtonTitle())
                        .font(CustomFont.proximaNovaBold(withSize: 16))
                        .foregroundColor(R.color.blueLight()!.suColor)
                }
                .padding(.top, 20)
                .padding(.horizontal)
            }

            content

            Spacer()
        }
        .ignoresSafeArea(edges: .bottom)
        .onLoad {
            viewModel.loadServicesFilters()
        }
    }

    private var content: AnyView {
        switch viewModel.servicesResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last))
        case let .loaded(services): return AnyView(loadedView(services, showLoading: false))
        case let .cache(services): return AnyView(loadedView(services, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Loading Content

private extension SearchResultFiltersView {
    var notRequestedView: some View {
        EmptyView()
    }

    func loadingView(_ previouslyLoaded: [ServiceResponse]?) -> some View {
        if let services = previouslyLoaded {
            return AnyView(loadedView(services, showLoading: true))
        } else {
            return AnyView(
                HStack {
                    Spacer()

                    ActivityIndicatorView().padding(32)

                    Spacer()
                }
            )
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.loadServicesFilters()
        })
    }
}

// MARK: - Displaying Content

private extension SearchResultFiltersView {
    func loadedView(_ services: [ServiceResponse], showLoading _: Bool) -> some View {
        let groupedServices = services.group(by: { $0.group ?? "" })
        
        var groupedServices1 = [[ServiceResponse]]()
        for service in groupedServices {
            let services = service.filter { response in
                response.group != "Price range"
            }
            if services.count > 0 {
                groupedServices1.append(services)
            }
        }
        
        return ScrollView {
            LazyVStack(alignment: .leading) {
                HStack {
                    Text("Distance")
                        .style(.listSectionTitle)
                        .padding(.top, 8)

                    Spacer()

                    Text("\(viewModel.searchRequest.range.roundedString(toPlaces: 1) ?? "")")
                        .style(.subText)
                        .padding(.top, 8)
                }

                HStack {
                    Text("\(viewModel.minimumValue)")
                        .style(.subText)

                    Slider(value: $viewModel.searchRequest.range, in: 1 ... 20)

                    Text("\(viewModel.maximumvalue)")
                        .style(.subText)
                }
                .padding(.top, 4)

                //                FilterItem(title: "Open Now", image: nil, isChecked: viewModel.searchResultFilters.openNow) { _ in
                //                }
                //                .padding(.top, 6)

                ForEach(groupedServices1.indices) { index in
                    Section(header: Text(groupedServices1.get(index)?.first?.group ?? "").style(.listSectionTitle)) {
                        ForEach(groupedServices1[index], id: \.id) { service in
                            let isChecked = viewModel.searchRequest.services.contains(service.service ?? "")
                            FilterItem(title: service.service ?? "", image: nil, isChecked: isChecked) { isChecked in
                                viewModel.selectFilter(isChecked: isChecked, service: service.service ?? "")
                            }
                            .padding(.top, 2)
                        }
                    }
                    .padding(.top)
                }
            }
            .padding(.horizontal, 20)
            .padding(.top, 0)
            .padding(.bottom, 80)
        }
    }
}

struct SearchResultFiltersView_Previews: PreviewProvider {
    static var previews: some View {
        SearchResultFiltersView(viewModel: .init(container: .preview))
    }
}
