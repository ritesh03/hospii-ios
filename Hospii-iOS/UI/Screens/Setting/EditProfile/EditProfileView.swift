//
//  EditProfileView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import SwiftUI

struct EditProfileView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        ZStack(alignment: .top) {
            NavigationLink(
                destination: SetPasswordView(viewModel: .init(container: viewModel.container), action: {
                    viewModel.isPresentedSuccessAlert = true
                }),
                isActive: $viewModel.isPresentedSetPasswordView
            ) {
                EmptyView()
            }

            NavigationLink(
                destination: SignupUserProfileView(viewModel: .init(container: viewModel.container, username: "", mode: .update), action: {
                    viewModel.isPresentedSuccessAlert = true
                }),
                isActive: $viewModel.isPresentedSetUserProfileView
            ) {
                EmptyView()
            }

            VStack(spacing: 20) {
                SettingsButton(title: R.string.localizable.editProfileViewEditUserInformationButtonTitle(), image: R.image.profileCircleIcon()!.suImage) {
                    viewModel.isPresentedSetUserProfileView = true
                }

                SettingsButton(title: R.string.localizable.editProfileViewChangePasswordButtonTitle(), image: R.image.keyIcon()!.suImage) {
                    viewModel.isPresentedSetPasswordView = true
                }

                Spacer()
            }
        }
        .padding(.vertical)
        .padding(.horizontal, 20)
        .modifier(NavigationBarModifier(title: R.string.localizable.editProfileViewPageTitleText()))
        .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.0, tapToDismiss: true, alert: {
            AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.signUpUserProfileViewAlertTitle())
        }, completion: {
            viewModel.isPresentedSuccessAlert = false
        })
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileView(viewModel: .init(container: .preview))
    }
}
