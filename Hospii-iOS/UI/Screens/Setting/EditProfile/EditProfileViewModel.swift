//
//  EditProfileViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import SwiftUI
import Valet

// MARK: - EditProfileViewModel

extension EditProfileView {
    class ViewModel: ObservableObject {
        // State
        @Published var isPresentedSetPasswordView: Bool = false

        @Published var isPresentedSetUserProfileView: Bool = false

        @Published var isPresentedSuccessAlert: Bool = false
        
        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer) {
            self.container = container
            cancelBag.collect {}
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
