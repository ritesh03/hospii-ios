//
//  SettingsView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import CoreStore
import Introspect
import Kingfisher
import SwiftUI
import ThirdPartyMailer

struct SettingsView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        NavigationView {
            ZStack(alignment: .top) {
                NavigationLink(
                    destination: EditProfileView(viewModel: .init(container: viewModel.container)),
                    isActive: $viewModel.isPresentedEditProfileView
                ) {
                    EmptyView()
                }

                NavigationLink(
                    destination: SignUpBusinessProfileView(viewModel: .init(container: viewModel.container, mode: .update), action: {
                        viewModel.isPresentedSuccessAlert = true
                    }),
                    isActive: $viewModel.isPresentedBusinessProfileView
                ) {
                    EmptyView()
                }
                NavigationLink(
                    destination: SignUpBusinessProfileView(viewModel: .init(container: viewModel.container, mode: .create), action: {
                        viewModel.isPresentedSuccessAlert = true
                    }),
                    isActive: $viewModel.isPresentedCreateBusinessProfileView
                ) {
                    EmptyView()
                }

                ScrollView {
                    VStack(alignment: .center, spacing: 20) {
                        content

                        SettingsButton(title: R.string.localizable.settingsViewContactUsButtonTitle(), image: R.image.emailIcon()!.suImage) {
                            viewModel.isPresentedContactActionSheet = true
                        }
                        .padding(.horizontal, 20)

                        SettingsButton(title: R.string.localizable.settingsViewLogOutButtonTitle(), image: R.image.leaveIcon()!.suImage) {
                            viewModel.isPresentedLogoutAlert = true
                        }
                        .padding(.horizontal, 20)

                        Spacer()
                    }
                }
                .navigationBarTitle("", displayMode: .large)
                .navigationBarHidden(true)
                .alert(isPresented: $viewModel.isPresentedLogoutAlert, content: {
                    Alert(
                        title: Text(R.string.localizable.settingsViewLogOutAlertTitle()),
                        primaryButton: .destructive(Text(R.string.localizable.settingsViewLogOutButtonTitle())) {
                            viewModel.logOut()
                        },
                        secondaryButton: .cancel(Text(R.string.localizable.commonCancelButtonTitle()))
                    )
                })
                .sheet(isPresented: $viewModel.isPresentedServiceFiltersView) {
                    ServiceFiltersView(viewModel: .init(container: viewModel.container))
                }
                .sheet(isPresented: $viewModel.isPresentedContactView) {
                    NavigationView {
                        ContactViewController()
                            .ignoresSafeArea(.keyboard)
                            .navigationBarTitleDisplayMode(.inline)
                            .modifier(NavigationBarModifier(title: R.string.localizable.contactViewPageTitleText()))
                    }
                }
                .sheet(isPresented: $viewModel.isPresentedPhotoUploadingView) {
                    NavigationView {
                        PortfoiloUploadingView(viewModel: .init(container: viewModel.container))
                    }
                }
                .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.0, tapToDismiss: true, alert: {
                    AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.signUpUserProfileViewAlertTitle())
                }, completion: {
                    viewModel.isPresentedSuccessAlert = false
                })
                .actionSheet(isPresented: $viewModel.isPresentedContactActionSheet) {
                    ActionSheet(
                        title: Text(R.string.localizable.contactViewPageTitleText()),
                        buttons: [
                            .default(Text(R.string.localizable.contactViewEmailText())) {
                                let recipient = "support@hospii.com"
                                let subject = "Hospii Contact Us"
                                var body = ""

                                let device = UIDevice.current
                                body += "\n\nDevice"
                                body += "\n  OS: " + device.systemVersion
                                body += "\n  System Name: " + device.systemName
                                body += "\n  Model: " + device.modelName
                                body += "\n  App Version: " + (Bundle.main.appVersion ?? "unknown")
                                body += "\n  Build Version: " + (Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String ?? "unknown")
                                ThirdPartyMailer.openCompose(recipient: recipient, subject: subject, body: body)
                            },
                            .default(Text(R.string.localizable.contactViewLiveChatText())) {
                                viewModel.isPresentedContactView = true
                            },
                            .cancel(),
                        ]
                    )
                }
                .onLoad {
                    viewModel.getProfile()
                }
            }
        }
    }

    private var content: some View {
        ListReader(viewModel.userPublisher) { users in
            let user = users.get(0)?.object
            let isProfessional = user?.isProfessional ?? false
            let hasBusiness = user?.hasBusiness ?? false
            if isProfessional {
                VStack(alignment: .center, spacing: 20) {
                    ZStack(alignment: .bottom) {
                        if let imageUrl = viewModel.coverImageUrl {
                            KFImage
                                .url(URL(string: imageUrl))
                                .setProcessor(ImageProcessors.avatarProcessor)
                                .resizable()
                                .scaledToFill()
                                .frame(maxWidth: .infinity)
                                .frame(height: coverImageHeight)
                                .clipped()
                        } else {
                            ZStack {}
                                .frame(maxWidth: .infinity)
                                .frame(height: coverImageHeight)
                        }

                        KFImage
                            .url(URL(string: user?.getPhoto()?.imageUrl ?? ""))
                            .placeholder {
                                R.image.defaultProfilePhoto()!.suImage
                                    .resizable()
                                    .clipped()
                                    .scaledToFit()
                                    .frame(width: 90, height: 90)
                                    .clipShape(Circle())
                            }
                            .setProcessor(ImageProcessors.avatarProcessor)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 90, height: 90)
                            .clipShape(Circle())
                            .alignmentGuide(.bottom) { d in d[.bottom] / 2 }
                    }
                    .frame(maxWidth: .infinity)
                    .frame(height: coverImageHeight)

                    Text("\(user?.firstName ?? "") \(user?.lastName ?? "")")
                        .font(CustomFont.proximaNovaSemibold(withSize: 20))
                        .foregroundColor(R.color.text()!.suColor)
                        .padding(.top, 25)

                    RoundedButton(style: .secondaryButton, title: R.string.localizable.settingsViewEditProfileButtonTitle(), cornerRadius: 16) {
                        viewModel.isPresentedEditProfileView = true
                    }
                    .padding(.horizontal, 64)

                    SettingsButton(title: R.string.localizable.settingsViewBusinessProfileEditButtonTitle(), image: R.image.storeIcon()!.suImage) {
                        viewModel.isPresentedBusinessProfileView = true
                    }
                    .padding(.horizontal, 20)

                    if hasBusiness {
                        SettingsButton(title: R.string.localizable.settingsViewServiceFilterButtonTitle(), image: R.image.filterIcon()!.suImage) {
                            viewModel.isPresentedServiceFiltersView = true
                        }
                        .padding(.horizontal, 20)

                        SettingsButton(title: R.string.localizable.settingsViewUploadServicePhotosButtonTitle(), image: R.image.addPhotoIcon()!.suImage) {
                            viewModel.isPresentedPhotoUploadingView = true
                        }
                        .padding(.horizontal, 20)
                    }
                }
                .edgesIgnoringSafeArea(.top)
            } else {
                VStack(alignment: .center, spacing: 20) {
                    KFImage.url(URL(string: user?.getPhoto()?.imageUrl ?? ""))
                        .placeholder { R.image.defaultProfilePhoto()!.suImage }
                        .setProcessor(ImageProcessors.avatarProcessor)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 90, height: 90)
                        .clipShape(Circle())
                        .padding(.top, 16)

                    Text("\(user?.firstName ?? "") \(user?.lastName ?? "")")
                        .font(CustomFont.proximaNovaSemibold(withSize: 20))
                        .foregroundColor(R.color.text()!.suColor)

                    RoundedButton(style: .secondaryButton, title: R.string.localizable.settingsViewEditProfileButtonTitle(), cornerRadius: 16) {
                        viewModel.isPresentedEditProfileView = true
                    }
                    .padding(.horizontal, 64)

                    SettingsButton(title: R.string.localizable.settingsViewCreateHairStylistButtonTitle(), image: R.image.scissorIcon()!.suImage) {
                        viewModel.isPresentedCreateBusinessProfileView = true
                    }
                    .padding(.top)
                    .padding(.horizontal, 20)
                }
            }
        }
    }

    private var coverImageHeight: CGFloat {
        return (UIScreen.main.bounds.size.width - 40) * 15 / 34
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(viewModel: .init(container: .preview))
    }
}
