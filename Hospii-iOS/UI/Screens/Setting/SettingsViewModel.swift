//
//  SettingsViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import ChatProvidersSDK
import ChatSDK
import CoreStore
import SwiftUI
import Valet

// MARK: - SettingsViewModel

extension SettingsView {
    class ViewModel: ObservableObject {
        // State
        @Published var isPresentedPhotoUploadingView: Bool = false

        @Published var isPresentedEditProfileView: Bool = false

        @Published var isPresentedLogoutAlert: Bool = false

        @Published var isPresentedServiceFiltersView: Bool = false

        @Published var isPresentedBusinessProfileView: Bool = false

        @Published var isPresentedCreateBusinessProfileView: Bool = false

        @Published var isPresentedContactView: Bool = false

        @Published var isPresentedContactActionSheet: Bool = false

        @Published var getProfileResponse: Loadable<UserResponse>

        @Published var isPresentedSuccessAlert: Bool = false
        
        @Published var coverImageUrl: String?

        let userPublisher: ListPublisher<UserMO> = {
            CoreStoreDefaults.dataStack.publishList(From<UserMO>()
                .orderBy(.ascending(\.key))
                .tweak { $0.fetchLimit = 1 })
        }()

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer, getProfileResponse: Loadable<UserResponse> = .notRequested) {
            self.container = container
            _getProfileResponse = .init(initialValue: getProfileResponse)

            cancelBag.collect {
                userPublisher.reactive
                    .snapshot(emitInitialValue: true)
                    .compactMap { $0.get(0)?.object }
                    .sink { [weak self] user in
                        if let business = user.getUserBusinesses()?.first, let self = self {
                            self.coverImageUrl = business.coverPhoto?.imageUrl
                        }
                        self?.addUserIdentity(user: user)
                    }
            }
        }

        func getProfile() {
            container.services.usersService
                .getProfile(response: loadableSubject(\.getProfileResponse))
        }

        func logOut() {
            LogoutUtils.logout(appState: container.appState)
        }

        fileprivate func addUserIdentity(user: UserMO) {
            let chatAPIConfiguration = ChatAPIConfiguration()
            chatAPIConfiguration.visitorInfo = VisitorInfo(name: user.username ?? "", email: user.email ?? "", phoneNumber: (user.phoneNumberCountryCode ?? "") + (user.phoneNumber ?? ""))
            ChatProvidersSDK.Chat.instance?.configuration = chatAPIConfiguration
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
