//
//  SetPasswordViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import Moya
import ResponsiveTextField
import SwiftUI
import Valet

struct SetPasswordViewFirstResponses {
    var oldPasswordResponderDemand: FirstResponderDemand?

    var newPasswordFirstResponderDemand: FirstResponderDemand?
}

// MARK: - SetPasswordViewModel

extension SetPasswordView {
    class ViewModel: ObservableObject {
        // State
        @Published var updatePasswordRequest = PutPasswordRequest()

        @Published var passwordError: String = ""

        @Published var updateResponse: Loadable<Response>

        @Published var isPresentedSuccessAlert: Bool = false

        @Published var isPresentedFaliedAlert: Bool = false

        @Published var setPasswordViewFirstResponses = SetPasswordViewFirstResponses()

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer, updateResponse: Loadable<Response> = .notRequested) {
            self.container = container
            _updateResponse = .init(initialValue: updateResponse)
            cancelBag.collect {
                $updateResponse
                    .sink { [weak self] response in
                        switch response {
                        case .loaded:
                            self?.isPresentedSuccessAlert = true
                        case .failed:
                            self?.isPresentedFaliedAlert = true
                        default:
                            break
                        }
                    }
            }
        }

        func updatePassword() {
            passwordError = Validator.shared.validate(password: updatePasswordRequest.newPassword)

            if !passwordError.isEmpty || updatePasswordRequest.password.isEmpty {
                return
            }

            container.services.authService.putUpdatePassword(response: loadableSubject(\.updateResponse), request: updatePasswordRequest)
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
