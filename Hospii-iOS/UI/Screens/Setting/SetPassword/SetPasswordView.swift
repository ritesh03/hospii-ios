//
//  SetPasswordView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/4/21.
//

import ResponsiveTextField
import SwiftUI

struct SetPasswordView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let action: () -> Void
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Text(R.string.localizable.setPasswordViewPageDescriptionText())
                .style(.sectionSubtitle)

            PrimaryTextField(text: $viewModel.updatePasswordRequest.password, placeHolder: "", title: R.string.localizable.setPasswordViewOldPasswordTextFieldTitle(), error: "", style: .secureTextField, firstResponderDemand: $viewModel.setPasswordViewFirstResponses.oldPasswordResponderDemand, returnType: .next) {
                viewModel.setPasswordViewFirstResponses.newPasswordFirstResponderDemand = .shouldBecomeFirstResponder
            }

            PrimaryTextField(text: $viewModel.updatePasswordRequest.newPassword, placeHolder: "", title: R.string.localizable.setPasswordViewNewPasswordTextFieldTitle(), error: viewModel.passwordError, style: .secureTextField, firstResponderDemand: $viewModel.setPasswordViewFirstResponses.newPasswordFirstResponderDemand, returnType: .send) {
                viewModel.setPasswordViewFirstResponses.newPasswordFirstResponderDemand = .shouldResignFirstResponder
            }

            RoundedButton(style: .primaryButton, isLoading: viewModel.updateResponse.isLoading, title: R.string.localizable.setPasswordViewUpdateButtonTitle()) {
                viewModel.updatePassword()
            }
            .disabled(viewModel.updateResponse.isLoading)
            .padding(.top, 20)

            Spacer()
        }
        ._onBindingChange($viewModel.isPresentedSuccessAlert, perform: { value in
            if value {
                action()
                self.presentationMode.wrappedValue.dismiss()
            }
        })
//        .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.5, tapToDismiss: true, offsetY: 120) {
//            AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.signUpUserProfileViewAlertTitle())
//        }
        .toast(isPresenting: $viewModel.isPresentedFaliedAlert, duration: 1.5, tapToDismiss: true, offsetY: 120) {
            AlertToast(displayMode: .hud, type: .error(R.color.white()!.suColor), title: R.string.localizable.setPasswordViewFailedAlertTitle())
        }
        .padding(.vertical)
        .padding(.horizontal, 20)
        .modifier(NavigationBarModifier(title: R.string.localizable.setPasswordViewPageTitleText()))
    }
}

struct SetPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        SetPasswordView(viewModel: .init(container: .preview), action: {})
    }
}
