//
//  UINavigationController+extensions.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/30/21.
//

import Foundation
import SwiftUI

// Enable swipe back for the NavigationView.
extension UINavigationController: UIGestureRecognizerDelegate {
    override open func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self

        //        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithOpaqueBackground()
        //        appearance.shadowImage = R.image.transparent()
        //        navigationBar.standardAppearance = appearance
        //        navigationBar.compactAppearance = appearance
        //        navigationBar.scrollEdgeAppearance = appearance
    }

    public func gestureRecognizerShouldBegin(_: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}

extension UITabBarController {
    override open func viewDidLoad() {
        super.viewDidLoad()
        let appearance = UITabBarAppearance()
        appearance.configureWithOpaqueBackground()
        if #available(iOS 15.0, *) {
            tabBar.scrollEdgeAppearance = appearance
        } else {
            tabBar.standardAppearance = appearance
        }
    }

    //    override open func viewDidAppear(_ animated: Bool) {
    //        super.viewDidAppear(animated)
    //        let appearance = UITabBarAppearance()
    //        appearance.configureWithOpaqueBackground()
    //        if #available(iOS 15.0, *) {
    //            tabBar.scrollEdgeAppearance = appearance
    //        } else {
    //            tabBar.standardAppearance = appearance
    //        }
    //    }
}
