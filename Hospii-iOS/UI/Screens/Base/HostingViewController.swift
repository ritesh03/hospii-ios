//
//  HostingViewController.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/30/21.
//

import Foundation
import SwiftUI

class HostingViewController<ContentView: View>: UIHostingController<ContentView> {
    @objc override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    @objc override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    @objc override var shouldAutorotate: Bool {
        return true
    }
}
