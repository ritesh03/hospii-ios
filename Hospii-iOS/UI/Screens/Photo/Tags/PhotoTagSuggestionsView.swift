//
//  PhotoTagSuggestionsView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/17/21.
//

import SwiftUI

struct PhotoTagSuggestionsView: View {
    @StateObject var viewModel: ViewModel
    @Binding var isPresented: Bool

    let onTagSelected: (String) -> Void

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack {
                if #available(iOS 15.0, *) {
                    RoundedSearchBarTextField(isFocused: true, input: $viewModel.keyword, title: .constant(R.string.localizable.searchViewSearchBarTitle()), placeholderColor: .constant(R.color.gray119()!.suColor)) { _ in
                    }
                    .padding(.leading)
                } else {
                    RoundedSearchBarTextFieldIOS14(isFocused: true, input: $viewModel.keyword, title: .constant(R.string.localizable.searchViewSearchBarTitle())) { _ in
                    }
                }

                Button(action: {
                    isPresented = false
                }) {
                    Text(R.string.localizable.commonCancelButtonTitle())
                        .style(.actionButton)
                }
            }
            .padding(.top, 24)
            .padding(.leading, 4)
            .padding(.trailing)

            ScrollView {
                LazyVStack(alignment: .center) {
                    if !viewModel.keyword.isEmpty {
                        Text(R.string.localizable.photoTagSuggestionsViewListTitle())
                            .style(.listSectionTitle)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.horizontal)
                            .padding(.vertical, 12)

                        Text(viewModel.keyword)
                            .style(.primaryText)
                            .lineLimit(1)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.horizontal)
                            .padding(.horizontal)
                            .padding(.vertical, 12)
                            .onTapGesture {
                                onTagSelected(viewModel.keyword)
                                isPresented = false
                            }

                        Divider()
                    }

                    content
                }
            }
            .padding(.vertical, 12)
        }
    }

    private var content: AnyView {
        switch viewModel.tags {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last))
        case let .loaded(tags): return AnyView(loadedView(tags, showLoading: false))
        case let .cache(tags): return AnyView(loadedView(tags, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Loading Content

private extension PhotoTagSuggestionsView {
    var notRequestedView: some View {
        EmptyView()
    }

    func loadingView(_ previouslyLoaded: [TagResponse]?) -> some View {
        if let tags = previouslyLoaded {
            return AnyView(loadedView(tags, showLoading: true))
        } else {
            return AnyView(ActivityIndicatorView().padding())
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.loadTags()
        })
    }
}

// MARK: - Displaying Content

private extension PhotoTagSuggestionsView {
    func loadedView(_ tags: [TagResponse], showLoading: Bool) -> some View {
        let tags = tags.filter { $0.display.lowercased() != viewModel.keyword.lowercased() }

        if showLoading {
            return AnyView(ActivityIndicatorView().padding())
        } else {
            return AnyView(ForEach(tags, id: \.tag) { tag in
                Text(tag.display)
                    .style(.primaryText)
                    .lineLimit(1)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)
                    .padding(.horizontal)
                    .padding(.vertical, 12)
                    .onTapGesture {
                        onTagSelected(tag.tag)
                        isPresented = false
                    }

                Divider()
            })
        }
    }
}

// MARK: - Preview

struct PhotoTagSuggestionsView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoTagSuggestionsView(viewModel: .init(container: .preview), isPresented: .constant(true)) { _ in
        }
    }
}
