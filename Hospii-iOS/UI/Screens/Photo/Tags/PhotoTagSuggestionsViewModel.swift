//
//  PhotoTagSuggestionsViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/17/21.
//

import Foundation

// MARK: - PhotoTagSuggestionsViewModel

extension PhotoTagSuggestionsView {
    class ViewModel: ObservableObject {
        // State
        @Published var tags: Loadable<[TagResponse]>

        @Published var keyword: String = ""

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()

        init(container: DIContainer, tags: Loadable<[TagResponse]> = .notRequested) {
            self.container = container
            _tags = .init(initialValue: tags)
            cancelBag.collect {
                $keyword
                    .debounce(for: .milliseconds(800), scheduler: DispatchQueue.main)
                    .sink { [weak self] _ in
                        self?.loadTags()
                    }
            }
        }

        func loadTags() {
            if keyword.isEmpty {
                tags = .notRequested
                return
            }
            container.services.searchService.getTags(response: loadableSubject(\.tags), request: SearchSuggestionsRequest(keyword: keyword, latitude: nil, longitude: nil))
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
