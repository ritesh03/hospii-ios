//
//  PhotoGalleryView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Kingfisher
import SwiftUI

struct PhotoGalleryView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        GeometryReader { geometryReader in
            ScrollViewReader { proxy in
                PullToRefreshView(header: SimpleRefreshingView(),
                                  footer: SimpleRefreshingView(),
                                  isHeaderRefreshing: $viewModel.headerRefreshing,
                                  isFooterRefreshing: $viewModel.footerRefreshing,
                                  isNoMoreData: $viewModel.getPostsRequest.noMore,
                                  onHeaderRefresh: { viewModel.reloadPhotos() },
                                  onFooterRefresh: { viewModel.fetchPhotos() }) {
                    LazyVStack {
                        ForEach(viewModel.photosResponse.value?.data ?? [], id: \.id) { post in
                            makePhotoCell(width: geometryReader.size.width, post: post)
                                .tag(post.id)
                        }
                    }
                }
                .onLoad {
                    if viewModel.getPostsRequest.selectedItemId != -1 {
                        viewModel.postView(post: (viewModel.photosResponse.value?.data ?? []).first { $0.id == viewModel.getPostsRequest.selectedItemId })
                        proxy.scrollTo(viewModel.getPostsRequest.selectedItemId, anchor: .top)
                    }
                }
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: HStack {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }) {
                        R.image.back()!.suImage
                    }

                    Text("\(viewModel.getPostsRequest.business.user?.firstName ?? "") \(viewModel.getPostsRequest.business.user?.lastName ?? "")")
                        .style(.navTitle)
                        .lineLimit(1)
                        .frame(maxWidth: 200)
                })
            }
        }
    }

    private func makePhotoCell(width: CGFloat, post: PostResponse) -> some View {
        let user = post.business?.user
        let media = post.__medias__?.first
        let photoWidth = media?.width ?? 0
        let photoHeight = media?.height ?? 0

        return VStack(spacing: 0) {
            KFImage
                .url(URL(string: media?.imageUrl ?? ""))
                .setProcessor(ImageProcessors.thumbnailProcessor)
                .resizable()
                .scaledToFit()
                .frame(width: width, height: photoWidth > 0 && photoHeight > 0 ? (width * (photoHeight / photoWidth)) : width)
                .clipped()

            PhotoAuthorView(user: user, createdAt: post.createdAt)
                .padding(.horizontal)
                .padding(.top)
                .padding(.bottom, 24)
        }
    }
}

struct PhotoGalleryView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoGalleryView(viewModel: .init(container: .preview, getPostsRequest: GetPostsRequest(business: UserBusinessResponse(id: 0, name: "", businessAbout: nil, type: nil, status: nil, phoneNumber: nil, phoneNumberExtension: nil, phoneNumberCountryCode: nil, isPhoneNumberVerified: false, coverPhoto: nil, userBusinessHours: nil, userAddress: BusinessAddressResponse(address: "", address2: nil, city: nil, state: nil, code: nil, latitude: nil, longitude: nil), __serviceFilters__: nil, user: nil, distance: 0)), photosResponse: .notRequested))
    }
}
