//
//  PhoneGalleryViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Moya
import SwiftUI

// MARK: - PhotoGalleryViewModel

extension PhotoGalleryView {
    class ViewModel: ObservableObject {
        // State
        @Published var photosResponse: Loadable<SearchFeedsResponse>

        @Published var postViewResponse: Loadable<Response>

        @Published var footerRefreshing: Bool = false

        @Published var headerRefreshing: Bool = false

        @Published var getPostsRequest: GetPostsRequest

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer,
             getPostsRequest: GetPostsRequest,
             photosResponse: Loadable<SearchFeedsResponse>,
             postViewResponse: Loadable<Response> = .notRequested)
        {
            self.photosResponse = photosResponse
            self.getPostsRequest = getPostsRequest
            self.container = container
            _postViewResponse = .init(initialValue: postViewResponse)
            cancelBag.collect {
                $photosResponse
                    .sink { [weak self] response in
                        self?.photosResponseChanged(response: response)
                    }
            }
        }

        func reloadPhotos() {
            getPostsRequest.page = 1
            getPostsRequest.noMore = false
            fetchPhotos()
        }

        func fetchPhotos() {
            container.services.businessesService.getUserBusinessPhotos(response: loadableSubject(\.photosResponse), getPostsRequest: getPostsRequest)
        }

        func postView(post: PostResponse?) {
            guard let post = post else {
                return
            }

            container.services.mediaService.postView(response: loadableSubject(\.postViewResponse), postId: post.id)
        }

        private func photosResponseChanged(response: Loadable<SearchFeedsResponse>) {
            switch response {
            case let .loaded(body):
                headerRefreshing = false
                footerRefreshing = false

                if !body.data.isEmpty {
                    getPostsRequest.page = getPostsRequest.page + 1
                }

                if body.meta.itemCount == 0 {
                    getPostsRequest.noMore = true
                }
            case .failed:
                footerRefreshing = false
            default:
                break
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
