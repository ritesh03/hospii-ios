//
//  PortfoiloUploadingViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import Combine
import Moya
import PermissionsSwiftUIPhoto
import SwiftUI

enum PortfoiloUploadingMode {
    case normal
    case editing
}

// MARK: - PortfoiloUploadingViewModel

extension PortfoiloUploadingView {
    class ViewModel: ObservableObject {
        // State
        @Published var images: [UIImage] = []

        @Published var uploadPhotoHoursResponse: Loadable<Response>

        @Published var deletePostsResponse: Loadable<Response>

        @Published var photosResponse: Loadable<SearchFeedsResponse>

        @Published var isLoading: Bool = false

        @Published var tags: [String] = []

        @Published var about: String = ""

        @Published var isPresentedImagePickerView = false

        @Published var isPresentedPhotoDetailsView = false

        @Published var isPresentedPermissionsAlert = false

        @Published var isPresentedSettingsAlert = false

        @Published var isPresentedSuccessAlert = false

        @Published var pageRequest = PageRequest()

        @Published var footerRefreshing: Bool = false

        @Published var mode: PortfoiloUploadingMode = .normal

        // Misc
        let container: DIContainer

        private var cancelBag = CancelBag()

        init(container: DIContainer, uploadPhotoHoursResponse: Loadable<Response> = .notRequested, photosResponse: Loadable<SearchFeedsResponse> = .notRequested, deletePostsResponse: Loadable<Response> = .notRequested) {
            self.container = container
            _uploadPhotoHoursResponse = .init(initialValue: uploadPhotoHoursResponse)
            _photosResponse = .init(initialValue: photosResponse)
            _deletePostsResponse = .init(initialValue: deletePostsResponse)

            cancelBag.collect {
                $uploadPhotoHoursResponse
                    .sink { [weak self] response in
                        self?.onUploadPhotoResponseChanged(response: response)
                    }

                $isPresentedPermissionsAlert
                    .removeDuplicates()
                    .filter { !$0 }
                    .dropFirst()
                    .sink { [weak self] _ in
                        self?.openImagePicker(false)
                    }

                $photosResponse
                    .sink { [weak self] response in
                        self?.getPhotosResponseChanged(response: response)
                    }
            }
        }

        func openImagePicker(_ shouldOpenPermissionAlert: Bool = true) {
            guard !isLoading, !isPresentedSuccessAlert else {
                return
            }
            let authorized = JMPhotoPermissionManager.photo.authorizationStatus == .authorized || JMPhotoPermissionManager.photo.authorizationStatus == .limited
            if authorized {
                isPresentedImagePickerView = true
            } else if shouldOpenPermissionAlert {
                if JMPhotoPermissionManager.photo.authorizationStatus == .denied {
                    isPresentedSettingsAlert = true
                } else {
                    isPresentedPermissionsAlert = true
                }
            }
        }

        func removePhoto(index: Int) {
            guard !isLoading, !isPresentedSuccessAlert else {
                return
            }
            images.remove(at: index)
        }

        func removePhoto(id: Int) {
            guard !isLoading, !isPresentedSuccessAlert else {
                return
            }
            if let value = photosResponse.value, let index = value.data.firstIndex(where: { $0.id == id }) {
                var data = value.data
                data.remove(at: index)
                photosResponse.setValue(newValue: SearchFeedsResponse(data: data, meta: value.meta))
            }
            container.services.mediaService.deletePhotos(response: loadableSubject(\.deletePostsResponse), ids: [id])
        }

        func upload() {
            isLoading = true
            let imageData = images
                .compactMap { $0.jpegData(compressionQuality: 1.0) }
                .filter { Float($0.count) / 1024.0 / 1024.0 <= container.appState[\.system].photoFileSizeLimit }
            container.services.mediaService.postPhoto(cancelBag: cancelBag, response: loadableSubject(\.uploadPhotoHoursResponse), imageData: imageData, tags: tags, about: about)
        }

        private func onUploadPhotoResponseChanged(response: Loadable<Response>) {
            switch response {
            case .loaded, .failed:
                isPresentedSuccessAlert = true
                isLoading = false
            default:
                break
            }
        }

        func getPhotos() {
            photosResponse.cancelTask()
            container.services.usersService.getPhotos(response: loadableSubject(\.photosResponse), request: pageRequest)
        }

        func getPhotosResponseChanged(response: Loadable<SearchFeedsResponse>) {
            switch response {
            case let .loaded(body):
                footerRefreshing = false

                if body.meta.itemCount == 0 {
                    pageRequest.noMore = true
                } else {
                    pageRequest.page = pageRequest.page + 1
                }
            case .failed:
                footerRefreshing = false
            default:
                break
            }
        }

        func toggleMode() {
            if mode == .editing {
                mode = .normal
            } else {
                mode = .editing
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
