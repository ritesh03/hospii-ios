//
//  PortfoiloUploadingView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import Introspect
import Kingfisher
import PermissionsSwiftUIPhoto
import SwiftUI

struct PortfoiloTestItem: Identifiable {
    let id = UUID()
}

struct PortfoiloUploadingView: View {
    @StateObject var viewModel: ViewModel

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .bottom) {
                NavigationLink(
                    destination: PhotoDetailsView(viewModel: .init(container: viewModel.container, images: viewModel.images)) { about, tags in
                        viewModel.about = about
                        viewModel.tags = tags
                        viewModel.upload()
                    },
                    isActive: $viewModel.isPresentedPhotoDetailsView
                ) {
                    EmptyView()
                }

                PullToRefreshView(header: EmptyView(),
                                  footer: SimpleRefreshingView(),
                                  isHeaderRefreshing: .constant(false),
                                  isFooterRefreshing: $viewModel.footerRefreshing,
                                  isNoMoreData: $viewModel.pageRequest.noMore,
                                  onHeaderRefresh: {},
                                  onFooterRefresh: { viewModel.getPhotos() }) {
                    VStack(alignment: .leading, spacing: 8) {
                        Text(R.string.localizable.portfoiloUploadingViewPageDescriptionText())
                            .style(.sectionSubtitle)

                        LazyVGrid(columns: [
                            GridItem(.flexible()),
                            GridItem(.flexible()),
                            GridItem(.flexible()),
                        ], spacing: 12) {
                            VStack {
                                R.image.addFrontPhoto()!.suImage
                            }
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                            .overlay(
                                RoundedRectangle(cornerRadius: 20)
                                    .strokeBorder(
                                        style: StrokeStyle(
                                            lineWidth: 1.5,
                                            dash: [4]
                                        )
                                    )
                                    .foregroundColor(R.color.gray219()!.suColor)
                            )
                            .aspectRatio(1, contentMode: .fit)
                            .onTapGesture {
                                viewModel.openImagePicker()
                            }
                            .disabled(viewModel.uploadPhotoHoursResponse.isLoading)

                            ForEach(viewModel.images.indices, id: \.self) { index in
                                if let image = viewModel.images.get(index) {
                                    makeCell(image: image, index: index, width: (geometry.size.width - 56) / 3)
                                } else {
                                    EmptyView()
                                }
                            }

                            content(geometry: geometry)
                        }
                        .padding(.top, 16)
                    }
                    .padding(.top)
                    .padding(.horizontal, 16)
                }

                RoundedButton(style: .primaryButton, isLoading: viewModel.isLoading, title: R.string.localizable.portfoiloUploadingViewAddPhotoButtonTitle()) {
                    viewModel.openImagePicker()
                }
                .padding(.horizontal)
                .padding(.bottom, 20)
                .disabled(viewModel.isLoading)
                .shadow(radius: 4)
            }
        }
        .onAppear {
            let attributes = [NSAttributedString.Key.font: R.font.proximaNovaSemibold(size: 17)!]
            UINavigationBar.appearance().titleTextAttributes = attributes
            UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal)
        }
        .sheet(isPresented: $viewModel.isPresentedImagePickerView) {
            YPImagePickerView(allowsEditing: false, selectionLimit: 50, delegate: YPImagePickerView.Delegate(isPresented: $viewModel.isPresentedImagePickerView, didCancel: {}, didSelect: { images in
                DispatchQueue.main.async {
                    viewModel.images = viewModel.images + images
                    viewModel.isPresentedPhotoDetailsView = true
                }
            }))
        }
        .JMAlert(showModal: $viewModel.isPresentedPermissionsAlert, for: [.photo], restrictDismissal: false, autoDismiss: true, autoCheckAuthorization: true)
        .alert(isPresented: $viewModel.isPresentedSettingsAlert) {
            Alert(
                title: Text(R.string.localizable.permissionsPhotoLibraryAlertTitle()),
                message: Text(R.string.localizable.permissionsPhotoLibraryAlertMessage()),
                primaryButton: .cancel(Text(R.string.localizable.commonCancelButtonTitle())),
                secondaryButton: .default(Text(R.string.localizable.permissionsSettingsButtonTitle()), action: {
                    if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                })
            )
        }
        .navigationTitle(R.string.localizable.portfoiloUploadingViewPageTitleText())
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                R.image.back()!.suImage
            }, trailing: Button(action: {
                viewModel.toggleMode()
            }) {
                Text(viewModel.mode == .editing ? R.string.localizable.commonCancelButtonTitle() : R.string.localizable.portfoiloUploadingViewEditButtonTitle())
                    .style(.actionButton)
            })
        .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.5, tapToDismiss: false, offsetY: 120, alert: {
            AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.portfoiloUploadingViewSuccessAlertTitle())
        }, completion: {
            presentationMode.wrappedValue.dismiss()
        })
    }

    fileprivate func makeCell(image: UIImage, index: Int, width: CGFloat) -> some View {
        let width = max(0, width)
        return ZStack {
            image.suImage
                .resizable()
                .scaledToFill()
                .frame(width: width, height: width)
                .cornerRadius(16)
                .clipped()
                .overlay(
                    viewModel.mode == .editing ? AnyView(R.image.deleteRounded()!.suImage
                        .shadow(radius: 2)
                        .padding(10)
                        .onTapGesture {
                            viewModel.removePhoto(index: index)
                        }
                        .disabled(viewModel.uploadPhotoHoursResponse.isLoading)) : AnyView(EmptyView()),
                    alignment: .topTrailing
                )
        }
    }

    fileprivate func makePostCell(post: PostResponse, width: CGFloat) -> some View {
        return ZStack {
            KFImage
                .url(URL(string: post.__medias__?.first?.imageUrl ?? ""))
                .setProcessor(ImageProcessors.thumbnailProcessor)
                .resizable()
                .scaledToFill()
                .frame(width: width, height: width)
                .cornerRadius(16)
                .clipped()
                .overlay(
                    viewModel.mode == .editing ? AnyView(R.image.deleteRounded()!.suImage
                        .shadow(radius: 2)
                        .padding(10)
                        .onTapGesture {
                            viewModel.removePhoto(id: post.id)
                        }
                        .disabled(viewModel.uploadPhotoHoursResponse.isLoading)) : AnyView(EmptyView()),
                    alignment: .topTrailing
                )
        }
    }

    func content(geometry: GeometryProxy) -> AnyView {
        switch viewModel.photosResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(geometry: geometry, last?.data))
        case let .loaded(posts): return AnyView(loadedView(geometry: geometry, posts.data, showLoading: false))
        case let .cache(posts): return AnyView(loadedView(geometry: geometry, posts.data, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Loading Content

private extension PortfoiloUploadingView {
    var notRequestedView: some View {
        ZStack {}
            .onAppear {
                viewModel.getPhotos()
            }
    }

    func loadingView(geometry: GeometryProxy, _ previouslyLoaded: [PostResponse]?) -> some View {
        if let posts = previouslyLoaded {
            return AnyView(loadedView(geometry: geometry, posts, showLoading: true))
        } else {
            return AnyView(ActivityIndicatorView()
                .padding())
        }
    }

    func failedView(_: Error) -> some View {
        EmptyView()
    }
}

// MARK: - Displaying Content

private extension PortfoiloUploadingView {
    func loadedView(geometry: GeometryProxy, _ posts: [PostResponse], showLoading _: Bool) -> some View {
        ForEach(posts, id: \.id) { post in
            makePostCell(post: post, width: (geometry.size.width - 56) / 3)
        }
    }
}

struct PortfoiloUploadingView_Previews: PreviewProvider {
    static var previews: some View {
        PortfoiloUploadingView(viewModel: .init(container: .preview))
    }
}
