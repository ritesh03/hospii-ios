//
//  PhotoEditorViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import Combine
import SwiftUI

// MARK: - PortfoiloUploadingViewModel

extension PhotoEditorView {
    class ViewModel: ObservableObject {
        // State

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()

        init(container: DIContainer) {
            self.container = container
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
