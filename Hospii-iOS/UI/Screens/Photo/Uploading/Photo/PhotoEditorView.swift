//
//  PhotoEditorView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import SwiftUI

struct PhotoEditorView: View {
    @StateObject var viewModel: ViewModel

    @State private var currentScale: CGFloat = 1.0
    @GestureState private var zoomFactor: CGFloat = 1.0

    @State private var position = CGPoint(x: 0, y: 0)

    var magnificationGesture: some Gesture {
        return MagnificationGesture()
            .updating($zoomFactor) { value, scale, _ in
                scale = value
            }
            .onEnded { value in
                self.currentScale = value
            }
    }

    var dragGesture: some Gesture {
        return DragGesture()
            .onChanged { value in
                self.position = value.location
            }
    }

    var body: some View {
        ZStack(alignment: .top) {
            Color.black
                .edgesIgnoringSafeArea(.all)

            VStack {
                Text("Move and Scale")
                    .font(CustomFont.proximaNovaSemibold(withSize: 18))
                    .foregroundColor(.white)

                Spacer()

                ZStack {
                    R.image.hospiiSquareLogo()!.suImage
                        .resizable()
                        .scaledToFit()
                        .clipped()
                        .scaleEffect(currentScale * zoomFactor)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .gesture(magnificationGesture)
                        .gesture(dragGesture)

                    R.image.profilePhotoMask()!.suImage
                        .resizable()
                        .scaledToFill()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                }.aspectRatio(1, contentMode: .fit)

                Spacer()

                HStack {
                    Button("Cancel") {}
                        .font(CustomFont.proximaNovaSemibold(withSize: 18))
                        .foregroundColor(.white)
                        .padding(20)

                    Spacer()

                    Button("Done") {}
                        .font(CustomFont.proximaNovaSemibold(withSize: 18))
                        .foregroundColor(.white)
                        .padding(20)
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }

    func HoleShapeMask(in rect: CGRect) -> Path {
        var shape = Rectangle().path(in: rect)
        shape.addPath(Circle().path(in: rect))
        return shape
    }
}

struct PhotoEditorView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoEditorView(viewModel: .init(container: .preview))
    }
}
