//
//  PhotoDetailsViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/16/21.
//

import Foundation
import UIKit

// MARK: - PhotoDetailsViewModel

extension PhotoDetailsView {
    class ViewModel: ObservableObject {
        // State
        @Published var description: String = ""

        @Published var isPresentedPhotoTagsView = false

        @Published var images: [UIImage]

        @Published var tags: [String] = []

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()

        init(container: DIContainer, images: [UIImage]) {
            self.container = container
            _images = .init(initialValue: images)
            cancelBag.collect {}
        }

        func addTag(tag: String) {
            tags.append(tag)
        }

        func removeTag(_ index: Int) {
            tags.remove(at: index)
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
