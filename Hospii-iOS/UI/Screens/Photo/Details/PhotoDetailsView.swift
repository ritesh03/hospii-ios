//
//  PhotoDetailsView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/16/21.
//

import Introspect
import SwiftUI

struct PhotoDetailsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @StateObject var viewModel: ViewModel

    let action: (String, [String]) -> Void

    var body: some View {
        GeometryReader { geometry in
            ScrollView {
                VStack(alignment: .leading) {
                    if !viewModel.images.isEmpty {
                        TabView {
                            ForEach(viewModel.images, id: \.self) { image in
                                makeImageCell(geometry: geometry, image: image)
                            }
                        }
                        .frame(width: geometry.size.width, height: geometry.size.width)
                        .background(R.color.white()!.suColor)
                        .tabViewStyle(.page)
                        .indexViewStyle(.page(backgroundDisplayMode: .interactive))
                    } else {
                        EmptyView()
                    }

                    HStack(spacing: 0) {
                        Spacer()

                        Button(action: {
                            viewModel.isPresentedPhotoTagsView = true
                        }) {
                            Text(R.string.localizable.photoDetailsViewAddTagsButtonTitle())
                                .padding(.vertical, 10)
                                .padding(.horizontal, 16)
                                .style(.actionButton)
                                .overlay(RoundedRectangle(cornerRadius: .infinity).stroke(R.color.gray219()!.suColor))
                                .padding(.top, 4)
                                .padding(.horizontal)
                        }
                    }

                    PrimaryTextEditor(text: $viewModel.description, placeHolder: R.string.localizable.photoDetailsViewAboutTextFieldPlaceholder(), title: R.string.localizable.photoDetailsViewAboutTextFieldTitle(), error: "")
                        .padding(.horizontal)
                        .padding(.top, -6)

                    ScrollView(.horizontal) {
                        HStack {
                            ForEach(viewModel.tags.indices, id: \.self) { index in
                                HStack {
                                    Text(viewModel.tags[index])
                                        .style(.subText)

                                    Button(action: {
                                        viewModel.removeTag(index)
                                    }) {
                                        R.image.smallCloseIcon()!.suImage
                                            .frame(width: 14, height: 14)
                                    }
                                }
                                .padding(.vertical, 8)
                                .padding(.horizontal, 10)
                                .overlay(RoundedRectangle(cornerRadius: .infinity).stroke(R.color.gray219()!.suColor))
                                .padding(.leading, 2)
                                .padding(.vertical, 2)
                            }
                        }
                        .padding(.leading, 16)
                    }
                    .padding(.top, 8)

                    RoundedButton(style: .primaryButton, title: R.string.localizable.portfoiloUploadingViewUploadButtonTitle()) {
                        action(viewModel.description, viewModel.tags)
                        presentationMode.wrappedValue.dismiss()
                    }
                    .padding(.top)
                    .padding(.bottom, 80)
                    .padding(.horizontal, 20)
                }
            }
            .sheet(isPresented: $viewModel.isPresentedPhotoTagsView) {
                PhotoTagSuggestionsView(viewModel: .init(container: viewModel.container), isPresented: $viewModel.isPresentedPhotoTagsView) { tag in
                    viewModel.addTag(tag: tag)
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .modifier(NavigationBarModifier(title: R.string.localizable.photoDetailsViewPageTitleText()))
        }
    }

    private func makeImageCell(geometry: GeometryProxy, image: UIImage) -> some View {
        image.suImage
            .resizable()
            .scaledToFit()
            .frame(width: geometry.size.width, height: geometry.size.width)
            .clipped()
    }
}

// struct PhotoDetailsView_Previews: PreviewProvider {
//    static var previews: some View {
//        PhotoDetailsView(viewModel: .init(container: .preview, images: [])) { _, _ in
//        }
//    }
// }
