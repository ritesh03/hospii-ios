//
//  ContactView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/14/21.
//

import ChatSDK
import MessagingSDK
import SwiftUI
import UIKit
import ZendeskCoreSDK

struct ContactViewController: UIViewControllerRepresentable {
    typealias UIViewControllerType = UIViewController

    func makeUIViewController(context _: Context) -> UIViewController {
        let messagingConfiguration = MessagingConfiguration()
        messagingConfiguration.name = "Hospii"

        let chatConfiguration = ChatConfiguration()
        // We should already have this information from the User model
        chatConfiguration.preChatFormConfiguration.name = .hidden
        chatConfiguration.preChatFormConfiguration.email = .hidden
        chatConfiguration.preChatFormConfiguration.phoneNumber = .hidden

        do {
            // Build view controller
            let chatEngine = try ChatEngine.engine()
            let viewController = try Messaging.instance.buildUI(engines: [chatEngine], configs: [messagingConfiguration, chatConfiguration])

            return viewController
        } catch {
            return UIViewController()
        }
    }

    func updateUIViewController(_: UIViewController, context _: Context) {}
}
