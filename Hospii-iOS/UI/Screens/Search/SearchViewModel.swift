//
//  SearchViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/24/21.
//

import CoreLocation
import CoreStore
import SwiftUI

// MARK: - SearchViewModel

extension SearchView {
    class ViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
        // State
        @Published var footerRefreshing: Bool = false

        @Published var headerRefreshing: Bool = false

        @Published var searchRequest = SearchRequest()

        @Published var searchFeedsResponse: Loadable<SearchFeedsResponse>

        @Published var sortingSuggestionsResponse: Loadable<[SortingSuggestionResponse]>

        @Published var isPresentedSearchResultsView: Bool = false

        @Published var isPresentedSearchSuggestionsView = false

        @AppStorage(StorageKeys.presentedLoacationRequest.rawValue) var presentedLoacationRequest: Bool = false

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer) {
            self.container = container
            let appState = container.appState

            _searchFeedsResponse = .init(initialValue: appState[\.userData.feeds])
            _sortingSuggestionsResponse = .init(initialValue: appState[\.userData.sortingSuggestions])

            super.init()

            CLLocationManager().delegate = self

            cancelBag.collect {
                $searchFeedsResponse
                    .sink { [weak self] response in
                        self?.searchFeedsResponseChanged(response: response)
                    }

                $searchFeedsResponse
                    .filter { $0.isLoaded }
                    .sink { appState[\.userData.feeds] = $0 }

                //                appState.map(\.userData.feeds)
                //                    .removeDuplicates()
                //                    .weakAssign(to: \.searchFeedsResponse, on: self)

                $sortingSuggestionsResponse
                    .filter { $0.isLoaded }
                    .sink { appState[\.userData.sortingSuggestions] = $0 }

                //                appState.map(\.userData.sortingSuggestions)
                //                    .removeDuplicates()
                //                    .weakAssign(to: \.sortingSuggestionsResponse, on: self)

                NotificationCenter.default.publisher(for: Notification.Name(NotificationEvents.locationRequestClosed.event), object: nil)
                    .sink { [weak self] _ in
                        self?.loadFeeds()
                    }
            }
        }

        func reloadFeeds() {
            searchRequest.noMore = false
            searchRequest.page = 1
            footerRefreshing = false
            loadFeeds()
        }

        func initFeeds() {
            if CLLocationManager().authorizationStatus == .denied || CLLocationManager().authorizationStatus == .restricted || (presentedLoacationRequest && CLLocationManager().authorizationStatus == .notDetermined) {
                loadFeeds()
            } else if CLLocationManager().authorizationStatus == .authorizedAlways || CLLocationManager().authorizationStatus == .authorizedWhenInUse {
                updateLocation(CLLocationManager())
            }
        }

        func loadFeeds() {
            searchFeedsResponse.cancelTask()
            container.services.searchService.getFeeds(response: loadableSubject(\.searchFeedsResponse), request: searchRequest)
        }

        func loadSortingSuggestions() {
            container.services.searchService.getSortingSuggestions(response: loadableSubject(\.sortingSuggestionsResponse))
        }

        func searchFeedsResponseChanged(response: Loadable<SearchFeedsResponse>) {
            switch response {
            case let .loaded(body):
                headerRefreshing = false
                footerRefreshing = false

                if body.meta.itemCount == 0 {
                    searchRequest.noMore = true
                } else {
                    searchRequest.page = searchRequest.page + 1
                }
            case .failed:
                footerRefreshing = false
            default:
                break
            }
        }

        func selectedFeed(itemId: Int) {
            searchRequest.selectedItemId = itemId
            isPresentedSearchResultsView = true
        }

        fileprivate func updateLocation(_ manager: CLLocationManager) {
            let locValue = manager.location?.coordinate
            if let latitude = locValue?.latitude, let longitude = locValue?.longitude {
                searchRequest.latitude = latitude
                searchRequest.longitude = longitude

                reloadFeeds()
            } else {
                reloadFeeds()
            }
        }

        func locationManager(_: CLLocationManager, didChangeAuthorization _: CLAuthorizationStatus) {
            //            if status == .authorizedAlways || status == .authorizedWhenInUse {
            //                updateLocation(manager)
            //            }
        }

        func selectSorting(sortingSuggestion: SortingSuggestionResponse) {
            let suggestion = sortingSuggestion.suggestion

            if suggestion == searchRequest.sort {
                searchRequest.sort = nil
                reloadFeeds()
            } else {
                searchRequest.sort = suggestion
                reloadFeeds()
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
