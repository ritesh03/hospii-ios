//
//  PhotoListViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Moya
import PhoneNumberKit
import SwiftUI

// MARK: - PhotoListViewModel

extension SearchResultsListView {
    class ViewModel: ObservableObject {
        // State
        @Published var searchRequest: SearchRequest

        @Published var searchFeedsResponse: Loadable<SearchFeedsResponse>

        @Published var footerRefreshing: Bool = false

        @Published var headerRefreshing: Bool = false

        @Published var postViewResponse: Loadable<Response>

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer
        let phoneNumberKit = PhoneNumberKit()

        init(container: DIContainer,
             searchRequest: SearchRequest,
             searchFeedsResponse: Loadable<SearchFeedsResponse>,
             postViewResponse: Loadable<Response> = .notRequested)
        {
            self.container = container
            self.searchRequest = searchRequest
            self.searchFeedsResponse = searchFeedsResponse
            _postViewResponse = .init(initialValue: postViewResponse)

            cancelBag.collect {
                $searchFeedsResponse
                    .sink { [weak self] response in
                        self?.searchFeedsResponseChanged(response: response)
                    }
            }
        }

        func reloadFeeds() {
            searchRequest.noMore = false
            searchRequest.page = 1
            footerRefreshing = false
            loadFeeds()
        }

        func loadFeeds() {
            searchFeedsResponse.cancelTask()
            container.services.searchService.getFeeds(response: loadableSubject(\.searchFeedsResponse), request: searchRequest)
        }

        func postView(feed: PostResponse?) {
            guard let feed = feed else {
                return
            }

            container.services.mediaService.postView(response: loadableSubject(\.postViewResponse), postId: feed.id)
        }

        func searchFeedsResponseChanged(response: Loadable<SearchFeedsResponse>) {
            switch response {
            case let .loaded(body):
                headerRefreshing = false
                footerRefreshing = false

                if !body.data.isEmpty {
                    searchRequest.page = searchRequest.page + 1
                }

                if body.meta.itemCount == 0 {
                    searchRequest.noMore = true
                }
            case .failed:
                footerRefreshing = false
            default:
                break
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
