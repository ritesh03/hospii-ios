//
//  SearchResultsListView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Kingfisher
import SwiftUI

struct SearchResultsListView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        content
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: HStack {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }) {
                    R.image.back()!.suImage
                }

                Text(viewModel.searchRequest.tags.first ?? R.string.localizable.commonExplore())
                    .style(.navTitle)
                    .lineLimit(1)
                    .frame(maxWidth: 200)
            })
    }

    private func makeCell(width: CGFloat, post: PostResponse) -> some View {
        let business = post.business!
        let media = post.__medias__?.first
        let photoWidth = media?.width ?? 0
        let photoHeight = media?.height ?? 0

        return VStack(spacing: 0) {
            NavigationLink(
                destination: BusinessProfileView(viewModel: .init(container: viewModel.container, business: business))
            ) {
                VStack(spacing: 0) {
                    KFImage
                        .url(URL(string: media?.imageUrl ?? ""))
                        .setProcessor(ImageProcessors.thumbnailProcessor)
                        .resizable()
                        .scaledToFit()
                        .frame(width: width, height: photoWidth > 0 && photoHeight > 0 ? (width * (photoHeight / photoWidth)) : width)
                        .clipped()

                    BusinessPreviewView(business: business)
                        .padding(.top, 8)
                        .padding(.bottom, 12)
                }
            }
            .buttonStyle(FlatLinkStyle())

            BusinessInformationView(business: business, phoneNumberKit: viewModel.phoneNumberKit)
                .padding(.horizontal)
                .padding(.vertical, 4)
                .padding(.bottom, 24)
        }
    }

    private var content: AnyView {
        switch viewModel.searchFeedsResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last?.data))
        case let .loaded(feeds): return AnyView(loadedView(feeds.data, showLoading: false))
        case let .cache(feeds): return AnyView(loadedView(feeds.data, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Loading Content

private extension SearchResultsListView {
    var notRequestedView: some View {
        EmptyView()
    }

    func loadingView(_ previouslyLoaded: [PostResponse]?) -> some View {
        if let feeds = previouslyLoaded {
            return AnyView(loadedView(feeds, showLoading: true))
        } else {
            return AnyView(ActivityIndicatorView().padding())
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.reloadFeeds()
        })
    }
}

// MARK: - Displaying Content

private extension SearchResultsListView {
    func loadedView(_ feeds: [PostResponse], showLoading _: Bool) -> some View {
        GeometryReader { geoReader in
            ScrollViewReader { proxy in
                PullToRefreshView(header: SimpleRefreshingView(),
                                  footer: SimpleRefreshingView(),
                                  isHeaderRefreshing: $viewModel.headerRefreshing,
                                  isFooterRefreshing: $viewModel.footerRefreshing,
                                  isNoMoreData: $viewModel.searchRequest.noMore,
                                  onHeaderRefresh: { viewModel.reloadFeeds() },
                                  onFooterRefresh: { viewModel.loadFeeds() }) {
                    LazyVStack {
                        ForEach(feeds, id: \.id) { feed in
                            makeCell(width: geoReader.size.width, post: feed)
                                .tag(feed.id)
                        }
                    }
                }
                .onLoad {
                    if viewModel.searchRequest.selectedItemId != -1 {
                        viewModel.postView(feed: feeds.first { $0.id == viewModel.searchRequest.selectedItemId })
                        proxy.scrollTo(viewModel.searchRequest.selectedItemId, anchor: .top)
                    }
                }
            }
        }
    }
}

struct SearchResultsListView_Previews: PreviewProvider {
    static var previews: some View {
        SearchResultsListView(viewModel: .init(container: .preview,
                                               searchRequest: SearchRequest(),
                                               searchFeedsResponse: .notRequested))
    }
}
