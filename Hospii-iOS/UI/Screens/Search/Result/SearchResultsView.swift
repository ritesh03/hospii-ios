//
//  SearchResultsView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/17/21.
//

import Kingfisher
import SwiftUI

struct SearchResultsView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack(spacing: 0) {
            NavigationLink(destination: SearchResultsListView(viewModel: .init(container: viewModel.container, searchRequest: viewModel.searchRequest, searchFeedsResponse: viewModel.searchFeedsResponse)), isActive: $viewModel.isPresentedSearchResultsView)
                {
                    EmptyView()
                }

            if viewModel.resultLayoutType == .grid {
                ZStack {
                    gridViewContent
                }
                .onLoad {
                    viewModel.initFeeds()
                }
            } else {
                ZStack {
                    listViewContent
                }
                .onLoad {
                    viewModel.initBusinesses()
                }
            }
        }
        .sheet(isPresented: $viewModel.isPresentedSearchResultsFilters) {
            SearchResultFiltersView(viewModel: .init(container: viewModel.container))
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: barLeadingItem, trailing: barTrailingItem)
    }

    private var barLeadingItem: some View {
        HStack {
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                R.image.back()!.suImage
            }

            Text(viewModel.title)
                .style(.navTitle)
                .lineLimit(1)
                .frame(maxWidth: 200)
        }
    }

    private var barTrailingItem: some View {
        HStack(spacing: 4) {
            Button(action: {
                viewModel.isPresentedSearchResultsFilters = true
            }) {
                R.image.filterIcon()!.suImage
            }

            if viewModel.resultLayoutType == .grid {
                Button(action: {
                    viewModel.resultLayoutType = .list
                }) {
                    R.image.listViewIcon()!.suImage
                }
            } else {
                Button(action: {
                    viewModel.resultLayoutType = .grid
                }) {
                    R.image.gridViewIcon()!.suImage
                }
            }
        }
    }

    private func makeCell(post: PostResponse, width: CGFloat) -> some View {
        let media = post.__medias__?.first
        let width = max(0, width)
        return KFImage
            .url(URL(string: media?.imageUrl ?? ""))
            .setProcessor(ImageProcessors.gridProcessor)
            .resizable()
            .scaledToFill()
            .frame(width: width, height: width)
            .clipped()
            .onTapGesture {
                viewModel.selectedFeed(itemId: post.id)
            }
    }

    private func makeBusinessCell(business: UserBusinessResponse) -> some View {
        NavigationLink(
            destination: BusinessProfileView(viewModel: .init(container: viewModel.container, business: business))
        ) {
            VStack(spacing: 0) {
                VStack(alignment: .leading) {
                    BusinessPreviewView(business: business)

                    Divider()
                }
            }
        }
        .buttonStyle(FlatLinkStyle())
    }

    private var gridViewContent: AnyView {
        switch viewModel.searchFeedsResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last?.data))
        case let .loaded(feeds): return AnyView(loadedView(feeds.data, showLoading: false))
        case let .cache(feeds): return AnyView(loadedView(feeds.data, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }

    private var listViewContent: AnyView {
        switch viewModel.searchBusinessesResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(listLoadingView(last?.data))
        case let .loaded(businesses): return AnyView(listLoadedView(businesses.data, showLoading: false))
        case let .cache(businesses): return AnyView(listLoadedView(businesses.data, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Loading Content

private extension SearchResultsView {
    var notRequestedView: some View {
        EmptyView()
    }

    func loadingView(_ previouslyLoaded: [PostResponse]?) -> some View {
        if let feeds = previouslyLoaded {
            return AnyView(loadedView(feeds, showLoading: true))
        } else {
            return AnyView(ActivityIndicatorView().padding())
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.loadFeeds()
        })
    }

    func listLoadingView(_ previouslyLoaded: [UserBusinessResponse]?) -> some View {
        if let businesses = previouslyLoaded {
            return AnyView(listLoadedView(businesses, showLoading: true))
        } else {
            return AnyView(ActivityIndicatorView().padding())
        }
    }
}

// MARK: - Displaying Content

private extension SearchResultsView {
    func loadedView(_ feeds: [PostResponse], showLoading _: Bool) -> some View {
        GeometryReader { geoReader in
            PullToRefreshView(header: SimpleRefreshingView(),
                              footer: SimpleRefreshingView(),
                              isHeaderRefreshing: $viewModel.headerRefreshing,
                              isFooterRefreshing: $viewModel.footerRefreshing,
                              isNoMoreData: $viewModel.searchRequest.noMore,
                              onHeaderRefresh: { viewModel.reloadFeeds() },
                              onFooterRefresh: { viewModel.loadFeeds() }) {
                VStack(alignment: .leading, spacing: 0) {
                    Text(viewModel.searchRequest.location ?? "")
                        .padding(.horizontal)
                        .padding(.vertical, 12)
                        .font(CustomFont.proximaNovaSemibold(withSize: 14))
                        .foregroundColor(R.color.text()!.suColor)

                    LazyVGrid(columns: [
                        GridItem(.flexible(), spacing: 1),
                        GridItem(.flexible(), spacing: 1),
                        GridItem(.flexible(), spacing: 1),
                    ], spacing: 1) {
                        ForEach(feeds, id: \.id) { feed in
                            makeCell(post: feed, width: (geoReader.size.width - 2) / 3)
                                .frame(maxWidth: .infinity)
                        }
                    }
                }
            }.padding(.top, 1)
        }
    }

    func listLoadedView(_ businesses: [UserBusinessResponse], showLoading _: Bool) -> some View {
        GeometryReader { _ in
            PullToRefreshView(header: SimpleRefreshingView(),
                              footer: SimpleRefreshingView(),
                              isHeaderRefreshing: $viewModel.businessHeaderRefreshing,
                              isFooterRefreshing: $viewModel.businessFooterRefreshing,
                              isNoMoreData: $viewModel.searchBusinessRequest.noMore,
                              onHeaderRefresh: { viewModel.reloadBusinesses() },
                              onFooterRefresh: { viewModel.loadBusinesses() }) {
                LazyVStack(alignment: .leading, spacing: 0) {
                    Text(viewModel.searchRequest.location ?? "")
                        .padding(.horizontal)
                        .padding(.vertical, 12)
                        .font(CustomFont.proximaNovaSemibold(withSize: 14))
                        .foregroundColor(R.color.text()!.suColor)

                    ForEach(businesses, id: \.id) { business in
                        makeBusinessCell(business: business)
                    }
                }
            }.padding(.top, 1)
        }
    }
}

struct SearchResultsView_Previews: PreviewProvider {
    static var previews: some View {
        SearchResultsView(viewModel: .init(container: .preview, searchRequest: SearchRequest()))
    }
}
