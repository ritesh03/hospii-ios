//
//  SearchResultsViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/17/21.
//

import CoreLocation
import CoreStore
import SwiftUI

// MARK: - SearchViewModel

extension SearchResultsView {
    class ViewModel: ObservableObject {
        // State
        @Published var footerRefreshing: Bool = false

        @Published var headerRefreshing: Bool = false

        @Published var businessFooterRefreshing: Bool = false

        @Published var businessHeaderRefreshing: Bool = false

        @Published var searchRequest: SearchRequest

        @Published var searchBusinessRequest: SearchRequest

        @Published var searchFeedsResponse: Loadable<SearchFeedsResponse>

        @Published var searchBusinessesResponse: Loadable<SearchBusinessesResponse>

        @Published var isPresentedSearchResultsView: Bool = false

        @Published var isPresentedSearchResultsFilters: Bool = false

        @Published var resultLayoutType: ResultLayoutType = .grid

        @Published var title: String
        
        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer, searchRequest: SearchRequest, searchFeedsResponse: Loadable<SearchFeedsResponse> = .notRequested, searchBusinessesResponse: Loadable<SearchBusinessesResponse> = .notRequested, title: String = "") {
            container.appState[\.userData].searchRequest = searchRequest

            self.container = container
            let appState = container.appState
            _searchRequest = .init(initialValue: container.appState.value.userData.searchRequest)
            _searchFeedsResponse = .init(initialValue: searchFeedsResponse)
            _searchBusinessesResponse = .init(initialValue: searchBusinessesResponse)
            _title = .init(initialValue: title)

            searchBusinessRequest = SearchRequest(tags: searchRequest.tags, services: searchRequest.services, latitude: searchRequest.latitude, longitude: searchRequest.longitude, page: 1, noMore: false, location: searchRequest.location, selectedItemId: searchRequest.selectedItemId, range: searchRequest.range)

            cancelBag.collect {
                $searchFeedsResponse
                    .sink { [weak self] response in
                        self?.searchFeedsResponseChanged(response: response)
                    }

                $searchBusinessesResponse
                    .sink { [weak self] response in
                        self?.searchBusinessesResponseChanged(response: response)
                    }

                $isPresentedSearchResultsFilters
                    .filter { !$0 }
                    .sink { [weak self] _ in
                        if self?.resultLayoutType == .grid {
                            self?.reloadFeeds()
                        } else {
                            self?.syncSearchBusinessRequest()
                            self?.reloadBusinesses()
                        }
                    }

                $searchRequest
                    .sink { appState[\.userData.searchRequest] = $0 }

                appState.map(\.userData.searchRequest)
                    .removeDuplicates()
                    .weakAssign(to: \.searchRequest, on: self)
            }
        }

        func syncSearchBusinessRequest() {
            searchBusinessRequest = SearchRequest(tags: searchRequest.tags, services: searchRequest.services, latitude: searchRequest.latitude, longitude: searchRequest.longitude, page: 1, noMore: false, location: searchRequest.location, selectedItemId: searchRequest.selectedItemId, range: searchRequest.range)
        }

        func reloadFeeds() {
            searchRequest.noMore = false
            searchRequest.page = 1
            footerRefreshing = false
            loadFeeds()
        }

        func initFeeds() {
            reloadFeeds()
        }

        func loadFeeds() {
            searchFeedsResponse.cancelTask()
            container.services.searchService.getSearchResults(response: loadableSubject(\.searchFeedsResponse), request: searchRequest)
        }

        func reloadBusinesses() {
            searchBusinessRequest.noMore = false
            searchBusinessRequest.page = 1
            businessFooterRefreshing = false
            loadBusinesses()
        }

        func initBusinesses() {
            reloadBusinesses()
        }

        func loadBusinesses() {
            searchBusinessesResponse.cancelTask()
            container.services.searchService.getBusinesses(response: loadableSubject(\.searchBusinessesResponse), request: searchBusinessRequest)
        }

        func selectedFeed(itemId: Int) {
            searchRequest.selectedItemId = itemId
            isPresentedSearchResultsView = true
        }

        private func searchFeedsResponseChanged(response: Loadable<SearchFeedsResponse>) {
            switch response {
            case let .loaded(body):
                headerRefreshing = false
                footerRefreshing = false

                if !body.data.isEmpty {
                    searchRequest.page = searchRequest.page + 1
                }

                if body.meta.itemCount == 0 {
                    searchRequest.noMore = true
                }
            case .failed:
                footerRefreshing = false
            default:
                break
            }
        }

        private func searchBusinessesResponseChanged(response: Loadable<SearchBusinessesResponse>) {
            switch response {
            case let .loaded(body):
                businessHeaderRefreshing = false
                businessFooterRefreshing = false

                if !body.data.isEmpty {
                    searchBusinessRequest.page = searchBusinessRequest.page + 1
                }

                if body.meta.itemCount == 0 {
                    searchBusinessRequest.noMore = true
                }
            case .failed:
                businessFooterRefreshing = false
            default:
                break
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
