//
//  SearchView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/24/21.
//

import Kingfisher
import SwiftUI

struct SearchView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 0) {
                NavigationLink(destination: SearchResultsListView(viewModel: .init(container: viewModel.container, searchRequest: viewModel.searchRequest, searchFeedsResponse: viewModel.searchFeedsResponse)), isActive: $viewModel.isPresentedSearchResultsView)
                    {
                        EmptyView()
                    }

                if viewModel.isPresentedSearchSuggestionsView {
                    SearchSuggestionsView(viewModel: .init(container: viewModel.container), isPresented: $viewModel.isPresentedSearchSuggestionsView)
                } else {
                    content
                }
            }
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(true)
        }
    }

    private func makeHeaderView() -> some View {
        return VStack(spacing: 0) {
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack {
                    ForEach(viewModel.sortingSuggestionsResponse.value ?? [], id: \.suggestion) { suggestion in
                        makeQuickSuggestionCell(sortingSuggestions: suggestion)
                    }
                }
                .padding(.horizontal, 18)
                .padding(.top, 12)
            }

            Divider()
        }
        .background(R.color.white()!.suColor)
    }

    private func makeCell(post: PostResponse, width: CGFloat) -> some View {
        let media = post.__medias__?.first
        let width = max(0, width)
        return KFImage
            .url(URL(string: media?.imageUrl ?? ""))
            .setProcessor(ImageProcessors.gridProcessor)
            .resizable()
            .scaledToFill()
            .frame(width: width, height: width)
            .clipped()
            .onTapGesture {
                viewModel.selectedFeed(itemId: post.id)
            }
    }

    private func makeQuickSuggestionCell(sortingSuggestions: SortingSuggestionResponse) -> some View {
        return ZStack {
            Button(action: {
                viewModel.selectSorting(sortingSuggestion: sortingSuggestions)
            }) {
                Text(sortingSuggestions.displayName)
            }
            .padding(.horizontal, 12)
            .padding(.vertical, 10)
            .contentShape(Rectangle())
            .style(.textFieldLabel)
            .background(viewModel.searchRequest.sort == sortingSuggestions.suggestion ? R.color.yellowAlpha50()!.suColor : R.color.clear()!.suColor)
            .cornerRadius(.infinity)
            .overlay(
                RoundedRectangle(cornerRadius: .infinity)
                    .stroke(lineWidth: 1)
                    .foregroundColor(R.color.gray219()!.suColor)
            )
            .padding(.bottom, 12)
        }
    }

    private var content: AnyView {
        switch viewModel.searchFeedsResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last?.data))
        case let .loaded(feeds): return AnyView(loadedView(feeds.data, showLoading: false))
        case let .cache(feeds): return AnyView(loadedView(feeds.data, showLoading: false)
                .onAppear {
                    viewModel.initFeeds()
                    viewModel.loadSortingSuggestions()
                })
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Loading Content

private extension SearchView {
    var notRequestedView: some View {
        ZStack {}
            .onAppear {
                viewModel.initFeeds()
                viewModel.loadSortingSuggestions()
            }
    }

    func loadingView(_ previouslyLoaded: [PostResponse]?) -> some View {
        if let feeds = previouslyLoaded {
            return AnyView(loadedView(feeds, showLoading: true))
        } else {
            return AnyView(LoadingView())
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.loadFeeds()
        })
    }
}

// MARK: - Displaying Content

private extension SearchView {
    func loadedView(_ feeds: [PostResponse], showLoading _: Bool) -> some View {
        GeometryReader { geoReader in
            PullToRefreshView(header: SimpleRefreshingView(),
                              footer: SimpleRefreshingView(),
                              isHeaderRefreshing: $viewModel.headerRefreshing,
                              isFooterRefreshing: $viewModel.footerRefreshing,
                              isNoMoreData: $viewModel.searchRequest.noMore,
                              onHeaderRefresh: { viewModel.reloadFeeds() },
                              onFooterRefresh: { viewModel.loadFeeds() }) {
                VStack(alignment: .leading, spacing: 0) {
                    RoundedSearchBar {
                        self.viewModel.isPresentedSearchSuggestionsView.toggle()
                    }.padding(.horizontal)

                    LazyVGrid(columns: [
                        GridItem(.flexible(), spacing: 1),
                        GridItem(.flexible(), spacing: 1),
                        GridItem(.flexible(), spacing: 1),
                    ], spacing: 1, pinnedViews: .sectionHeaders) {
                        Section(header: makeHeaderView()) {
                            //                            Text(R.string.localizable.searchViewPageDescriptionText())
                            //                                .padding(.horizontal)
                            //                                .padding(.vertical, 12)
                            //                                .style(.listSectionHeader)
                            ForEach(feeds, id: \.id) { feed in
                                makeCell(post: feed, width: (geoReader.size.width - 2) / 3)
                            }
                        }
                    }
                }
            }
            .padding(.top, 1)
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(viewModel: .init(container: .preview))
    }
}
