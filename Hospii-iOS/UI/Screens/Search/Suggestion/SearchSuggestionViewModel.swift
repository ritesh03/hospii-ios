//
//  SearchSuggestionViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/24/21.
//

import CoreLocation
import MapboxGeocoder
import OrderedCollections
import SwiftUI

// MARK: - Loacation Request

struct LoacationRequest {
    var locationKeyword = ""

    var locationPlaceholder = R.string.localizable.searchSuggestionsViewSearchBarTitle()

    var isLocationMode = false

    var isLoading: Bool = false

    var addresses: OrderedDictionary<GeocodedPlacemark, String?> = [:]
}

// MARK: - SearchSuggestionsViewModel

extension SearchSuggestionsView {
    class ViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
        // State
        @Published var searchRequest = SearchSuggestionsRequest()

        @Published var searchResponse: Loadable<SuggestionsResponse>

        @Published var locationRequest = LoacationRequest()

        @Published var locationPlaceholderColor = R.color.gray119()!.suColor

        // Misc
        private var task: URLSessionDataTask?
        private let geocoder = Geocoder(accessToken: ApiConstants.MAPBOX_KEY)
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer, searchResponse: Loadable<SuggestionsResponse> = .notRequested) {
            self.container = container
            _searchResponse = .init(initialValue: searchResponse)

            super.init()

            CLLocationManager().delegate = self

            cancelBag.collect {
                $searchRequest
                    .compactMap { $0.keyword }
                    .removeDuplicates()
                    .debounce(for: .milliseconds(800), scheduler: DispatchQueue.main)
                    .sink { [weak self] _ in
                        self?.loadSuggestions()
                        if self?.locationRequest.isLocationMode == true {
                            self?.locationRequest.isLocationMode = false
                        }
                    }

                $locationRequest
                    .compactMap { $0.locationKeyword }
                    .removeDuplicates()
                    .debounce(for: .milliseconds(800), scheduler: DispatchQueue.main)
                    .sink { [weak self] keyword in
                        if keyword.isEmpty {
                            if self?.locationRequest.isLocationMode == true {
                                self?.locationRequest.isLocationMode = false
                            }
                            return
                        }
                        self?.searchLocations()
                        if self?.locationRequest.isLocationMode == false {
                            self?.locationRequest.isLocationMode = true
                        }
                    }
            }
        }

        func loadLocation() {
            if CLLocationManager().authorizationStatus == .authorizedAlways || CLLocationManager().authorizationStatus == .authorizedWhenInUse {
                let locValue = CLLocationManager().location?.coordinate
                if let latitude = locValue?.latitude, let longitude = locValue?.longitude {
                    searchRequest.latitude = latitude
                    searchRequest.longitude = longitude
                    locationRequest.locationPlaceholder = R.string.localizable.commonCurrentLocation()
                    searchRequest.location = R.string.localizable.commonCurrentLocation()
                    locationPlaceholderColor = R.color.blue()!.suColor
                }
            }
        }

        func loadSuggestions() {
            if searchRequest.keyword.isEmpty {
                searchResponse = .notRequested
                return
            }
            searchResponse.cancelTask()
            container
                .services
                .searchService
                .getSuggestions(response: loadableSubject(\.searchResponse), request: searchRequest)
        }

        func locationManager(_: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            if status == .authorizedAlways || status == .authorizedWhenInUse {
                if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                    if CLLocationManager.isRangingAvailable() {}
                }
            }
        }

        func locationManager(_: CLLocationManager, didUpdateLocations _: [CLLocation]) {}

        func locationManager(_: CLLocationManager, didFailWithError _: Error) {
            if CLLocationManager().authorizationStatus == .denied || CLLocationManager().authorizationStatus == .restricted {}
        }

        func selectLocation(latitude: Double, longitude: Double, location: String) {
            searchRequest.latitude = latitude
            searchRequest.longitude = longitude
            searchRequest.location = location
            locationRequest.isLocationMode = false
            locationRequest.locationKeyword = ""
            locationRequest.locationPlaceholder = location
            locationPlaceholderColor = R.color.gray119()!.suColor

            loadSuggestions()
        }

        func selectCurrentLocation() {
            CLLocationManager().requestWhenInUseAuthorization()
            locationRequest.locationKeyword = ""
            locationRequest.locationPlaceholder = R.string.localizable.commonCurrentLocation()
            locationPlaceholderColor = R.color.blue()!.suColor
            loadLocation()
            loadSuggestions()
        }

        func searchLocations() {
            if locationRequest.locationKeyword.isEmpty {
                return
            }
            task?.cancel()
            locationRequest.isLoading = true
            let forwardGeocodeOptions = ForwardGeocodeOptions(query: locationRequest.locationKeyword)
            forwardGeocodeOptions.allowedISOCountryCodes = ["US"]
            forwardGeocodeOptions.allowedScopes = [.place, .locality, .neighborhood]
            forwardGeocodeOptions.maximumResultCount = 10
            forwardGeocodeOptions.focalLocation = exposedLocation
            task = geocoder.geocode(forwardGeocodeOptions) { [weak self] locations, _, error in
                if let error = error {
                    debugPrint("Error when trying to geocode query: \(error.localizedDescription)")
                } else if let locations = locations, !locations.isEmpty {
                    var autoCompletedAddressResults: OrderedDictionary<GeocodedPlacemark, String?> = [:]
                    for location in locations {
                        if location.location?.coordinate.latitude != nil, location.location?.coordinate.longitude != nil {
                            autoCompletedAddressResults[location] = "\(location.formattedName), \(location.postalAddress?.state ?? "")"
                        }
                    }

                    self?.locationRequest.addresses = autoCompletedAddressResults
                }
                self?.locationRequest.isLoading = false
            }
        }

        private var exposedLocation: CLLocation? {
            return CLLocationManager().location
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
