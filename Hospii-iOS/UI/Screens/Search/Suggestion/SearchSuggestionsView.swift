//
//  SearchSuggestionsView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/24/21.
//

import SwiftUI

struct SearchSuggestionsView: View {
    @StateObject var viewModel: ViewModel
    @Binding var isPresented: Bool

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            ScrollView {
                LazyVStack(spacing: 0, pinnedViews: .sectionHeaders) {
                    Section(header: makeHeaderView()) {
                        if #available(iOS 15.0, *) {
                            RoundedSearchBarTextField(input: $viewModel.locationRequest.locationKeyword, image: R.image.autoLocationIcon()!.suImage, title: $viewModel.locationRequest.locationPlaceholder, placeholderColor: $viewModel.locationPlaceholderColor) { _ in
                            }
                            .padding(.horizontal)
                            .padding(.bottom, 8)
                        } else {
                            RoundedSearchBarTextFieldIOS14(input: $viewModel.locationRequest.locationKeyword, image: R.image.autoLocationIcon()!.suImage, title: $viewModel.locationRequest.locationPlaceholder) { _ in
                            }
                            .padding(.horizontal)
                            .padding(.bottom, 8)
                        }

                        if !viewModel.locationRequest.isLocationMode {
                            content
                        } else {
                            locationsView
                        }
                    }
                }
            }
            .padding(.top, 1)
        }
        .onLoad {
            viewModel.loadLocation()
        }
    }

    private func makeHeaderView() -> some View {
        return HStack {
            if #available(iOS 15.0, *) {
                RoundedSearchBarTextField(isFocused: true, input: $viewModel.searchRequest.keyword, title: .constant(R.string.localizable.searchViewSearchBarTitle()), placeholderColor: .constant(R.color.gray119()!.suColor)) { _ in
                }
                .padding(.leading)
            } else {
                RoundedSearchBarTextFieldIOS14(isFocused: true, input: $viewModel.searchRequest.keyword, title: .constant(R.string.localizable.searchViewSearchBarTitle())) { _ in
                }
                .padding(.leading)
            }

            Button(action: {
                isPresented = false
            }) {
                Text(R.string.localizable.commonCancelButtonTitle())
                    .style(.actionButton)
            }
        }
        .padding(.bottom, 8)
        .background(R.color.white()!.suColor)
        .padding(.trailing)
    }

    private var content: AnyView {
        switch viewModel.searchResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last))
        case let .loaded(response): return AnyView(loadedView(response, showLoading: false))
        case let .cache(response): return AnyView(loadedView(response, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }

    private func makeTagCell(tag: TagResponse) -> some View {
        VStack(alignment: .leading) {
            NavigationLink(
                destination: SearchResultsView(viewModel: .init(container: viewModel.container, searchRequest: SearchRequest(tags: [tag.tag], services: [], latitude: viewModel.searchRequest.latitude, longitude: viewModel.searchRequest.longitude, page: 1, noMore: false, location: viewModel.searchRequest.location), title: tag.original))
            ) {
                HStack(alignment: .center) {
                    R.image.searchIcon()!.suImage
                    Text(tag.original)
                        .padding(.leading, 12)
                        .font(CustomFont.proximaNovaLight(withSize: 16))
                        .foregroundColor(R.color.text()!.suColor)
                        .lineLimit(2)
                        .minimumScaleFactor(0.01)
                    Spacer()
                }
                .frame(maxWidth: .infinity)
            }
            .padding(.vertical, 8)

            Divider()
        }
        .padding(.horizontal)
    }

    private func makeBusinessCell(business: UserBusinessResponse) -> some View {
        NavigationLink(
            destination: BusinessProfileView(viewModel: .init(container: viewModel.container, business: business))
        ) {
            VStack(spacing: 0) {
                VStack(alignment: .leading) {
                    BusinessPreviewView(business: business)

                    Divider()
                }
            }
        }
        .buttonStyle(FlatLinkStyle())
    }

    private func cellLocationView(index: Int) -> some View {
        let address = Array(viewModel.locationRequest.addresses.values)[index]
        return HStack(alignment: .center) {
            R.image.mapIcon()!.suImage
            Text(address ?? "")
                .padding(.leading, 10)
                .font(CustomFont.proximaNovaLight(withSize: 16))
                .foregroundColor(R.color.text()!.suColor)
                .lineLimit(2)
                .minimumScaleFactor(0.01)
            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 6)
        .onTapGesture {
            let geocodedPlacemark = Array(viewModel.locationRequest.addresses.keys)[index]
            viewModel.selectLocation(latitude: geocodedPlacemark.location!.coordinate.latitude, longitude: geocodedPlacemark.location!.coordinate.longitude, location: address ?? "")
        }
    }
}

// MARK: - Loading Content

private extension SearchSuggestionsView {
    var notRequestedView: some View {
        EmptyView()
    }

    func loadingView(_ previouslyLoaded: SuggestionsResponse?) -> some View {
        if let response = previouslyLoaded {
            return AnyView(loadedView(response, showLoading: true))
        } else {
            return AnyView(ActivityIndicatorView().padding())
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.loadSuggestions()
        })
    }
}

// MARK: - Displaying Content

private extension SearchSuggestionsView {
    func loadedView(_ response: SuggestionsResponse, showLoading: Bool) -> some View {
        LazyVStack {
            if showLoading {
                ActivityIndicatorView().padding()
            }

            if response.tags?.isEmpty != false, response.businesses?.isEmpty != false, !viewModel.searchResponse.isLoading, !viewModel.searchRequest.keyword.isEmpty {
                Text("No results for: \(viewModel.searchRequest.keyword)")
                    .style(.listSectionHeader)
                    .padding()
            }

            ForEach(response.tags ?? [], id: \.tag) { tag in
                makeTagCell(tag: tag)
            }

            ForEach(response.businesses ?? [], id: \.id) { business in
                makeBusinessCell(business: business)
            }
        }
        .padding(.top, 8)
        .frame(maxWidth: .infinity)
    }

    private var locationsView: some View {
        LazyVStack {
            if viewModel.locationRequest.isLoading {
                ActivityIndicatorView().padding()
            }

            currentLocationView

            ForEach(Array(viewModel.locationRequest.addresses.values).indices, id: \.self) { index in
                cellLocationView(index: index)
                    .frame(maxWidth: .infinity)
            }
        }
        .padding(.top, 8)
        .frame(maxWidth: .infinity)
    }

    private var currentLocationView: some View {
        HStack(alignment: .center) {
            R.image.mapIcon()!.suImage
                .renderingMode(.template)
                .foregroundColor(R.color.blue()!.suColor)
            Text(R.string.localizable.commonCurrentLocation())
                .padding(.leading, 10)
                .font(CustomFont.proximaNovaLight(withSize: 16))
                .foregroundColor(R.color.blue()!.suColor)
                .lineLimit(2)
                .minimumScaleFactor(0.01)
            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 6)
        .onTapGesture {
            viewModel.selectCurrentLocation()
        }
    }
}

struct SearchSuggestionsView_Previews: PreviewProvider {
    static var previews: some View {
        SearchSuggestionsView(viewModel: .init(container: .preview), isPresented: .constant(false))
    }
}
