//
//  CircleView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import SwiftUI

struct CircleView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        VStack(spacing: 24) {
            R.image.searchLocationEmpty()!.suImage

            Text(R.string.localizable.circleViewPageTitleText())
                .style(.sectionHeader)
                .multilineTextAlignment(.center)

            Text(R.string.localizable.circleViewPageTitlePageDescriptionText())
                .style(.sectionSubtitle)
                .multilineTextAlignment(.center)

            //            RoundedButton(style: .primaryButton, title: R.string.localizable.circleViewShareButtonTitle()) {
            //
            //            }
            Button(action: actionSheet) {
                Text(R.string.localizable.circleViewShareButtonTitle())
            }
            .padding(.vertical, 20)
            .padding(.horizontal, 64)
            .contentShape(Rectangle())
            .style(.actionButton)
            .foregroundColor(R.color.text()!.suColor)
            .background(R.color.yellow()!.suColor)
            .overlay(
                RoundedRectangle(cornerRadius: .infinity)
                    .stroke(lineWidth: 0)
                    .foregroundColor(.clear)
            )
            .cornerRadius(.infinity)
        }
        .padding()
    }

    func actionSheet() {
        guard let urlShare = URL(string: ApiConstants.SHARE_URL) else { return }
        let activityVC = UIActivityViewController(activityItems: [urlShare], applicationActivities: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(activityVC, animated: true, completion: nil)
    }
}

struct CircleView_Previews: PreviewProvider {
    static var previews: some View {
        CircleView(viewModel: .init(container: .preview))
    }
}
