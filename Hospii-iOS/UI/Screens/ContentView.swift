//
//  ContentView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/12/21.
//

import Combine
import SwiftUI

// MARK: - View

struct ContentView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        ZStack {
            if viewModel.isRunningTests {
                // Don't show any view when the app is running unit tests.
            } else {
                if viewModel.routingState.accessToken != nil {
                    MainView(viewModel: .init(container: viewModel.container))
                } else {
                    LoginView(viewModel: .init(container: viewModel.container))
                }
            }
        }
    }
}

// MARK: - Routing

extension ContentView {
    struct Routing: Equatable {
        var accessToken: String?
    }
}

// MARK: - ViewModel

extension ContentView {
    class ViewModel: ObservableObject {
        // State
        @Published var routingState: Routing

        // Misc
        let container: DIContainer
        let isRunningTests: Bool

        private var cancelBag = CancelBag()

        init(container: DIContainer, isRunningTests: Bool = ProcessInfo.processInfo.isRunningTests) {
            self.container = container
            let appState = container.appState
            self.isRunningTests = isRunningTests
            _routingState = .init(initialValue: appState.value.routing.contentView)

            cancelBag.collect {
                $routingState
                    .sink { appState[\.routing.contentView] = $0 }

                appState.map(\.routing.contentView)
                    .removeDuplicates()
                    .weakAssign(to: \.routingState, on: self)
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}

// MARK: - Preview

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: ContentView.ViewModel(container: .preview))
    }
}
