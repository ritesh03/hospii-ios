//
//  LoginView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/13/21.
//

import Combine
import Introspect
import ResponsiveTextField
import SwiftUI

// MARK: - LoginView

struct LoginView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        NavigationView {
            ZStack(alignment: .top) {
                NavigationLink(
                    destination: SignUpUsernameView(viewModel: .init(container: viewModel.container)),
                    isActive: $viewModel.isPresentedSignupView
                ) {
                    EmptyView()
                }

                NavigationLink(
                    destination: ForgotPasswordView(viewModel: .init(container: viewModel.container)),
                    isActive: $viewModel.isPresentedForgotPasswordView
                ) {
                    EmptyView()
                }

                ScrollView {
                    VStack(alignment: .leading, spacing: 16) {
                        Image(uiImage: R.image.hospiiSquareLogo()!)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 73, height: 73)

                        Text(R.string.localizable.logInViewPageTitleText())
                            .style(.sectionHeader)
                            .padding(.top, 12)

                        Text(R.string.localizable.logInViewPageDescriptionText())
                            .style(.sectionSubtitle)

                        PrimaryTextField(text: $viewModel.username, placeHolder: R.string.localizable.logInViewEmailMobileUsernameTextFieldPlaceholder(), title: R.string.localizable.logInViewEmailMobileUsernameTextFieldTitle(), error: viewModel.usernameError, firstResponderDemand: $viewModel.loginTextFieldFirstResponses.userNameResponderDemand, returnType: .next) {
                            viewModel.loginTextFieldFirstResponses.passwordFirstResponderDemand = .shouldBecomeFirstResponder
                        }
                        .padding(.top, 24)

                        PrimaryTextField(text: $viewModel.password, placeHolder: R.string.localizable.logInViewPasswordTextFieldPlaceholder(), title: R.string.localizable.logInViewPasswordTextFieldTitle(), error: viewModel.passwordError, style: .secureTextField, firstResponderDemand: $viewModel.loginTextFieldFirstResponses.passwordFirstResponderDemand, returnType: .join) {
                            viewModel.loginTextFieldFirstResponses.passwordFirstResponderDemand = .shouldResignFirstResponder
                            viewModel.login()
                        }
                        .padding(.top, 4)

                        Text(R.string.localizable.logInViewForgotPasswordButtonTitle())
                            .style(.subText)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .onTapGesture {
                                viewModel.isPresentedForgotPasswordView = true
                            }

                        RoundedButton(style: .primaryButton, isLoading: viewModel.loginResponse.isLoading, title: R.string.localizable.logInViewSignInButtonTitle()) {
                            viewModel.login()
                        }
                        .disabled(viewModel.loginResponse.isLoading)
                        .padding(.top, 20)

                        RoundedButton(style: .secondaryButton, title: R.string.localizable.logInViewSignUpButtonTitle()) {
                            viewModel.isPresentedSignupView = true
                        }
                        .disabled(viewModel.loginResponse.isLoading)
                        .padding(.top, 4)
                        .padding(.bottom, 20)
                    }
                    .frame(maxWidth: .infinity)
                    .padding(.vertical)
                    .padding(.horizontal, 20)
                }

                if viewModel.loginResponse.error != nil {
                    TopBannerAlertView(message: R.string.localizable.logInViewErrorBannerText())
                }
            }
            .introspectScrollView { scrollView in
                scrollView.alwaysBounceVertical = false
            }
            .navigationBarTitle("", displayMode: .large)
            .navigationBarHidden(true)
            .hideKeyboardOnTap()
        }
    }
}

// MARK: - Preview

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(viewModel: .init(container: .preview))
    }
}
