//
//  LoginViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/13/21.
//

import Combine
import Moya
import ResponsiveTextField
import SwiftUI

struct LoginTextFieldFirstResponses {
    var userNameResponderDemand: FirstResponderDemand?

    var passwordFirstResponderDemand: FirstResponderDemand?
}

// MARK: - LoginViewModel

extension LoginView {
    class ViewModel: ObservableObject {
        // State
        @Published var username: String = ""

        @Published var password: String = ""

        @Published var usernameError: String = ""

        @Published var passwordError: String = ""

        @Published var isPresentedAuthError: Bool = false

        @Published var isPresentedSignupView: Bool = false

        @Published var isPresentedForgotPasswordView: Bool = false

        @Published var loginResponse: Loadable<Tokens>

        @Published var loginTextFieldFirstResponses = LoginTextFieldFirstResponses()

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()

        init(container: DIContainer, loginResponse: Loadable<Tokens> = .notRequested) {
            self.container = container
            let appState = container.appState
            _loginResponse = .init(initialValue: loginResponse)
            cancelBag.collect {
                $username
                    .sink { [weak self] text in
                        self?.validate(username: text, isRevalidate: true)
                    }

                $password
                    .sink { [weak self] text in
                        self?.validate(password: text, isRevalidate: true)
                    }

                $loginResponse
                    .compactMap { $0.value?.accessToken }
                    .sink { appState[\.routing.contentView].accessToken = $0 }
            }
        }

        func login() {
            validate(username: username)
            validate(password: password)

            if !usernameError.isEmpty || !passwordError.isEmpty {
                return
            }

            container.services.authService
                .postLogin(response: loadableSubject(\.loginResponse), loginRequest: LoginRequest(username: username, password: password, grantType: "client_credentials"))
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}

extension LoginView.ViewModel {
    func validate(username: String?, isRevalidate: Bool = false) {
        if isRevalidate, usernameError.isEmpty {
            return
        }
        if let usernameValidation = username?.validateText(type: .username) as? UsernameValidationError {
            if usernameValidation == .empty {
                usernameError = R.string.localizable.usernameValidationErrorEmpty()
            } else {
                usernameError = ""
            }
        } else {
            usernameError = ""
        }
    }

    func validate(password: String?, isRevalidate: Bool = false) {
        if isRevalidate, passwordError.isEmpty {
            return
        }
        if let passwordValidation = password?.validateText(type: .password) as? PasswordValidationError {
            if passwordValidation == .empty {
                passwordError = R.string.localizable.passwordValidationErrorEmpty()
            } else {
                passwordError = ""
            }
        } else {
            passwordError = ""
        }
    }
}
