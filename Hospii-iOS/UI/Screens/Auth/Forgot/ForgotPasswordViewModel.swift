//
//  ForgotPasswordViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import Moya
import ResponsiveTextField
import SwiftUI
import Valet

// MARK: - SetPasswordViewModel

extension ForgotPasswordView {
    class ViewModel: ObservableObject {
        // State
        @Published var forgotPasswordRequest = ForgotPasswordRequest()

        @Published var initCodeResponse: Loadable<Response>

        @Published var emailError: String = ""

        @Published var phonenumberError: String = ""

        @Published var isPresentedOPTSheet = false

        @Published var token: String? = ""

        @Published var isPresentedResetPasswordView = false

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer, initCodeResponse: Loadable<Response> = .notRequested) {
            self.container = container
            _initCodeResponse = .init(initialValue: initCodeResponse)
            cancelBag.collect {
                $token
                    .compactMap { $0 }
                    .filter { !$0.isEmpty }
                    .sink { [weak self] _ in
                        self?.isPresentedResetPasswordView = true
                    }

                $initCodeResponse
                    .sink { [weak self] response in
                        self?.onResponseChange(response: response)
                    }
            }
        }

        func postInitVerificationCode() {
            emailError = Validator.shared.validate(email: forgotPasswordRequest.email)
            phonenumberError = Validator.shared.validate(phoneNumber: forgotPasswordRequest.phoneNumber)

            if emailError.isEmpty || phonenumberError.isEmpty {
                emailError = ""
                phonenumberError = ""
                container.services.authService.postInitiateForgotPasswordVerificationCode(response: loadableSubject(\.initCodeResponse), request: forgotPasswordRequest)
            }
        }

        private func onResponseChange(response: Loadable<Response>) {
            switch response {
            case .loaded:
                isPresentedOPTSheet = true
            default:
                break
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
