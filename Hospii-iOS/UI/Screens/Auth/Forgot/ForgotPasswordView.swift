//
//  ForgotPasswordView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import SwiftUI

struct ForgotPasswordView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        ZStack(alignment: .top) {
            NavigationLink(
                destination: ResetPasswordView(viewModel: .init(container: viewModel.container, updatePasswordRequest: ResetPasswordRequest(token: viewModel.token!, newPassword: ""))),
                isActive: $viewModel.isPresentedResetPasswordView
            ) {
                EmptyView()
            }

            ScrollView {
                VStack(alignment: .leading, spacing: 20) {
                    Text(R.string.localizable.forgotPasswordViewPageDescriptionText())
                        .style(.sectionSubtitle)

                    PrimaryTextField(text: $viewModel.forgotPasswordRequest.email, placeHolder: "", title: R.string.localizable.forgotPasswordViewEmailTextFieldTitle(), error: viewModel.emailError, style: .normalTextField, returnType: .next) {
                        viewModel.postInitVerificationCode()
                    }

                    HStack {
                        Spacer()

                        Text("or")
                            .padding(.bottom, -16)
                            .style(.sectionSubtitle)

                        Spacer()
                    }

                    PrimaryTextField(text: $viewModel.forgotPasswordRequest.phoneNumber, placeHolder: "", title: R.string.localizable.forgotPasswordViewPhoneNumberTextFieldTitle(), error: viewModel.phonenumberError, style: .phoneNumberTextField, returnType: .next) {
                        viewModel.postInitVerificationCode()
                    }

                    RoundedButton(style: .primaryButton, isLoading: viewModel.initCodeResponse.isLoading, title: R.string.localizable.forgotPasswordViewContinueButtonTitle()) {
                        viewModel.postInitVerificationCode()
                    }
                    .disabled(viewModel.initCodeResponse.isLoading)
                    .padding(.top, 20)

                    Spacer()
                }
                .padding(.vertical)
                .padding(.horizontal, 20)
                .modifier(NavigationBarModifier(title: R.string.localizable.forgotPasswordViewPageTitleText()))
                .sheet(isPresented: $viewModel.isPresentedOPTSheet) {
                    PhoneNumberVerificationView(viewModel: .init(container: viewModel.container, mode: .forgotPassword, forgotPasswordRequest: viewModel.forgotPasswordRequest), isPresented: $viewModel.isPresentedOPTSheet, token: $viewModel.token)
                }
                .hideKeyboardOnTap()
            }
        }
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView(viewModel: .init(container: .preview))
    }
}
