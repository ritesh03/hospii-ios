//
//  ResetPasswordViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import Moya
import ResponsiveTextField
import SwiftUI
import Valet

struct ResetPasswordViewFirstResponses {
    var newPasswordResponderDemand: FirstResponderDemand? = .shouldBecomeFirstResponder
}

// MARK: - SetPasswordViewModel

extension ResetPasswordView {
    class ViewModel: ObservableObject {
        // State
        @Published var updatePasswordRequest: ResetPasswordRequest

        @Published var passwordError: String = ""

        @Published var updateResponse: Loadable<Response>

        @Published var isPresentedSuccessAlert: Bool = false

        @Published var resetPasswordViewFirstResponses = ResetPasswordViewFirstResponses()

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer, updatePasswordRequest: ResetPasswordRequest = ResetPasswordRequest(), updateResponse: Loadable<Response> = .notRequested) {
            self.container = container
            _updatePasswordRequest = .init(initialValue: updatePasswordRequest)
            _updateResponse = .init(initialValue: updateResponse)
            cancelBag.collect {
                $updateResponse
                    .sink { [weak self] response in
                        switch response {
                        case .loaded:
                            self?.isPresentedSuccessAlert = true
                        default:
                            break
                        }
                    }
            }
        }

        func updatePassword() {
            passwordError = Validator.shared.validate(password: updatePasswordRequest.newPassword)

            if !passwordError.isEmpty || updatePasswordRequest.newPassword.isEmpty {
                return
            }

            container.services.authService.putResetPassword(response: loadableSubject(\.updateResponse), request: updatePasswordRequest)
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
