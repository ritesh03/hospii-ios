//
//  ResetPasswordView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/6/22.
//

import SwiftUI

struct ResetPasswordView: View {
    @StateObject var viewModel: ViewModel

    @Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Text(R.string.localizable.resetPasswordViewPageDescriptionText())
                .style(.sectionSubtitle)

            PrimaryTextField(text: $viewModel.updatePasswordRequest.newPassword, placeHolder: "", title: R.string.localizable.resetPasswordViewNewPasswordTextFieldTitle(), error: "", style: .secureTextField, firstResponderDemand: $viewModel.resetPasswordViewFirstResponses.newPasswordResponderDemand, returnType: .next) {
                viewModel.resetPasswordViewFirstResponses.newPasswordResponderDemand = .shouldBecomeFirstResponder
            }

            RoundedButton(style: .primaryButton, isLoading: viewModel.updateResponse.isLoading, title: R.string.localizable.resetPasswordViewUpdateButtonTitle()) {
                hideKeyboard()
                viewModel.updatePassword()
            }
            .disabled(viewModel.updateResponse.isLoading)
            .padding(.top, 20)

            Spacer()
        }
        .padding(.horizontal, 20)
        .modifier(NavigationBarModifier(title: R.string.localizable.resetPasswordViewPageTitleText()))
        .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.5, tapToDismiss: false, offsetY: 60, alert: {
            AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.resetPasswordViewAlertMessage())
        }, completion: {
            presentationMode.wrappedValue.dismiss()
        })
    }
}

struct ResetPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ResetPasswordView(viewModel: .init(container: .preview))
    }
}
