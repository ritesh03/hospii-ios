//
//  UsernameViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import Combine
import ResponsiveTextField
import SwiftUI

struct SignUpUsernameTextFieldFirstResponses {
    var userNameResponderDemand: FirstResponderDemand? = .shouldBecomeFirstResponder
}

// MARK: - SignUpUsernameViewModel

extension SignUpUsernameView {
    class ViewModel: ObservableObject {
        // State
        @Published var username: String = ""

        @Published var usernameError: String = ""

        @Published var isPresentedSignupView: Bool = false

        @Published var checkUserAvilibilityResponse: Loadable<CheckUsernameAvilibilityResponse>

        @Published var signUpUsernameTextFieldFirstResponses = SignUpUsernameTextFieldFirstResponses()

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()

        init(container: DIContainer, checkUserAvilibilityResponse: Loadable<CheckUsernameAvilibilityResponse> = .notRequested) {
            self.container = container
            _checkUserAvilibilityResponse = .init(initialValue: checkUserAvilibilityResponse)
            cancelBag.collect {
                $checkUserAvilibilityResponse
                    .sink { [weak self] response in
                        self?.onCheckUserAvilibilityResponseChange(response: response)
                    }

                $username
                    .sink { [weak self] text in
                        self?.validate(username: text, isRevalidate: true)
                    }
            }
        }

        func checkUserAvilibility() {
            validate(username: username)
            if !usernameError.isEmpty {
                return
            }
            container.services.authService
                .postCheckUsernameAvailability(response: loadableSubject(\.checkUserAvilibilityResponse), request: CheckUsernameAvilibilityRequest(username: username))
        }

        private func onCheckUserAvilibilityResponseChange(response: Loadable<CheckUsernameAvilibilityResponse>) {
            switch response {
            case let .loaded(response):
                if response.availability {
                    isPresentedSignupView = true
                } else {
                    usernameError = R.string.localizable.usernameValidationErrorTaken()
                }
            case .failed:
                usernameError = R.string.localizable.usernameValidationErrorTaken()
            default:
                break
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}

extension SignUpUsernameView.ViewModel {
    func validate(username: String?, isRevalidate: Bool = false) {
        if isRevalidate, usernameError.isEmpty {
            return
        }
        if let usernameValidation = username?.validateText(type: .username) as? UsernameValidationError {
            let errorMessage: String
            switch usernameValidation {
            case .empty:
                errorMessage = R.string.localizable.usernameValidationErrorEmpty()
            case .tooLong:
                errorMessage = R.string.localizable.usernameValidationErrorTooLong()
            case .tooShort:
                errorMessage = R.string.localizable.usernameValidationErrorTooShort()
            case .noSpecialCharacter:
                errorMessage = R.string.localizable.usernameValidationErrorNoSpecialCharacter()
            case .noPeriodStart:
                errorMessage = R.string.localizable.usernameValidationErrorNoPeriodStart()
            case .noPeriodEnd:
                errorMessage = R.string.localizable.usernameValidationErrorNoPeriodEnd()
            case .noTwoPeriodRow:
                errorMessage = R.string.localizable.usernameValidationErrorNoTwoPeriodRow()
            case .oneLetter:
                errorMessage = R.string.localizable.usernameValidationErrorOneLetter()
            }
            usernameError = errorMessage
        } else {
            usernameError = ""
        }
    }
}
