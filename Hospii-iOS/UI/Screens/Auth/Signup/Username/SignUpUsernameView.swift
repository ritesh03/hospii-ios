//
//  UsernameView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import ResponsiveTextField
import SwiftUI

struct SignUpUsernameView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        ZStack(alignment: .top) {
            NavigationLink(
                destination: SignupUserProfileView(viewModel: .init(container: viewModel.container, username: viewModel.username), action: {}),
                isActive: $viewModel.isPresentedSignupView
            ) {
                EmptyView()
            }

            ScrollView {
                VStack(alignment: .leading, spacing: 16) {
                    Text(R.string.localizable.signUpUsernameViewPageDescriptionText())
                        .style(.sectionSubtitle)

                    PrimaryTextField(text: $viewModel.username, placeHolder: R.string.localizable.signUpUsernameViewUsernameTextFieldPlaceholder(), title: R.string.localizable.signUpUsernameViewUsernameTextFieldTitle(), error: viewModel.usernameError, firstResponderDemand: $viewModel.signUpUsernameTextFieldFirstResponses.userNameResponderDemand, returnType: .next) {
                        viewModel.checkUserAvilibility()
                    }
                    .padding(.top, 16)

                    RoundedButton(style: .primaryButton, isLoading: viewModel.checkUserAvilibilityResponse.isLoading, title: R.string.localizable.signUpUsernameViewSubmitButtonTitle()) {
                        viewModel.signUpUsernameTextFieldFirstResponses.userNameResponderDemand = .shouldResignFirstResponder
                        viewModel.checkUserAvilibility()
                    }
                    .disabled(viewModel.checkUserAvilibilityResponse.isLoading)
                    .padding(.top, 4)
                    .padding(.bottom, 60)
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical)
                .padding(.horizontal, 20)
            }

            if viewModel.checkUserAvilibilityResponse.error != nil {
                TopBannerAlertView(message: R.string.localizable.topBannerAlertViewCommonErrorMessage())
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .modifier(NavigationBarModifier(title: R.string.localizable.signUpUsernameViewPageTitleText()))
        .hideKeyboardOnTap()
    }
}

struct UsernameView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpUsernameView(viewModel: .init(container: .preview))
    }
}
