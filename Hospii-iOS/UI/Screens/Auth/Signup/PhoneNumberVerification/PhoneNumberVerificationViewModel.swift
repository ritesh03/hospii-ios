//
//  PhoneNumberVerificationViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import Combine
import Introspect
import Moya
import SwiftUI

// MARK: - PhoneNumberVerificationViewModel

enum PhoneNumberVerificationMode {
    case register
    case forgotPassword
}

extension PhoneNumberVerificationView {
    class ViewModel: ObservableObject {
        // State
        @Published var verifyPhoneNumberResponse: Loadable<Response>

        @Published var verifyForgotPasswordCodeResponse: Loadable<ForgotPasswordResponse>

        @Published var initPhoneNumberVerificationResponse: Loadable<Response>

        @Published var resendCountDown = false

        @Published var resendTitle = R.string.localizable.phoneNumberVerificationViewResendButtonTitle()

        @Published var passCode = ""

        @Published var isPresentedSuccessAlert: Bool = false

        // Misc
        let container: DIContainer
        let mode: PhoneNumberVerificationMode
        private let countdownSeconds = 40
        private var cancelBag = CancelBag()
        private var cancellable: AnyCancellable?
        private var forgotPasswordRequest: ForgotPasswordRequest

        init(container: DIContainer, mode: PhoneNumberVerificationMode = .register, forgotPasswordRequest: ForgotPasswordRequest = ForgotPasswordRequest(), verifyPhoneNumberResponse: Loadable<Response> = .notRequested, initPhoneNumberVerificationResponse: Loadable<Response> = .notRequested, verifyForgotPasswordCodeResponse: Loadable<ForgotPasswordResponse> = .notRequested) {
            self.container = container
            self.mode = mode
            self.forgotPasswordRequest = forgotPasswordRequest

            _verifyPhoneNumberResponse = .init(initialValue: verifyPhoneNumberResponse)
            _initPhoneNumberVerificationResponse = .init(initialValue: initPhoneNumberVerificationResponse)
            _verifyForgotPasswordCodeResponse = .init(initialValue: verifyForgotPasswordCodeResponse)

            cancelBag.collect {
                $verifyPhoneNumberResponse
                    .sink { [weak self] response in
                        self?.onVerifyPhoneNumberResponseChange(response: response)
                    }
            }
        }

        func initPhoneNumberVerification() {
            container.services
                .authService
                .postInitPhoneNumberVerification(response: loadableSubject(\.initPhoneNumberVerificationResponse))
        }

        func initForgotVerificationCode() {
            container.services
                .authService
                .postInitiateForgotPasswordVerificationCode(response: loadableSubject(\.initPhoneNumberVerificationResponse), request: forgotPasswordRequest)
        }

        func resentOPT() {
            startCountDownTimer()
            switch mode {
            case .register:
                initPhoneNumberVerification()
            case .forgotPassword:
                initForgotVerificationCode()
            }
        }

        func startCountDownTimer() {
            cancellable?.cancel()
            resendCountDown = true
            resendTitle = "\(R.string.localizable.phoneNumberVerificationViewResendButtonTitle()) In \(countdownSeconds)"
            cancellable = Timer.TimerPublisher(interval: 1.0, runLoop: .main, mode: .default)
                .autoconnect()
                .scan(0) { counter, _ in
                    counter + 1
                }
                .sink { [weak self] counter in
                    guard let self = self else {
                        return
                    }
                    self.resendTitle = "\(R.string.localizable.phoneNumberVerificationViewResendButtonTitle()) In \(self.countdownSeconds - counter)"
                    if counter == self.countdownSeconds {
                        self.resendTitle = R.string.localizable.phoneNumberVerificationViewResendButtonTitle()
                        self.resendCountDown = false
                        self.cancellable?.cancel()
                    }
                }
        }

        func verifyPhoneNumber() {
            guard passCode.count == 6 else {
                return
            }

            switch mode {
            case .register:
                container.services
                    .authService
                    .postVerifyPhoneNumber(response: loadableSubject(\.verifyPhoneNumberResponse), code: passCode)
            case .forgotPassword:
                forgotPasswordRequest.code = passCode
                container.services
                    .authService
                    .postCheckForgotPasswordVerificationCode(response: loadableSubject(\.verifyForgotPasswordCodeResponse), request: forgotPasswordRequest)
            }
        }

        private func onVerifyPhoneNumberResponseChange(response: Loadable<Response>) {
            switch response {
            case .loaded:
                if !isPresentedSuccessAlert {
                    isPresentedSuccessAlert = true
                }
            default:
                break
            }
        }

        deinit {
            cancellable?.cancel()
            debugPrint("\(self) deinit")
        }
    }
}
