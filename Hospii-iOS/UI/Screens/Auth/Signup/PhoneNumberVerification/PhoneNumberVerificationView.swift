//
//  PhoneNumberVerificationView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import Introspect
import ResponsiveTextField
import SwiftUI

struct PhoneNumberVerificationView: View {
    @StateObject var viewModel: ViewModel

    @Binding var isPresented: Bool

    @Binding var token: String?

    @State var emailFirstResponderDemand: FirstResponderDemand? = .shouldBecomeFirstResponder

    var body: some View {
        VStack(spacing: 20) {
            RoundedRectangle(cornerRadius: 16)
                .fill(R.color.gray219()!.suColor)
                .frame(width: 60, height: 4)
                .padding(.top, 8)

            Text(R.string.localizable.phoneNumberVerificationViewPageTitleText())
                .style(.sectionHeader)
                .padding(.top, 4)

            Text(R.string.localizable.phoneNumberVerificationViewPageDescriptionText())
                .style(.sectionSubtitle)
                .multilineTextAlignment(.center)
                .minimumScaleFactor(0.5)
                .padding(.horizontal)

            ResponsiveTextField(
                placeholder: R.string.localizable.phoneNumberVerificationView6DigitCodeTitle(),
                text: $viewModel.passCode,
                firstResponderDemand: $emailFirstResponderDemand,
                configuration: .phoneNumberVerification
            )
            .responsiveTextFieldTextAlignment(.center)
            .accentColor(R.color.yellow()!.suColor)
            .frame(maxWidth: .infinity)
            .frame(height: 40)
            .padding(.top, 12)
            .onChange(of: viewModel.passCode, perform: { value in
                if value.count > 6 {
                    viewModel.passCode = String(value.prefix(6))
                }
            })

            Divider()
                .frame(height: 1)
                .padding(.top, -8)
                .background(viewModel.verifyPhoneNumberResponse.error != nil ? R.color.red()!.suColor : R.color.text()!.suColor)

            if viewModel.verifyPhoneNumberResponse.error != nil {
                Text(R.string.localizable.phoneNumberVerificationViewErrorWrongCode())
                    .style(.textFieldErrorMessage)
            }

            RoundedButton(style: .primaryButton, isLoading: viewModel.verifyPhoneNumberResponse.isLoading || viewModel.verifyForgotPasswordCodeResponse.isLoading, title: R.string.localizable.phoneNumberVerificationViewVerifyButtonTitle()) {
                hideKeyboard()
                viewModel.verifyPhoneNumber()
            }
            .disabled(viewModel.verifyPhoneNumberResponse.isLoading || viewModel.verifyForgotPasswordCodeResponse.isLoading)
            .padding(.top, 4)

            RoundedButton(style: .secondaryButton, title: viewModel.resendTitle) {
                viewModel.resentOPT()
            }
            .background(viewModel.resendCountDown ? R.color.gray219()!.suColor : R.color.clear()?.suColor)
            .opacity(viewModel.resendCountDown ? 0.6 : 1)
            .cornerRadius(.infinity)
            .disabled(viewModel.resendCountDown || viewModel.verifyPhoneNumberResponse.isLoading || viewModel.verifyForgotPasswordCodeResponse.isLoading)
        }
        .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.5, tapToDismiss: false, offsetY: 60, alert: {
            AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.phoneNumberVerificationViewAlertTitle())
        }, onTap: {
            isPresented = false
        }, completion: {
            isPresented = false
        })
        .onChange(of: viewModel.verifyForgotPasswordCodeResponse) { response in
            if response.isLoaded {
                isPresented = false
                token = response.value!.token
            }
        }
        .hideKeyboardOnTap()
        .padding(.vertical)
        .padding(.horizontal, 20)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .onLoad {
            switch viewModel.mode {
            case .register:
                viewModel.initPhoneNumberVerification()
            case .forgotPassword:
                viewModel.startCountDownTimer()
            }
        }
    }
}

struct PhoneNumberVerificationView_Previews: PreviewProvider {
    static var previews: some View {
        PhoneNumberVerificationView(viewModel: .init(container: .preview), isPresented: .constant(true), token: .constant(nil))
    }
}
