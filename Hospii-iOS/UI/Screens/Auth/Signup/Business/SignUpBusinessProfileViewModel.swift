//
//  BusinessProfileViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import Combine
import CoreStore
import MapboxGeocoder
import Moya
import PermissionsSwiftUIPhoto
import PhoneNumberKit
import ResponsiveTextField
import SwiftDate
import SwiftUI

struct SignUpBusinessProfileTextFieldFirstResponses {
    var businessAptResponderDemand: FirstResponderDemand?

    var businessNameResponderDemand: FirstResponderDemand?
}

enum BusinessProfileEditorMode {
    case create
    case signup
    case update
}

// MARK: - SignUpBusinessProfileViewModel

extension SignUpBusinessProfileView {
    class ViewModel: ObservableObject {
        // State
        @Published var businessName: String = ""

        @Published var businessAddress: String = ""

        @Published var businessAddress1: String = ""

        @Published var businessAddress2: String = ""

        @Published var businessAddressState: String = ""

        @Published var businessAddressCity: String = ""

        @Published var businessAddressCode: String = ""

        @Published var businessAbout: String = ""

        @Published var businessPhoneNumber: String = ""

        @Published var businessNameError: String = ""

        @Published var businessAddressError: String = ""

        @Published var businessAboutError: String = ""

        @Published var businessPhoneNumberError: String = ""

        @Published var mondayHours = BusinessHour(weekday: 0)

        @Published var tuesdayHours = BusinessHour(weekday: 1)

        @Published var wednesdayHours = BusinessHour(weekday: 2)

        @Published var thursdayHours = BusinessHour(weekday: 3)

        @Published var fridayHours = BusinessHour(weekday: 4)

        @Published var saturdayHours = BusinessHour(weekday: 5)

        @Published var sundayHours = BusinessHour(weekday: 6)

        @Published var createBusinessResponse: Loadable<CreateBusinessResponse>

        @Published var createBusinessHoursResponse: Loadable<Response>

        @Published var getProfileResponse: Loadable<UserResponse>

        @Published var postCoverImageResponse: Loadable<Response>

        @Published var location: GeocodedPlacemark? = nil

        @Published var activeSheet: ActivePhotoPickerSheet?

        @Published var image: UIImage?

        @Published var cropImage: UIImage?

        @Published var coverImageUrl: String?

        @Published var businessId: Int?

        @Published var mode: BusinessProfileEditorMode

        @Published var signUpBusinessProfileTextFieldFirstResponses = SignUpBusinessProfileTextFieldFirstResponses()

        @Published var isPresentedPermissionsAlert = false

        @Published var isPresentedSettingsAlert = false

        @Published var isPresentedSuccessAlert = false

        @Published var isPresentedSearchLoacationView: Bool = false

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()
        private let phoneNumberKit = PhoneNumberKit()
        private var openingHour = Date(year: 2022, month: 12, day: 31, hour: 9, minute: 00, region: .current)
        private var closedHour = Date(year: 2022, month: 12, day: 31, hour: 20, minute: 00, region: .current)

        private lazy var userPublisher: ListPublisher<UserMO> = {
            CoreStoreDefaults.dataStack.publishList(From<UserMO>()
                .orderBy(.ascending(\.key))
                .tweak { $0.fetchLimit = 1 })
        }()

        init(container: DIContainer,
             createBusinessResponse: Loadable<CreateBusinessResponse> = .notRequested,
             createBusinessHoursResponse: Loadable<Response> = .notRequested,
             getProfileResponse: Loadable<UserResponse> = .notRequested,
             postCoverImageResponse: Loadable<Response> = .notRequested,
             mode: BusinessProfileEditorMode = .signup)
        {
            self.container = container
            _createBusinessResponse = .init(initialValue: createBusinessResponse)
            _createBusinessHoursResponse = .init(initialValue: createBusinessHoursResponse)
            _getProfileResponse = .init(initialValue: getProfileResponse)
            _postCoverImageResponse = .init(initialValue: postCoverImageResponse)

            _mode = .init(initialValue: mode)
            cancelBag.collect {
                $location
                    .compactMap { $0?.formattedAddress(compact: true) }
                    .weakAssign(to: \.businessAddress, on: self)

                $createBusinessResponse
                    .sink { [weak self] response in
                        self?.onCreateBusinessResponseChange(response: response)
                    }

                $postCoverImageResponse
                    .sink { [weak self] response in
                        switch response {
                        case .loaded:
                            self?.getProfile()
                        default:
                            break
                        }
                    }

                $createBusinessResponse
                    .compactMap { $0.value?.id }
                    .weakAssign(to: \.businessId, on: self)

                $createBusinessHoursResponse
                    .sink { [weak self] response in
                        self?.onCreateBusinessHoursResponseChange(response: response)
                    }

                $image
                    .sink { [weak self] image in
                        self?.applyImage(image: image)
                    }

                $cropImage
                    .sink { [weak self] image in
                        if image != nil {
                            self?.coverImageUrl = nil
                        }
                        self?.activeSheet = nil
                    }

                $businessName
                    .sink { [weak self] text in
                        self?.validate(businessName: text, isRevalidate: true)
                    }

                $businessAddress
                    .sink { [weak self] text in
                        self?.validate(businessAddress: text, isRevalidate: true)
                    }

                $businessPhoneNumber
                    .sink { [weak self] text in
                        self?.validate(businessPhoneNumber: text, isRevalidate: true)
                    }

                $isPresentedPermissionsAlert
                    .removeDuplicates()
                    .filter { !$0 }
                    .dropFirst()
                    .sink { [weak self] _ in
                        self?.openImagePicker(false)
                    }
            }

            if mode == .update {
                userPublisher.reactive
                    .snapshot(emitInitialValue: true)
                    .compactMap { $0.get(0)?.object }
                    .sink { [weak self] user in
                        if self?.isPresentedSuccessAlert == true {
                            return
                        }
                        if let business = user.getUserBusinesses()?.first, let self = self {
                            self.businessName = business.name
                            self.businessAbout = business.businessAbout ?? ""
                            self.businessPhoneNumber = (business.phoneNumberCountryCode ?? "") + (business.phoneNumber ?? "")
                            self.businessAddress = "\(business.userAddress.address) \(business.userAddress.address2 ?? ""), \(business.userAddress.city ?? ""), \(business.userAddress.state ?? "") \(business.userAddress.code ?? "")"
                            self.coverImageUrl = business.coverPhoto?.imageUrl
                            self.businessAddress1 = business.userAddress.address
                            self.businessAddress2 = business.userAddress.address2 ?? ""
                            self.businessAddressCity = business.userAddress.city ?? ""
                            self.businessAddressState = business.userAddress.state ?? ""
                            self.businessAddressCode = business.userAddress.code ?? ""
                            for hour in business.userBusinessHours ?? [] {
                                let savedHour = BusinessHour.from(hour)
                                switch hour.weekDay {
                                case 0:
                                    self.mondayHours = savedHour
                                case 1:
                                    self.tuesdayHours = savedHour
                                case 2:
                                    self.wednesdayHours = savedHour
                                case 3:
                                    self.thursdayHours = savedHour
                                case 4:
                                    self.fridayHours = savedHour
                                case 5:
                                    self.saturdayHours = savedHour
                                case 6:
                                    self.sundayHours = savedHour
                                default:
                                    break
                                }
                            }
                        }
                    }
                    .store(in: cancelBag)
            }
        }

        func createBusiness() {
            validate(businessName: businessName)
            validate(businessAddress: businessAddress)
            validate(businessPhoneNumber: businessPhoneNumber)

            if !businessNameError.isEmpty || !businessAddressError.isEmpty || !businessPhoneNumberError.isEmpty {
                return
            }

            let parsedPhonenumber = try? phoneNumberKit.parse(businessPhoneNumber)
            guard let phoneNumber = parsedPhonenumber?.adjustedNationalNumber(), let countryCode = parsedPhonenumber?.countryCode else {
                return
            }

            let request = CreateBusinessRequest(name: businessName, about: businessAbout, address: location?.formattedName ?? businessAddress1, address2: businessAddress2.allSatisfy { $0.isWhitespace } ? nil : businessAddress2, city: location?.postalAddress?.city ?? businessAddressCity, state: location?.postalAddress?.state ?? businessAddressState, code: location?.postalAddress?.postalCode ?? businessAddressState, phoneNumber: phoneNumber, phoneNumberCountryCode: "+\(countryCode)", phoneNumberExtension: parsedPhonenumber?.numberExtension)

            switch mode {
            case .signup, .create:
                container.services.businessesService
                    .postCreateBusiness(response: loadableSubject(\.createBusinessResponse),
                                        request: request)
            case .update:
                container.services.businessesService
                    .putUpdateBusiness(response: loadableSubject(\.createBusinessResponse),
                                       request: request)
            }
        }

        func createBusinessHours(businessId: Int) {
            let businessHours = [mondayHours, tuesdayHours, wednesdayHours, thursdayHours, fridayHours, saturdayHours, sundayHours]
            let businessHourReqeusts: [BusinessHourRequest] = businessHours.map {
                BusinessHourRequest(weekDay: $0.weekday, openTime: $0.openHour.in(region: Region.UTC).toFormat("hh:mm aa"), closeTime: $0.closeHour.in(region: Region.UTC).toFormat("hh:mm aa"), isClosed: !$0.isOn, miscellaneous: nil)
            }
            container.services.businessesService
                .postCreateBusinessHours(response: loadableSubject(\.createBusinessHoursResponse), request: CreateBusinessHoursRequest(businessId: businessId, hours: businessHourReqeusts))
        }

        func getProfile() {
            guard mode != .signup else {
                return
            }
            container.services.usersService
                .getProfile(response: loadableSubject(\.getProfileResponse))
        }

        func openImagePicker(_ shouldOpenPermissionAlert: Bool = true) {
            let authorized = JMPhotoPermissionManager.photo.authorizationStatus == .authorized || JMPhotoPermissionManager.photo.authorizationStatus == .limited
            if authorized {
                activeSheet = .photoPicker
            } else if shouldOpenPermissionAlert {
                if JMPhotoPermissionManager.photo.authorizationStatus == .denied {
                    isPresentedSettingsAlert = true
                } else {
                    isPresentedPermissionsAlert = true
                }
            }
        }

        private func applyImage(image: UIImage?) {
            if let _ = image {
                activeSheet = .croppedPhotoPicker
            } else {
                activeSheet = nil
            }
        }

        private func onCreateBusinessResponseChange(response: Loadable<CreateBusinessResponse>) {
            switch response {
            case let .loaded(response):
                createBusinessHours(businessId: response.id)
            case .failed:
                break
            default:
                break
            }
        }

        private func onCreateBusinessHoursResponseChange(response: Loadable<Response>) {
            switch response {
            case .loaded:
                getProfile()
                // Make sure the file size is less or equal than 5mb.
                if let businessId = businessId,
                   let cropImage = cropImage,
                   let imageData = cropImage.jpegData(compressionQuality: 1.0),
                   Float(imageData.count) / 1024.0 / 1024.0 <= container.appState[\.system].photoFileSizeLimit
                {
                    container.services.businessesService
                        .postCover(response: loadableSubject(\.postCoverImageResponse), imageData: imageData, businessId: businessId)

                    finishTask()
                } else {
                    finishTask()
                }
            case .failed:
                break
            default:
                break
            }
        }

        private func finishTask() {
            if mode == .signup {
                container.appState[\.routing].mainView.signUpUserType = .user
            } else {
                isPresentedSuccessAlert = true
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}

extension SignUpBusinessProfileView.ViewModel {
    func validate(businessName: String?, isRevalidate: Bool = false) {
        if isRevalidate, businessNameError.isEmpty {
            return
        }
        if let businessNameValidation = businessName?.validateText(type: .businessName) as? BusinessNameValidationError {
            let errorMessage: String
            switch businessNameValidation {
            case .empty:
                errorMessage = R.string.localizable.businessNameValidationErrorEmpty()
            }
            businessNameError = errorMessage
        } else {
            businessNameError = ""
        }
    }

    func validate(businessAddress: String?, isRevalidate: Bool = false) {
        if isRevalidate, businessAddressError.isEmpty {
            return
        }
        if let businessAddressValidation = businessAddress?.validateText(type: .businessAddress) as? BusinessAddressValidationError {
            let errorMessage: String
            switch businessAddressValidation {
            case .empty:
                errorMessage = R.string.localizable.businessAddressValidationErrorEmpty()
            }
            businessAddressError = errorMessage
        } else {
            businessAddressError = ""
        }
    }

    func validate(businessPhoneNumber: String?, isRevalidate: Bool = false) {
        if isRevalidate, businessPhoneNumberError.isEmpty {
            return
        }

        let errorMessage: String

        if let phoneNumber = businessPhoneNumber, !phoneNumber.isEmpty {
            if !phoneNumberKit.isValidPhoneNumber(phoneNumber) {
                errorMessage = R.string.localizable.phoneNumberValidationErrorInvalidFormat()
            } else {
                errorMessage = ""
            }
        } else {
            errorMessage = R.string.localizable.phoneNumberValidationErrorEmpty()
        }

        businessPhoneNumberError = errorMessage
    }
}
