//
//  BusinessProfileView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import IQKeyboardManagerSwift
import Kingfisher
import PermissionsSwiftUIPhoto
import ResponsiveTextField
import SwiftUI

struct SignUpBusinessProfileView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let action: () -> Void
    
    var body: some View {
        if viewModel.mode == .signup {
            NavigationView {
                content
                    .navigationBarTitle(R.string.localizable.signUpBusinessProfileViewPageTitleText(), displayMode: .large)
                    .navigationTitle(R.string.localizable.signUpBusinessProfileViewPageTitleText())
                    .navigationBarBackButtonHidden(true)
            }
        } else {
            content
                .modifier(NavigationBarModifier(title: R.string.localizable.signUpBusinessProfileViewEditBusinessInformationTitle()))
        }
    }

    private var coverImage: Image {
        if let image = viewModel.cropImage {
            return Image(uiImage: image)
        }
        return Image(uiImage: R.image.clear()!)
    }

    private var content: some View {
        return ZStack(alignment: .top) {
            NavigationLink(
                destination: SearchLocationView(viewModel: .init(container: viewModel.container), location: $viewModel.location),
                isActive: $viewModel.isPresentedSearchLoacationView
            ) {
                EmptyView()
            }

            ScrollView {
                VStack(alignment: .leading) {
                    Text(R.string.localizable.signUpBusinessProfileViewPhotoUploadingSectionTitle())
                        .style(.textFieldLabel)
                        .padding(.top, 20)

                    ZStack(alignment: .center) {
                        VStack {
                            R.image.addFrontPhoto()!.suImage
                                .padding(.top, 12)
                            Text(R.string.localizable.signUpBusinessProfileViewPhotoUploadingSectionDescription())
                                .fixedSize(horizontal: false, vertical: true)
                                .lineLimit(nil)
                                .font(.subText)
                                .foregroundColor(R.color.gray162()!.suColor)
                                .padding(.top, 12)
                                .padding(.bottom, 12)
                                .multilineTextAlignment(.center)
                        }

                        if let imageUrl = viewModel.coverImageUrl {
                            KFImage.url(URL(string: imageUrl))
                                .setProcessor(ImageProcessors.avatarProcessor)
                                .resizable()
                                .scaledToFill()
                                .frame(maxWidth: .infinity)
                                .frame(height: coverImageHeight)
                                .cornerRadius(20)
                                .onTapGesture {
                                    viewModel.openImagePicker()
                                }
                        } else {
                            coverImage
                                .resizable()
                                .scaledToFill()
                                .frame(maxWidth: .infinity)
                                .frame(height: coverImageHeight)
                                .cornerRadius(20)
                                .onTapGesture {
                                    viewModel.openImagePicker()
                                }
                        }
                    }
                    .frame(maxWidth: .infinity)
                    .frame(height: coverImageHeight)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .strokeBorder(
                                style: StrokeStyle(
                                    lineWidth: 1.5,
                                    dash: [4]
                                )
                            )
                            .foregroundColor(R.color.gray219()!.suColor)
                    )
                    .padding(.top, 8)

                    PrimaryTextField(text: $viewModel.businessName, placeHolder: R.string.localizable.signUpBusinessProfileViewBusinessNameTextFieldPlaceholder(), title: R.string.localizable.signUpBusinessProfileViewBusinessNameTextFieldTitle(), error: viewModel.businessNameError, firstResponderDemand: $viewModel.signUpBusinessProfileTextFieldFirstResponses.businessNameResponderDemand, configuration: .words, returnType: .next) {
                        viewModel.signUpBusinessProfileTextFieldFirstResponses.businessNameResponderDemand = .shouldResignFirstResponder
                        viewModel.isPresentedSearchLoacationView = true
                    }
                    .padding(.top, 20)

                    VStack(alignment: .leading, spacing: 12) {
                        Text(R.string.localizable.signUpBusinessProfileViewBusinessAddressTextFieldTitle())
                            .style(.textFieldLabel)

                        Button(action: {
                            self.viewModel.isPresentedSearchLoacationView = true
                        }) {
                            Text(viewModel.businessAddress)
                                .style(.textFieldMessage)
                                .lineLimit(1)
                                .padding(.horizontal, 20)
                                .frame(height: 56)
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .overlay(RoundedRectangle(cornerRadius: .infinity).stroke(!viewModel.businessAddressError.isEmpty ? R.color.red()!.suColor : R.color.gray219()!.suColor))
                        }
                        .frame(height: 56)
                        .frame(maxWidth: .infinity)

                        if !viewModel.businessAddressError.isEmpty {
                            Text(viewModel.businessAddressError)
                                .style(.textFieldErrorMessage)
                        }
                    }
                    .padding(.top, 20)
                    .onTapGesture {
                        self.viewModel.isPresentedSearchLoacationView = true
                    }

                    PrimaryTextField(text: $viewModel.businessAddress2, placeHolder: R.string.localizable.signUpBusinessProfileViewBusinessAptTextFieldPlaceholder(), title: R.string.localizable.signUpBusinessProfileViewBusinessAptTextFieldTitle(), error: "", firstResponderDemand: $viewModel.signUpBusinessProfileTextFieldFirstResponses.businessAptResponderDemand, configuration: .words, returnType: .done) {
                        viewModel.signUpBusinessProfileTextFieldFirstResponses.businessAptResponderDemand = .shouldResignFirstResponder
                    }
                    .padding(.top, 20)

                    PrimaryTextEditor(text: $viewModel.businessAbout, placeHolder: R.string.localizable.signUpBusinessProfileViewBusinessAboutTextFieldPlaceholder(), title: R.string.localizable.signUpBusinessProfileViewBusinessAboutTextFieldTitle(), error: viewModel.businessAboutError)
                        .padding(.top, 20)

                    PrimaryTextField(text: $viewModel.businessPhoneNumber, placeHolder: R.string.localizable.signUpBusinessProfileViewBusinessPhoneNumberTextFieldPlaceholder(), title: R.string.localizable.signUpBusinessProfileViewBusinessPhoneNumberTextFieldTitle(), error: viewModel.businessPhoneNumberError, style: .phoneNumberTextField)
                        .padding(.top, 20)

                    Text(R.string.localizable.signUpBusinessProfileViewBusinessHoursSectionTitle())
                        .style(.textFieldLabel)
                        .padding(.top, 20)

                    Group {
                        BusinessHoursView(businessHour: $viewModel.mondayHours, dayOfWeek: R.string.localizable.dayOfTheWeekHoursMondayAbbreviationTitle())
                            .padding(.top, 8)

                        BusinessHoursView(businessHour: $viewModel.tuesdayHours, dayOfWeek: R.string.localizable.dayOfTheWeekHoursTuesdayAbbreviationTitle())
                            .padding(.top, 8)

                        BusinessHoursView(businessHour: $viewModel.wednesdayHours, dayOfWeek: R.string.localizable.dayOfTheWeekHoursWednesdayAbbreviationTitle())
                            .padding(.top, 8)

                        BusinessHoursView(businessHour: $viewModel.thursdayHours, dayOfWeek: R.string.localizable.dayOfTheWeekHoursThursdayAbbreviationTitle())
                            .padding(.top, 8)

                        BusinessHoursView(businessHour: $viewModel.fridayHours, dayOfWeek: R.string.localizable.dayOfTheWeekHoursFridayAbbreviationTitle())
                            .padding(.top, 8)

                        BusinessHoursView(businessHour: $viewModel.saturdayHours, dayOfWeek: R.string.localizable.dayOfTheWeekHoursSaturdayAbbreviationTitle())
                            .padding(.top, 8)

                        BusinessHoursView(businessHour: $viewModel.sundayHours, dayOfWeek: R.string.localizable.dayOfTheWeekHoursSundayAbbreviationTitle())
                            .padding(.top, 8)

                        RoundedButton(
                            style: .primaryButton,
                            isLoading: viewModel.createBusinessResponse.isLoading ||
                                viewModel.createBusinessHoursResponse.isLoading,
                            title: viewModel.mode == .signup ? R.string.localizable.signUpBusinessProfileViewSubmitButtonTitle() : R.string.localizable.signUpBusinessProfileViewUpdateButtonTitle()
                        ) {
                            hideKeyboard()
                            viewModel.createBusiness()
                        }
                        .disabled(viewModel.createBusinessResponse.isLoading ||
                            viewModel.createBusinessHoursResponse.isLoading)
                        .padding(.top, 20)
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.top)
                .padding(.horizontal, 20)
                .padding(.bottom, 60)
            }

            if viewModel.createBusinessResponse.error != nil {
                TopBannerAlertView(message: viewModel.createBusinessResponse.errorMessage ?? R.string.localizable.topBannerAlertViewCommonErrorMessage())
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .sheet(item: $viewModel.activeSheet) { sheet in
            switch sheet {
            case .photoPicker:
                PhotoImageView(image: $viewModel.image)
            case .croppedPhotoPicker:
                CroppedPhotoImageView(selectedImage: viewModel.image!, image: $viewModel.cropImage, cropMode: .custom)
                    .edgesIgnoringSafeArea(.all)
            }
        }
        .JMAlert(showModal: $viewModel.isPresentedPermissionsAlert, for: [.photo], restrictDismissal: false, autoDismiss: true, autoCheckAuthorization: true)
        .hideKeyboardOnTap()
        ._onBindingChange($viewModel.isPresentedSuccessAlert, perform: { value in
            if value {
                action()
                self.presentationMode.wrappedValue.dismiss()
            }
        })
//        .toast(isPresenting: $viewModel.isPresentedSuccessAlert, duration: 1.5, tapToDismiss: true, alert: {
//            AlertToast(displayMode: .hud, type: .complete(R.color.white()!.suColor), title: R.string.localizable.signUpUserProfileViewAlertTitle())
//        }, completion: {
//            self.presentationMode.wrappedValue.dismiss()
//        })
        .alert(isPresented: $viewModel.isPresentedSettingsAlert) {
            Alert(
                title: Text(R.string.localizable.permissionsPhotoLibraryAlertTitle()),
                message: Text(R.string.localizable.permissionsPhotoLibraryAlertMessage()),
                primaryButton: .cancel(Text(R.string.localizable.commonCancelButtonTitle())),
                secondaryButton: .default(Text(R.string.localizable.permissionsSettingsButtonTitle()), action: {
                    if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                })
            )
        }
        .hideKeyboardOnTap()
        .ignoresSafeArea(.keyboard)
        .onAppear {
            IQKeyboardManager.shared.enable = true
        }
        .onDisappear {
            IQKeyboardManager.shared.enable = false
        }
    }

    private var coverImageHeight: CGFloat {
        return (UIScreen.main.bounds.size.width - 40) * 15 / 34
    }
}

struct SignUpBusinessProfileView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpBusinessProfileView(viewModel: .init(container: .preview), action: {})
    }
}
