//
//  SignupView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import Kingfisher
import PermissionsSwiftUIPhoto
import ResponsiveTextField
import SwiftUI

struct SignupUserProfileView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let action: () -> Void
    
    var body: some View {
        ZStack(alignment: .top) {
            ScrollView {
                VStack(alignment: .leading, spacing: 20) {
                    if viewModel.mode == .signup {
                        UserTypePicker(selectedType: $viewModel.userSignUpType)
                    }

                    ZStack(alignment: .center) {
                        if let imageUrl = viewModel.avatarImageUrl {
                            KFImage.url(URL(string: imageUrl))
                                .setProcessor(ImageProcessors.avatarProcessor)
                                .resizable()
                                .scaledToFill()
                                .frame(width: 90, height: 90)
                                .clipShape(Circle())
                                .overlay(
                                    R.image.cameraRounded()!.suImage
                                        .offset(y: 8)
                                        .shadow(radius: 2), alignment: .bottom
                                )
                                .animation(.easeInOut)
                                .onTapGesture {
                                    viewModel.openImagePicker()
                                }
                        } else {
                            profileImage
                                .resizable()
                                .scaledToFill()
                                .frame(width: 90, height: 90)
                                .clipShape(Circle())
                                .overlay(
                                    R.image.cameraRounded()!.suImage
                                        .offset(y: 8)
                                        .shadow(radius: 2), alignment: .bottom
                                )
                                .animation(.easeInOut)
                                .onTapGesture {
                                    viewModel.openImagePicker()
                                }
                        }
                    }
                    .frame(maxWidth: .infinity)
                    .padding(.top, 4)

                    PrimaryTextField(text: $viewModel.firstName, placeHolder: R.string.localizable.signUpUserProfileViewFirstNameTextFieldPlaceholder(), title: R.string.localizable.signUpUserProfileViewFirstNameTextFieldTitle(), error: viewModel.firstNameError, firstResponderDemand: $viewModel.signupUserProfileTextFieldFirstResponses.firstNameResponderDemand, configuration: .words, returnType: .next) {
                        viewModel.signupUserProfileTextFieldFirstResponses.lastNameResponderDemand = .shouldBecomeFirstResponder
                    }

                    PrimaryTextField(text: $viewModel.lastName, placeHolder: R.string.localizable.signUpUserProfileViewLastNameTextFieldPlaceholder(), title: R.string.localizable.signUpUserProfileViewLastNameTextFieldTitle(), error: viewModel.lastNameError, firstResponderDemand: $viewModel.signupUserProfileTextFieldFirstResponses.lastNameResponderDemand, configuration: .words, returnType: .next) {
                        viewModel.signupUserProfileTextFieldFirstResponses.emailResponderDemand = .shouldBecomeFirstResponder
                    }

                    PrimaryTextField(text: $viewModel.email, placeHolder: R.string.localizable.signUpUserProfileViewEmailTextFieldPlaceholder(), title: R.string.localizable.signUpUserProfileViewEmailTextFieldTitle(), error: viewModel.emailError, firstResponderDemand: $viewModel.signupUserProfileTextFieldFirstResponses.emailResponderDemand, configuration: .emailField, returnType: .done) {
                        viewModel.signupUserProfileTextFieldFirstResponses.emailResponderDemand = .shouldResignFirstResponder
                    }

                    PrimaryTextField(text: $viewModel.phoneNumber, placeHolder: R.string.localizable.signUpUserProfileViewPhoneNumberTextFieldPlaceholder(), title: R.string.localizable.signUpUserProfileViewPhoneNumberTextFieldTitle(), error: viewModel.phoneNumberError, style: .phoneNumberTextField)

                    if viewModel.mode == .signup {
                        PrimaryTextField(text: $viewModel.password, placeHolder: R.string.localizable.signUpUserProfileViewPasswordTextFieldPlaceholder(), title: R.string.localizable.signUpUserProfileViewPasswordTextFieldTitle(), error: viewModel.passwordError, style: .secureTextField, firstResponderDemand: $viewModel.signupUserProfileTextFieldFirstResponses.passwordResponderDemand, returnType: .join) {
                            viewModel.signupUserProfileTextFieldFirstResponses.passwordResponderDemand = .shouldResignFirstResponder
                            if viewModel.mode == .signup {
                                viewModel.register()
                            } else {
                                viewModel.update()
                            }
                        }
                    }

                    applyButton
                        .disabled(viewModel.registerResponse.isLoading || viewModel.updateResponse.isLoaded)
                        .padding(.bottom, 60)
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical)
                .padding(.horizontal, 20)
            }

            if viewModel.registerResponse.error != nil {
                TopBannerAlertView(message: viewModel.registerResponse.errorMessage ?? R.string.localizable.topBannerAlertViewCommonErrorMessage())
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .modifier(NavigationBarModifier(title: viewModel.mode == .signup ? R.string.localizable.signUpUserProfileViewPageTitleText() : R.string.localizable.signUpUserProfileViewEditUserImformationTitle()))
        .fullScreenCover(item: $viewModel.activeSheet, onDismiss: {
            
        }, content: { sheet in
            switch sheet {
            case .photoPicker:
                PhotoImageView(image: $viewModel.image)
            case .croppedPhotoPicker:
                CroppedPhotoImageView(selectedImage: viewModel.image!, image: $viewModel.cropImage)
                    .edgesIgnoringSafeArea(.all)
            }
        })
        .sheet(isPresented: $viewModel.isPresentedOPTSheet) {
            PhoneNumberVerificationView(viewModel: .init(container: viewModel.container), isPresented: $viewModel.isPresentedOPTSheet, token: .constant(nil))
        }
        .JMAlert(showModal: $viewModel.isPresentedPermissionsAlert, for: [.photo], restrictDismissal: false, autoDismiss: true, autoCheckAuthorization: true)
        .alert(isPresented: $viewModel.isPresentedSettingsAlert) {
            Alert(
                title: Text(R.string.localizable.permissionsPhotoLibraryAlertTitle()),
                message: Text(R.string.localizable.permissionsPhotoLibraryAlertMessage()),
                primaryButton: .cancel(Text(R.string.localizable.commonCancelButtonTitle())),
                secondaryButton: .default(Text(R.string.localizable.permissionsSettingsButtonTitle()), action: {
                    if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                })
            )
        }
        ._onBindingChange($viewModel.isPresentedSuccessAlert, perform: { value in
            if value {
                action()
                self.presentationMode.wrappedValue.dismiss()
            }
        })
        .hideKeyboardOnTap()
    }

    private var profileImage: Image {
        if let image = viewModel.cropImage {
            return Image(uiImage: image)
        }
        switch viewModel.userSignUpType {
        case .user:
            return R.image.defaultProfilePhoto()!.suImage
        case .hairstylist:
            return R.image.defaultProfilePhoto()!.suImage
        }
    }

    private var applyButton: some View {
        if viewModel.mode == .signup {
            if viewModel.userSignUpType == .user {
                return RoundedButton(style: .primaryButton, isLoading: viewModel.registerResponse.isLoading, title: R.string.localizable.signUpUserProfileViewCreateButtonTitle()) {
                    viewModel.register()
                }
            } else {
                return RoundedButton(style: .primaryButton, isLoading: viewModel.registerResponse.isLoading, title: R.string.localizable.signUpUserProfileViewNextButtonTitle()) {
                    viewModel.register()
                }
            }
        } else {
            return RoundedButton(style: .primaryButton, isLoading: viewModel.updateResponse.isLoading, title: R.string.localizable.signUpUserProfileViewUpdateButtonTitle()) {
                viewModel.update()
            }
        }
    }
}

struct SignupView_Previews: PreviewProvider {
    static var previews: some View {
        SignupUserProfileView(viewModel: .init(container: .preview, username: ""), action: {})
    }
}
