//
//  SignupViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import Combine
import CoreStore
import Kingfisher
import Moya
import PermissionsSwiftUIPhoto
import PhoneNumberKit
import ResponsiveTextField
import SwiftUI

struct SignupUserProfileTextFieldFirstResponses {
    var firstNameResponderDemand: FirstResponderDemand? = .shouldBecomeFirstResponder

    var lastNameResponderDemand: FirstResponderDemand?

    var emailResponderDemand: FirstResponderDemand?

    var passwordResponderDemand: FirstResponderDemand?
}

enum ActivePhotoPickerSheet: Identifiable {
    var id: String {
        switch self {
        case .photoPicker: return "photoPicker"
        case .croppedPhotoPicker: return "croppedPhotoPicker"
        }
    }

    case photoPicker
    case croppedPhotoPicker
}

enum UserProfileEditorMode {
    case signup
    case update
}

// MARK: - SignupUserProfileViewModel

extension SignupUserProfileView {
    class ViewModel: ObservableObject {
        // State
        @Published var firstName: String = ""

        @Published var lastName: String = ""

        @Published var email: String = ""

        @Published var phoneNumber: String = ""

        @Published var password: String = ""

        @Published var firstNameError: String = ""

        @Published var lastNameError: String = ""

        @Published var emailError: String = ""

        @Published var phoneNumberError: String = ""

        @Published var passwordError: String = ""

        @Published var registerResponse: Loadable<Tokens>

        @Published var updateResponse: Loadable<UserResponse>

        @Published var userSignUpType: UserSignUpTypes = .user

        @Published var activeSheet: ActivePhotoPickerSheet?

        @Published var image: UIImage?

        @Published var cropImage: UIImage?

        @Published var avatarImageUrl: String?

        @Published var mode: UserProfileEditorMode

        @Published var signupUserProfileTextFieldFirstResponses = SignupUserProfileTextFieldFirstResponses()

        @Published var isPresentedSuccessAlert: Bool = false

        @Published var isPresentedPermissionsAlert = false

        @Published var isPresentedSettingsAlert = false

        @Published var isPresentedOPTSheet: Bool = false

        @Published var isPresentedImagePicker: Bool = false

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()
        private let username: String
        private let phoneNumberKit = PhoneNumberKit()

        private lazy var userPublisher: ListPublisher<UserMO> = {
            CoreStoreDefaults.dataStack.publishList(From<UserMO>()
                .orderBy(.ascending(\.key))
                .tweak { $0.fetchLimit = 1 })
        }()

        init(container: DIContainer,
             username: String,
             registerResponse: Loadable<Tokens> = .notRequested,
             updateResponse: Loadable<UserResponse> = .notRequested,
             mode: UserProfileEditorMode = .signup)
        {
            self.container = container
            self.username = username
            _registerResponse = .init(initialValue: registerResponse)
            _updateResponse = .init(initialValue: updateResponse)
            _mode = .init(initialValue: mode)
            cancelBag.collect {
                $registerResponse
                    .sink { [weak self] response in
                        self?.onRegisterResponseChange(response: response)
                    }

                $updateResponse
                    .sink { [weak self] response in
                        self?.onUpdateResponseChange(response: response)
                    }

                $image
                    .sink { [weak self] image in
                        self?.applyImage(image: image)
                    }

                $cropImage
                    .sink { [weak self] image in
                        if image != nil {
                            self?.avatarImageUrl = nil
                        }
                        self?.activeSheet = nil
                    }

                $firstName
                    .sink { [weak self] text in
                        self?.validate(firstName: text, isRevalidate: true)
                    }

                $lastName
                    .sink { [weak self] text in
                        self?.validate(lastName: text, isRevalidate: true)
                    }

                $email
                    .sink { [weak self] text in
                        self?.validate(email: text, isRevalidate: true)
                    }

                $phoneNumber
                    .sink { [weak self] text in
                        self?.validate(phoneNumber: text, isRevalidate: true)
                    }

                $password
                    .sink { [weak self] text in
                        self?.validate(password: text, isRevalidate: true)
                    }

                $isPresentedOPTSheet
                    .removeDuplicates()
                    .filter { !$0 }
                    .dropFirst()
                    .sink { [weak self] _ in
                        DispatchQueue.main.async {
                            self?.navigateToMainView()
                        }
                    }

                $isPresentedPermissionsAlert
                    .removeDuplicates()
                    .filter { !$0 }
                    .dropFirst()
                    .sink { [weak self] _ in
                        self?.openImagePicker(false)
                    }
            }

            if mode == .update {
                userPublisher.reactive
                    .snapshot(emitInitialValue: true)
                    .compactMap { $0.get(0)?.object }
                    .sink { [weak self] user in
                        self?.firstName = user.firstName ?? ""
                        self?.lastName = user.lastName ?? ""
                        self?.email = user.email ?? ""
                        self?.phoneNumber = user.phoneNumber ?? ""
                        if let photo = user.getPhoto() {
                            self?.avatarImageUrl = photo.imageUrl
                        }
                    }
                    .store(in: cancelBag)
            }
        }

        func register() {
            validate(firstName: firstName)
            validate(lastName: lastName)
            validate(email: email)
            validate(phoneNumber: phoneNumber)
            validate(password: password)

            if !firstNameError.isEmpty || !lastNameError.isEmpty || !emailError.isEmpty || !phoneNumberError.isEmpty || !passwordError.isEmpty {
                return
            }

            let parsedPhonenumber = try? phoneNumberKit.parse(phoneNumber)
            guard let phoneNumber = parsedPhonenumber?.adjustedNationalNumber(), let countryCode = parsedPhonenumber?.countryCode else {
                return
            }

            container.services.authService
                .postRegister(response: loadableSubject(\.registerResponse), request: RegisterRequest(
                    firstName: firstName, lastName: lastName, username: username, email: email, password: password, countryCode: "+\(countryCode)", phoneNumber: phoneNumber, isHairstylist: userSignUpType == .hairstylist
                ))
        }

        func update() {
            validate(firstName: firstName)
            validate(lastName: lastName)
            validate(email: email)
            validate(phoneNumber: phoneNumber)

            if !firstNameError.isEmpty || !lastNameError.isEmpty || !emailError.isEmpty || !phoneNumberError.isEmpty {
                return
            }

            let parsedPhonenumber = try? phoneNumberKit.parse(phoneNumber)
            guard let phoneNumber = parsedPhonenumber?.adjustedNationalNumber(), let countryCode = parsedPhonenumber?.countryCode else {
                return
            }

            container.services.usersService
                .putProfile(response: loadableSubject(\.updateResponse), request: UpdateProfileRequest(firstName: firstName, lastName: lastName, email: email, countryCode: "+\(countryCode)", phoneNumber: phoneNumber))
            self.isPresentedSuccessAlert = true
        }

        func openImagePicker(_ shouldOpenPermissionAlert: Bool = true) {
            let authorized = JMPhotoPermissionManager.photo.authorizationStatus == .authorized || JMPhotoPermissionManager.photo.authorizationStatus == .limited
            if authorized {
                activeSheet = .photoPicker
            } else if shouldOpenPermissionAlert {
                if JMPhotoPermissionManager.photo.authorizationStatus == .denied {
                    isPresentedSettingsAlert = true
                } else {
                    isPresentedPermissionsAlert = true
                }
            }
        }

        private func applyImage(image: UIImage?) {
            if let _ = image {
                activeSheet = .croppedPhotoPicker
            } else {
                activeSheet = nil
            }
        }

        fileprivate func updatePhoto(action: @escaping () -> Void) {
            // Make sure the file size is less or equal than 5mb.
            if let cropImage = cropImage,
               let imageData = cropImage.jpegData(compressionQuality: 1.0),
               Float(imageData.count) / 1024.0 / 1024.0 <= container.appState[\.system].photoFileSizeLimit
            {
                container.services.usersService
                    .postAvatar(response: nil, imageData: imageData)
                action()
            } else {
                action()
            }
        }

        private func onRegisterResponseChange(response: Loadable<Tokens>) {
            switch response {
            case .loaded:
                updatePhoto { [weak self] in
                    self?.isPresentedOPTSheet = true
                }
            default:
                break
            }
        }

        private func onUpdateResponseChange(response: Loadable<UserResponse>) {
            switch response {
            case let .loaded(data):
                updatePhoto { [weak self] in
                    if data.isPhoneNumberVerified == false {
                        self?.isPresentedOPTSheet = true
                    } else {
                        self?.isPresentedSuccessAlert = true
                    }
                }
            default:
                break
            }
        }

        private func navigateToMainView() {
            if mode == .signup {
                container.appState[\.routing].contentView.accessToken = container.appState[\.userData].accessToken
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}

extension SignupUserProfileView.ViewModel {
    func validate(firstName: String?, isRevalidate: Bool = false) {
        if isRevalidate, firstNameError.isEmpty {
            return
        }
        if let firstNameValidation = firstName?.validateText(type: .firstName) as? NameValidationError {
            let errorMessage: String
            switch firstNameValidation {
            case .empty:
                errorMessage = R.string.localizable.firstNameValidationErrorEmpty()
            case .tooLong:
                errorMessage = R.string.localizable.firstNameValidationErrorTooLong()
            case .invalidCharacter:
                errorMessage = R.string.localizable.firstNameValidationErrorInvalidCharacter()
            case .noSpecialCharacter:
                errorMessage = R.string.localizable.firstNameValidationErrorNoSpecialCharacter()
            }
            firstNameError = errorMessage
        } else {
            firstNameError = ""
        }
    }

    func validate(lastName: String?, isRevalidate: Bool = false) {
        if isRevalidate, lastNameError.isEmpty {
            return
        }
        if let lastNameValidation = lastName?.validateText(type: .lastName) as? NameValidationError {
            let errorMessage: String
            switch lastNameValidation {
            case .empty:
                errorMessage = R.string.localizable.lastNameValidationErrorEmpty()
            case .tooLong:
                errorMessage = R.string.localizable.lastNameValidationErrorTooLong()
            case .invalidCharacter:
                errorMessage = R.string.localizable.lastNameValidationErrorInvalidCharacter()
            case .noSpecialCharacter:
                errorMessage = R.string.localizable.lastNameValidationErrorNoSpecialCharacter()
            }
            lastNameError = errorMessage
        } else {
            lastNameError = ""
        }
    }

    func validate(email: String?, isRevalidate: Bool = false) {
        if isRevalidate, emailError.isEmpty {
            return
        }
        if let emailValidation = email?.validateText(type: .email) as? EmailValidationError {
            let errorMessage: String
            switch emailValidation {
            case .empty:
                errorMessage = R.string.localizable.emailValidationErrorEmpty()
            case .tooShort:
                errorMessage = R.string.localizable.emailValidationErrorTooShort()
            case .tooLong:
                errorMessage = R.string.localizable.emailValidationErrorTooLong()
            case .invalidFormat:
                errorMessage = R.string.localizable.emailValidationErrorInvalidFormat()
            }
            emailError = errorMessage
        } else {
            emailError = ""
        }
    }

    func validate(phoneNumber: String?, isRevalidate: Bool = false) {
        if isRevalidate, phoneNumberError.isEmpty {
            return
        }

        let errorMessage: String

        if let phoneNumber = phoneNumber, !phoneNumber.isEmpty {
            if !phoneNumberKit.isValidPhoneNumber(phoneNumber) {
                errorMessage = R.string.localizable.phoneNumberValidationErrorInvalidFormat()
            } else {
                errorMessage = ""
            }
        } else {
            errorMessage = R.string.localizable.phoneNumberValidationErrorEmpty()
        }

        phoneNumberError = errorMessage
    }

    func validate(password: String?, isRevalidate: Bool = false) {
        if isRevalidate, passwordError.isEmpty {
            return
        }
        if let passwordValidation = password?.validateText(type: .password) as? PasswordValidationError {
            let errorMessage: String
            switch passwordValidation {
            case .empty:
                errorMessage = R.string.localizable.passwordValidationErrorEmpty()
            case .invalidCharacter:
                errorMessage = R.string.localizable.passwordValidationErrorInvalidCharacter()
            case .noNumber:
                errorMessage = R.string.localizable.passwordValidationErrorNoNumber()
            case .noLowerCase:
                errorMessage = R.string.localizable.passwordValidationErrorNoLowerCase()
            case .noUpperCase:
                errorMessage = R.string.localizable.passwordValidationErrorNoUpperCase()
            case .tooLong:
                errorMessage = R.string.localizable.passwordValidationErrorTooLong()
            case .tooShort:
                errorMessage = R.string.localizable.passwordValidationErrorTooShort()
            }
            passwordError = errorMessage
        } else {
            passwordError = ""
        }
    }
}
