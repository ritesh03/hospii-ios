//
//  LocationPermissionViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import CoreLocation
import SwiftUI

// MARK: - LocationPermissionViewModel

extension LocationPermissionView {
    class ViewModel: ObservableObject {
        // State

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer) {
            self.container = container

            cancelBag.collect {}
        }

        func requestLocationPermission() {
            CLLocationManager().requestWhenInUseAuthorization()
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
