//
//  LocationPermissionView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import SwiftUI

enum LocationPermissionBottomSheetPosition: CGFloat, CaseIterable {
    case middle = 424, hidden = 0
}

struct LocationPermissionView: View {
    @StateObject var viewModel: ViewModel

    @Binding var locationPermissionBottomSheetPosition: LocationPermissionBottomSheetPosition

    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            Text(R.string.localizable.locationPermissionViewPageTitleText())
                .style(.sectionHeader)
                .lineLimit(2)
                .padding(.top, 16)

            Text(R.string.localizable.locationPermissionViewPageDescriptionText())
                .style(.primaryText)
                .lineLimit(3)
                .lineSpacing(2)
                .padding(.top, 4)

            HStack {
                Spacer()

                R.image.locationPermissionIcon()!.suImage
                    .resizable()
                    .frame(width: 80, height: 80, alignment: .center)
                    .scaledToFit()
                    .clipped()

                Spacer()
            }
            .padding(.top, 32)

            RoundedButton(style: .primaryButton, title: R.string.localizable.locationPermissionViewGrantAccessButtonTitle()) {
                viewModel.requestLocationPermission()
                locationPermissionBottomSheetPosition = .hidden
            }
            .padding(.bottom, 20)
            .padding(.top, 32)

            Spacer()
        }
        .padding(.horizontal, 20)
    }
}

struct LocationPermissionView_Previews: PreviewProvider {
    static var previews: some View {
        LocationPermissionView(viewModel: .init(container: .preview), locationPermissionBottomSheetPosition: .constant(.middle))
    }
}
