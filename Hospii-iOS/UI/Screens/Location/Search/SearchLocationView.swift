//
//  SearchLocationView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/20/21.
//

import MapboxGeocoder
import SwiftUI

struct SearchLocationView: View {
    @StateObject var viewModel: ViewModel

    @Binding var location: GeocodedPlacemark?

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ZStack {
            if #available(iOS 15.0, *) {
                List {
                    ForEach(Array(viewModel.addresses.values).indices, id: \.self) { index in
                        cellView(index: index)
                    }
                }
                .searchable(text: $viewModel.searchText,
                            placement: .navigationBarDrawer(displayMode: .always),
                            prompt: R.string.localizable.searchLoacationViewPageTitleText())
                .modifier(NavigationBarModifier(title: R.string.localizable.searchLoacationViewSectionHeaderText()))
            } else {
                List {
                    ForEach(Array(viewModel.addresses.values).indices, id: \.self) { index in
                        cellView(index: index)
                    }
                }
                .navigationBarSearch($viewModel.searchText,
                                     placeholder: R.string.localizable.searchLoacationViewPageTitleText(),
                                     hidesSearchBarWhenScrolling: false)
                .modifier(NavigationBarModifier(title: R.string.localizable.searchLoacationViewSectionHeaderText()))
            }

            //            if viewModel.addresses.isEmpty {
            //                VStack(spacing: 16) {
            //                    R.image.locationPermissionIcon()!.suImage
            //                    Text(R.string.localizable.searchLoacationViewSectionSubtitleText())
            //                        .style(.sectionSubtitle)
            //                        .multilineTextAlignment(.center)
            //                }
            //                .padding(.bottom, 24)
            //            }

            if viewModel.isLoading {
                ActivityIndicatorView(style: .large)
                    .frame(height: 16)
            }
        }
        .onAppear {
            self.viewModel.requestLocationPermission()
        }
    }

    private func cellView(index: Int) -> some View {
        let address = Array(viewModel.addresses.values)[index]
        return Button(action: {
            let geocodedPlacemark = Array(viewModel.addresses.keys)[index]
            self.location = geocodedPlacemark
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack(alignment: .center) {
                R.image.mapIcon()!.suImage
                Text(address ?? "")
                    .padding(.leading, 12)
                    .font(CustomFont.proximaNovaLight(withSize: 16))
                    .foregroundColor(R.color.text()!.suColor)
                    .lineLimit(2)
                    .minimumScaleFactor(0.01)
            }
            .padding(.horizontal, 8)
            .padding(.vertical)
        }
    }
}

struct SearchLocationView_Previews: PreviewProvider {
    static var previews: some View {
        SearchLocationView(viewModel: .init(container: .preview), location: .constant(nil))
    }
}
