//
//  SearchLocationViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/20/21.
//

import Combine
import CoreLocation
import MapboxGeocoder
import OrderedCollections
import SwiftUI

// MARK: - SearchLocationViewModel

extension SearchLocationView {
    class ViewModel: ObservableObject {
        // State
        @Published var searchText = ""

        @Published var addresses: OrderedDictionary<GeocodedPlacemark, String?> = [:]

        @Published var isLoading: Bool = false

        // Misc
        let container: DIContainer
        private var cancelBag = CancelBag()
        private let geocoder = Geocoder(accessToken: ApiConstants.MAPBOX_KEY)
        private var task: URLSessionDataTask?

        init(container: DIContainer) {
            self.container = container
            cancelBag.collect {
                $searchText
                    .filter { !$0.isEmpty }
                    .sink { [weak self] query in
                        self?.search(query: query)
                    }
            }
        }

        func requestLocationPermission() {
            CLLocationManager().desiredAccuracy = kCLLocationAccuracyBest
            CLLocationManager().requestWhenInUseAuthorization()
        }

        func search(query: String) {
            isLoading = true
            task?.cancel()
            let forwardGeocodeOptions = ForwardGeocodeOptions(query: query)
            forwardGeocodeOptions.allowedISOCountryCodes = ["US"]
            forwardGeocodeOptions.allowedScopes = .address
            forwardGeocodeOptions.maximumResultCount = 10
            forwardGeocodeOptions.focalLocation = exposedLocation
            task = geocoder.geocode(forwardGeocodeOptions) { [weak self] locations, _, error in
                if let error = error {
                    debugPrint("Error when trying to geocode query: \(error.localizedDescription)")
                } else if let locations = locations, !locations.isEmpty {
                    var autoCompletedAddressResults: OrderedDictionary<GeocodedPlacemark, String?> = [:]
                    for location in locations {
                        autoCompletedAddressResults[location] = location.formattedAddress()
                        self?.addresses = autoCompletedAddressResults
                    }
                }
                self?.isLoading = false
            }
        }

        private var exposedLocation: CLLocation? {
            return CLLocationManager().location
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
