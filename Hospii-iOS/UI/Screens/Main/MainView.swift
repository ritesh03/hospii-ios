//
//  HomeView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/21/21.
//

import BottomSheet
import Introspect
import SwiftUI

struct MainView: View {
    @StateObject var viewModel: ViewModel
    @State private var selection = 0

    private let settingsView: SettingsView

    private let searchView: SearchView

    private let circleView: CircleView

    init(viewModel: ViewModel) {
        _viewModel = StateObject(wrappedValue: viewModel)
        searchView = SearchView(viewModel: .init(container: viewModel.container))
        circleView = CircleView(viewModel: .init(container: viewModel.container))
        settingsView = SettingsView(viewModel: .init(container: viewModel.container))
    }

    var body: some View {
        if viewModel.routingState.signUpUserType == .hairstylist {
            SignUpBusinessProfileView(viewModel: .init(container: viewModel.container), action: {})
        } else {
            TabView(selection: $selection) {
                searchView
                    .tabItem {
                        selection == 0 ? R.image.searchIconFilled()!.suImage : R.image.searchIcon()!.suImage
                        Text(R.string.localizable.mainViewTabBarSearchTitle())
                            .style(.tabViewTitle)
                    }
                    .tag(0)

                circleView
                    .tabItem {
                        selection == 1 ? R.image.peopleIconFilled()!.suImage : R.image.peopleIcon()!.suImage
                        Text(R.string.localizable.mainViewTabBarCircleTitle())
                            .style(.tabViewTitle)
                    }
                    .tag(1)

                settingsView
                    .tabItem {
                        selection == 2 ? R.image.profileIconFilled()!.suImage : R.image.profileIcon()!.suImage
                        Text(R.string.localizable.mainViewTabBarProfileTitle())
                            .style(.tabViewTitle)
                    }
                    .tag(2)
            }
            .accentColor(R.color.text()!.suColor)
            .bottomSheet(bottomSheetPosition: $viewModel.locationPermissionBottomSheetPosition, options: [.allowContentDrag, .swipeToDismiss, .tapToDismiss, .absolutePositionValue, .noBottomPosition, .backgroundBlur(effect: .dark), .cornerRadius(20)]) {
                LocationPermissionView(viewModel: .init(container: viewModel.container), locationPermissionBottomSheetPosition: $viewModel.locationPermissionBottomSheetPosition)
            }
            .onLoad {
                viewModel.enableLocation()
                viewModel.getProfile()
            }
            .introspectTabBarController(customize: { controller in
                let appearance = controller.tabBar.standardAppearance
                appearance.configureWithOpaqueBackground()
                if #available(iOS 15.0, *) {
                    controller.tabBar.scrollEdgeAppearance = appearance
                } else {
                    controller.tabBar.standardAppearance = appearance
                }
            })
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(viewModel: .init(container: .preview))
    }
}
