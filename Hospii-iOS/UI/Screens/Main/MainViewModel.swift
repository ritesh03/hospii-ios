//
//  HomeViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/21/21.
//

import CoreLocation
import SwiftUI

// MARK: - Routing

extension MainView {
    struct Routing: Equatable {
        var signUpUserType: UserSignUpTypes?
    }
}

// MARK: - MainViewModel

extension MainView {
    class ViewModel: ObservableObject {
        // State
        @Published var routingState: Routing

        @Published var locationPermissionBottomSheetPosition: LocationPermissionBottomSheetPosition = .hidden

        @Published var getProfileResponse: Loadable<UserResponse>

        @AppStorage(StorageKeys.presentedLoacationRequest.rawValue) var presentedLoacationRequest: Bool = false

        // Misc
        private var cancelBag = CancelBag()
        let container: DIContainer

        init(container: DIContainer, getProfileResponse: Loadable<UserResponse> = .notRequested) {
            self.container = container
            let appState = container.appState
            _routingState = .init(initialValue: appState.value.routing.mainView)
            _getProfileResponse = .init(initialValue: getProfileResponse)

            cancelBag.collect {
                $routingState
                    .sink { appState[\.routing.mainView] = $0 }

                appState.map(\.routing.mainView)
                    .removeDuplicates()
                    .weakAssign(to: \.routingState, on: self)

                appState.map(\.routing.mainView)
                    .compactMap { $0.signUpUserType == .user }
                    .sink { _ in
                        // Show sigup successful dialog for new users.
                    }

                $locationPermissionBottomSheetPosition
                    .dropFirst()
                    .filter { $0 == .hidden }
                    .sink { [weak self] _ in
                        self?.sendLocationRequestClosedNotification()
                    }
            }
        }

        func sendLocationRequestClosedNotification() {
            if presentedLoacationRequest && CLLocationManager().authorizationStatus == .notDetermined {
                NotificationCenter.default.post(name: Notification.Name(NotificationEvents.locationRequestClosed.event), object: nil)
            }
        }

        func getProfile() {
            container.services.usersService
                .getProfile(response: loadableSubject(\.getProfileResponse))
        }

        private func enablePushNotification() {
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { settings in
                if settings.authorizationStatus == .authorized || settings.authorizationStatus == .provisional {
                    // Don't ask perimission.
                } else {
                    let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                    center.requestAuthorization(
                        options: authOptions,
                        completionHandler: { _, _ in }
                    )
                }
            }
        }

        func enableLocation() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                guard self.locationPermissionBottomSheetPosition == .hidden else {
                    return
                }

                if self.presentedLoacationRequest {
                    self.enablePushNotification()
                    return
                }

                switch CLLocationManager().authorizationStatus {
                case .notDetermined:
                    self.locationPermissionBottomSheetPosition = .middle
                    self.presentedLoacationRequest = true
                default:
                    self.enablePushNotification()
                }
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
