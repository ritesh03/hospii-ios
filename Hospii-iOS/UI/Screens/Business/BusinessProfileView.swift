//
//  BusinessProfileView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Introspect
import Kingfisher
import SwiftUI

struct BusinessProfileView: View {
    @StateObject var viewModel: ViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ZStack {
            NavigationLink(destination: PhotoGalleryView(viewModel: .init(container: viewModel.container, getPostsRequest: viewModel.getPostsRequest, photosResponse: viewModel.photosResponse)), isActive: $viewModel.isPresentedGalleryView)
                {
                    EmptyView()
                }

            content
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: HStack {
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                R.image.backWhiteIcon()!.suImage
                    .foregroundColor(viewModel.toggleToolbarColor ? R.color.text()!.suColor : R.color.textLight()!.suColor)
            }
        })
        .onLoad {
            viewModel.fetchBusiness()
        }
    }

    private var tabContent: AnyView {
        switch viewModel.selectedType {
        case .about: return AnyView(BusinessAboutView(viewModel: .init(container: viewModel.container, business: viewModel.business)))
        case .gallery: return AnyView(photosView)
        }
    }

    private var content: AnyView {
        switch viewModel.userBusinessResponse {
        case .notRequested: return AnyView(notRequestedView)
        case let .isLoading(last, _): return AnyView(loadingView(last))
        case let .loaded(business): return AnyView(loadedView(business, showLoading: false))
        case let .cache(business): return AnyView(loadedView(business, showLoading: false))
        case let .failed(error): return AnyView(failedView(error))
        }
    }

    private var photosView: some View {
        LazyVGrid(columns: [
            GridItem(.flexible(), spacing: 1),
            GridItem(.flexible(), spacing: 1),
            GridItem(.flexible(), spacing: 1),
        ], spacing: 1) {
            ForEach(viewModel.photosResponse.value?.data ?? [], id: \.id) { feed in
                makePhotoCell(post: feed, width: (UIScreen.main.bounds.size.width - 2) / 3)
            }
        }
        .onLoad {
            viewModel.reloadPhotos()
        }
    }

    private func makePhotoCell(post: PostResponse, width: CGFloat) -> some View {
        let width = max(0, width)
        return ZStack {
            KFImage
                .url(URL(string: post.__medias__?.first?.imageUrl ?? ""))
                .setProcessor(ImageProcessors.gridProcessor)
                .resizable()
                .scaledToFill()
                .frame(width: width, height: width)
                .clipped()
                .onTapGesture {
                    viewModel.getPostsRequest.selectedItemId = post.id
                    viewModel.isPresentedGalleryView = true
                }
        }
        .frame(maxWidth: width, maxHeight: width)
        .frame(width: width, height: width)
    }
}

struct BusinessProfileView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessProfileView(viewModel: .init(container: .preview, business: UserBusinessResponse(id: 0, name: "", businessAbout: nil, type: nil, status: nil, phoneNumber: nil, phoneNumberExtension: nil, phoneNumberCountryCode: nil, isPhoneNumberVerified: false, coverPhoto: nil, userBusinessHours: nil, userAddress: BusinessAddressResponse(address: "", address2: nil, city: nil, state: nil, code: nil, latitude: nil, longitude: nil), __serviceFilters__: nil, user: nil, distance: nil)))
    }
}

struct BusinessRoundedTextModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(CustomFont.proximaNovaSemibold(withSize: 13))
            .padding(.horizontal, 10)
            .padding(.vertical, 4)
    }
}

extension LinearGradient {
    static let blackOpacityGradient = LinearGradient(
        gradient: Gradient(colors: [Color.black.opacity(0.98), Color.black.opacity(0.00)]),
        startPoint: .top,
        endPoint: .bottom
    )
}

// MARK: - Loading Content

private extension BusinessProfileView {
    var notRequestedView: some View {
        EmptyView()
    }

    func loadingView(_ previouslyLoaded: UserBusinessResponse?) -> some View {
        if let business = previouslyLoaded {
            return AnyView(loadedView(business, showLoading: true))
        } else {
            return AnyView(ActivityIndicatorView()
                .padding())
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            viewModel.fetchBusiness()
        })
    }
}

// MARK: - Displaying Content

private extension BusinessProfileView {
    func loadedView(_ business: UserBusinessResponse, showLoading _: Bool) -> some View {
        GeometryReader { geometry in
            ZStack(alignment: .top) {
                PullToRefreshView(header: EmptyView(),
                                  footer: viewModel.selectedType == .gallery ? AnyView(SimpleRefreshingView()) : AnyView(EmptyView()),
                                  isHeaderRefreshing: .constant(false),
                                  isFooterRefreshing: viewModel.selectedType == .gallery ? $viewModel.footerRefreshing : .constant(false),
                                  isNoMoreData: viewModel.selectedType == .gallery ? $viewModel.getPostsRequest.noMore : .constant(true),
                                  onHeaderRefresh: {},
                                  onFooterRefresh: { viewModel.fetchPhotos() }) {
                    VStack {
                        ZStack {
                            GeometryReader { geometry in
                                KFImage
                                    .url(URL(string: business.coverPhoto?.imageUrl ?? ""))
                                    .placeholder {
                                        R.image.black()!.suImage
                                    }
                                    .setProcessor(ImageProcessors.thumbnailProcessor)
                                    .resizable()
                                    .scaledToFill()
                                    .overlay(LinearGradient.blackOpacityGradient)
                                    .frame(width: geometry.size.width, height: ParallaxHeader.getHeightForHeaderImage(geometry))
                                    .clipped()
                                    .offset(x: 0, y: ParallaxHeader.getOffsetForHeaderImage(geometry))
                            }.frame(height: UIScreen.main.bounds.width * 230 / 375)

                            VStack(alignment: .leading) {
                                Spacer()

                                HStack {
                                    Text("@\(business.user?.claimProfile?.username ?? business.user?.username ?? "")")
                                        .foregroundColor(R.color.white()!.suColor)
                                        .modifier(BusinessRoundedTextModifier())
                                        .background(R.color.black()!.suColor.opacity(0.45))
                                        .cornerRadius(.infinity)

                                    Spacer()

                                    if business.__serviceFilters__?.contains { $0.service == "Walk-ins" } == true {
                                        Text("Walk-ins accepted")
                                            .foregroundColor(R.color.green()!.suColor)
                                            .modifier(BusinessRoundedTextModifier())
                                            .background(R.color.white()!.suColor.opacity(0.95))
                                            .overlay(
                                                RoundedRectangle(cornerRadius: .infinity)
                                                    .stroke(lineWidth: 1)
                                                    .foregroundColor(R.color.green()!.suColor)
                                            )
                                            .cornerRadius(.infinity)
                                    }
                                }
                                .padding(.horizontal, 16)
                                .padding(.vertical, 10)
                                .frame(alignment: .bottom)
                            }
                        }

                        BusinessPreviewView(business: business)

                        BusinessInformationView(business: business, phoneNumberKit: viewModel.phoneNumberKit)
                            .padding(.top, 4)
                            .padding(.horizontal)

                        ZStack(alignment: .top) {
                            tabContent
                                .padding(.top, 74)

                            BusinessProfilePicker(selectedType: $viewModel.selectedType)
                                .padding(.horizontal)
                                .padding(.top, 12)
                        }
                    }
                }
                .edgesIgnoringSafeArea(.top)
            }
            .introspectScrollView { scrollView in
                if viewModel.contentOffsetSubscription == nil {
                    viewModel.subscribe(scrollView: scrollView)
                }
            }
            .edgesIgnoringSafeArea(.top)
        }
    }
}
