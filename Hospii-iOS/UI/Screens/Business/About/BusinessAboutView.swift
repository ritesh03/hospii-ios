//
//  BusinessAboutView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Kingfisher
import SwiftUI

struct BusinessAboutView: View {
    @StateObject var viewModel: ViewModel

    var body: some View {
        VStack(alignment: .leading, spacing: 9) {
            if !viewModel.hours.isEmpty {
                Text("Opening Hours")
                    .style(.listSectionHeader)

                ForEach(viewModel.hours, id: \.weekday) { hour in
                    OpeningHoursText(businessHour: hour)
                }
            }

            VStack(alignment: .leading, spacing: 9) {
                Text("Address")
                    .style(.listSectionHeader)
                    .padding(.top, 8)

                Text("\(viewModel.business.userAddress.address) \(viewModel.business.userAddress.address2 ?? ""), \(viewModel.business.userAddress.city ?? ""), \(viewModel.business.userAddress.state ?? "") \(viewModel.business.userAddress.code ?? "")")
                    .style(.subTxtGray)
                    .lineLimit(2)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .onTapGesture {
                        viewModel.isPresentedMapOptionsActionSheet = true
                    }

                Label(title: {
                    Text("Get directions - \(viewModel.business.distance?.roundedString(toPlaces: 1) ?? "") miles")
                        .font(CustomFont.proximaNovaSemibold(withSize: 14))
                        .foregroundColor(R.color.blueLight()!.suColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }, icon: {
                    R.image.mapBlueIcon()!.suImage
                })
                .onTapGesture {
                    viewModel.isPresentedMapOptionsActionSheet = true
                }
            }

            if let parsedPhoneNumber = viewModel.parsedPhoneNumber {
                VStack(alignment: .leading, spacing: 9) {
                    Text("Contact")
                        .style(.listSectionHeader)
                        .padding(.top, 8)

                    Label(title: {
                        Text(viewModel.phoneNumberKit.format(parsedPhoneNumber, toType: .national))
                            .style(.subTxtGray)
                            .lineLimit(2)
                            .minimumScaleFactor(0.1)
                    }, icon: {
                        R.image.phoneGrayIcon()!.suImage
                    })
                }
                .onTapGesture {
                    parsedPhoneNumber.call()
                }
            }

            if let serviceFilters = viewModel.business.__serviceFilters__, !serviceFilters.isEmpty {
                VStack(alignment: .leading, spacing: 9) {
                    Text("More info")
                        .style(.listSectionHeader)
                        .padding(.top, 8)

                    ForEach(serviceFilters, id: \.id) { service in
                        Label(title: {
                            Text(service.service ?? "")
                                .style(.subTxtGray)
                        }, icon: {
                            if let url = service.icon {
                                KFImage
                                    .url(URL(string: url))
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 16, height: 16)
                                    .padding(.trailing, 4)
                            }
                        })
                    }
                }
            }

            if let about = viewModel.business.businessAbout {
                VStack(alignment: .leading, spacing: 9) {
                    Text("About")
                        .style(.listSectionHeader)
                        .padding(.top, 8)

                    Text(about)
                        .style(.subTxtGray)
                        .lineLimit(nil)
                }
            }

            Spacer()
        }
        .padding(.horizontal)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
        .actionSheet(isPresented: $viewModel.isPresentedMapOptionsActionSheet) {
            ActionSheet(
                title: Text("Address"),
                buttons: [
                    .default(Text("Open in Apple Maps")) {
                        viewModel.openAppleMaps()
                    },
                    .default(Text("Open in Google Maps")) {
                        viewModel.openGoogleMap()
                    },
                    .cancel(),
                ]
            )
        }
    }
}

struct BusinessAboutView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessAboutView(viewModel: .init(container: .preview, business: UserBusinessResponse(id: 0, name: "", businessAbout: nil, type: nil, status: nil, phoneNumber: nil, phoneNumberExtension: nil, phoneNumberCountryCode: nil, isPhoneNumberVerified: false, coverPhoto: nil, userBusinessHours: nil, userAddress: BusinessAddressResponse(address: "", address2: nil, city: nil, state: nil, code: nil, latitude: nil, longitude: nil), __serviceFilters__: nil, user: nil, distance: nil)))
    }
}
