//
//  BusinessAboutViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Combine
import PhoneNumberKit
import SwiftUI

// MARK: - BusinessAboutViewModel

extension BusinessAboutView {
    class ViewModel: ObservableObject {
        // State
        @Published var business: UserBusinessResponse

        @Published var isPresentedMapOptionsActionSheet = false

        @Published var hours: [BusinessHour] = []

        @Published var parsedPhoneNumber: PhoneNumber? = nil

        // Misc
        let container: DIContainer
        let phoneNumberKit = PhoneNumberKit()

        init(container: DIContainer, business: UserBusinessResponse) {
            self.container = container
            self.business = business

            hours = (business.userBusinessHours ?? []).sorted {
                $0.weekDay < $1.weekDay
            }.map { BusinessHour.from($0) }

            if let phoneNumber = business.phoneNumber, let countryCode = business.phoneNumberCountryCode, let parsedPhoneNumber = try? phoneNumberKit.parse("\(countryCode)\(phoneNumber)") {
                self.parsedPhoneNumber = parsedPhoneNumber
            }
        }

        func openGoogleMap() {
            MapUtils.shared.openGoogleMaps(lat: business.userAddress.latitude, lng: business.userAddress.longitude)
        }

        func openAppleMaps() {
            MapUtils.shared.openAppleMaps(lat: business.userAddress.latitude, lng: business.userAddress.longitude)
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
