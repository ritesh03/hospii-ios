//
//  BusinessProfileViewModel.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Combine
import PhoneNumberKit
import SwiftUI

// MARK: - BusinessProfileViewModel

extension BusinessProfileView {
    class ViewModel: ObservableObject {
        // State
        @Published var selectedType: BusinessProfileType = .about

        @Published var business: UserBusinessResponse

        @Published var getPostsRequest: GetPostsRequest

        @Published var userBusinessResponse: Loadable<UserBusinessResponse>

        @Published var photosResponse: Loadable<SearchFeedsResponse>

        @Published var footerRefreshing = false

        @Published var isPresentedGalleryView = false

        @Published var toggleToolbarColor = false

        // Misc
        var cancelBag = CancelBag()
        var contentOffsetSubscription: AnyCancellable?
        let phoneNumberKit = PhoneNumberKit()
        let container: DIContainer

        init(container: DIContainer, business: UserBusinessResponse,
             userBusinessResponse: Loadable<UserBusinessResponse> = .notRequested,
             photosResponse: Loadable<SearchFeedsResponse> = .notRequested)
        {
            self.container = container
            self.business = business
            getPostsRequest = GetPostsRequest(business: business)
            _userBusinessResponse = .init(initialValue: userBusinessResponse)
            _photosResponse = .init(initialValue: photosResponse)

            cancelBag.collect {
                $photosResponse
                    .sink { [weak self] response in
                        self?.photosResponseChanged(response: response)
                    }
            }
        }

        func fetchBusiness() {
            container.services.businessesService.getUserBusinessById(response: loadableSubject(\.userBusinessResponse), businessId: business.id)
        }

        func reloadPhotos() {
            getPostsRequest.page = 1
            getPostsRequest.noMore = false
            fetchPhotos()
        }

        func fetchPhotos() {
            container.services.businessesService.getUserBusinessPhotos(response: loadableSubject(\.photosResponse), getPostsRequest: getPostsRequest)
        }

        func subscribe(scrollView: UIScrollView) {
            contentOffsetSubscription = scrollView.publisher(for: \.contentOffset).sink { [weak self] contentOffset in
                if (contentOffset.y / 200) >= 0.05, self?.toggleToolbarColor == false {
                    self?.toggleToolbarColor.toggle()
                } else if (contentOffset.y / 200) < 0.05, self?.toggleToolbarColor == true {
                    self?.toggleToolbarColor.toggle()
                }
            }
        }

        private func photosResponseChanged(response: Loadable<SearchFeedsResponse>) {
            switch response {
            case let .loaded(body):
                footerRefreshing = false

                if !body.data.isEmpty {
                    getPostsRequest.page = getPostsRequest.page + 1
                }

                if body.meta.itemCount == 0 {
                    getPostsRequest.noMore = true
                }
            case .failed:
                footerRefreshing = false
            default:
                break
            }
        }

        deinit {
            debugPrint("\(self) deinit")
        }
    }
}
