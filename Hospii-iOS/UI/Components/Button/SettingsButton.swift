//
//  SettingsButton.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import SwiftUI

struct SettingsButton: View {
    var title: String

    var image: Image

    let action: () -> Void

    var body: some View {
        Button(action: self.action) {
            HStack(spacing: 14) {
                image
                    .resizable()
                    .frame(width: 22, height: 22)
                    .scaledToFit()

                Text(title)

                Spacer()

                R.image.rightGrayArrrow()!.suImage
            }
            .frame(maxWidth: .infinity)
        }
        .padding(20)
        .contentShape(Rectangle())
        .font(CustomFont.proximaNovaRegular(withSize: 16))
        .foregroundColor(R.color.text()!.suColor)
        .overlay(
            RoundedRectangle(cornerRadius: 16)
                .stroke(lineWidth: 1)
                .foregroundColor(R.color.gray219()!.suColor)
        )
    }
}

struct SettingsButton_Previews: PreviewProvider {
    static var previews: some View {
        SettingsButton(title: "Create a Hairstylist account", image: R.image.profileCircleIcon()!.suImage, action: {})
    }
}
