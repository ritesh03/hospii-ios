//
//  RoundedButton.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import SwiftUI

enum RoundedButtonStyle {
    case primaryButton
    case secondaryButton
}

struct RoundedButton: View {
    var textColor: Color

    var borderColor: Color

    var borderWidth: CGFloat

    var backgroundColor: Color

    var isUppercased: Bool

    var title: String

    var cornerRadius: CGFloat

    var isLoading: Bool

    let action: () -> Void

    init(textColor: Color = R.color.text()!.suColor,
         borderColor: Color = .clear,
         borderWidth: CGFloat = 2,
         backgroundColor: Color = R.color.yellow()!.suColor,
         isUppercased: Bool = true,
         isLoading: Bool = false,
         title: String,
         cornerRadius: CGFloat = .infinity,
         action: @escaping () -> Void)
    {
        self.textColor = textColor
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.backgroundColor = backgroundColor
        self.isUppercased = isUppercased
        self.title = title
        self.isLoading = isLoading
        self.action = action
        self.cornerRadius = cornerRadius
    }

    init(style: RoundedButtonStyle, isLoading: Bool = false, title: String, cornerRadius: CGFloat = .infinity, action: @escaping () -> Void) {
        self.isLoading = isLoading
        switch style {
        case .primaryButton:
            textColor = R.color.text()!.suColor
            borderColor = .clear
            borderWidth = 0
            backgroundColor = R.color.yellow()!.suColor
            isUppercased = true
        case .secondaryButton:
            textColor = R.color.text()!.suColor
            borderColor = R.color.text()!.suColor
            borderWidth = 2
            backgroundColor = R.color.clear()!.suColor
            isUppercased = true
        }
        self.title = title
        self.action = action
        self.cornerRadius = cornerRadius
    }

    var body: some View {
        Button(action: self.action) {
            if !isLoading {
                Text(isUppercased ? title.uppercased() : title)
                    .frame(maxWidth: .infinity)
            } else {
                ActivityIndicatorView()
                    .frame(height: 16)
            }
        }
        .padding(20)
        .contentShape(Rectangle())
        .frame(maxWidth: .infinity)
        .style(.actionButton)
        .foregroundColor(textColor)
        .background(backgroundColor)
        .overlay(
            RoundedRectangle(cornerRadius: cornerRadius)
                .stroke(lineWidth: borderWidth)
                .foregroundColor(borderColor)
        )
        .cornerRadius(cornerRadius)
    }
}

struct RoundedButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundedButton(isLoading: false, title: "SIGN UP") {}
    }
}
