//
//  FilterItem.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import Kingfisher
import SwiftUI

struct FilterItem: View {
    @State var title: String

    @State var image: String?

    @State var isChecked: Bool

    let action: (Bool) -> Void

    var body: some View {
        HStack {
            if image != nil {
                KFImage
                    .url(URL(string: image ?? ""))
                    .resizable()
                    .scaledToFit()
                    .frame(width: 16, height: 16)
                    .padding(.trailing, 4)
            }

            Text(title)
                .style(.primaryText)

            Spacer()

            CheckBox(isChecked: $isChecked, action: action)
        }
        .onTapGesture {
            isChecked = !isChecked
        }
    }
}

struct FilterItem_Previews: PreviewProvider {
    static var previews: some View {
        FilterItem(title: "Credit Cards", image: nil, isChecked: true) { _ in
        }
    }
}
