//
//  BusinessProfilePicker.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import SwiftUI

struct BusinessProfilePicker: View {
    @Binding var selectedType: BusinessProfileType

    var body: some View {
        HStack(spacing: 0) {
            ForEach(BusinessProfileType.allCases, id: \.self) { type in
                ZStack {
                    Rectangle()
                        .frame(height: 45)
                        .foregroundColor((selectedType == type) ? R.color.yellow()!.suColor : .clear)
                        .cornerRadius(.infinity)

                    Button(action: {
                        self.selectedType = type
                    }) {
                        Text(type.title)
                            .font(Font.textFieldLabel)
                            .foregroundColor(R.color.text()!.suColor)
                            .frame(height: 38)
                            .frame(maxWidth: .infinity)
                    }
                    .contentShape(Rectangle())
                    .frame(height: 38)
                    .frame(maxWidth: .infinity)
                }
                .overlay(RoundedRectangle(cornerRadius: .infinity)
                    .stroke(lineWidth: 6)
                    .foregroundColor(R.color.gray242()!.suColor))
                .animation(.easeInOut(duration: 0.3), value: selectedType)
            }
        }
        .background(R.color.gray242()!.suColor)
        .overlay(RoundedRectangle(cornerRadius: .infinity)
            .stroke(lineWidth: 6)
            .foregroundColor(R.color.gray242()!.suColor))
        .cornerRadius(.infinity)
    }
}

struct BusinessProfilePicker_Previews: PreviewProvider {
    static var previews: some View {
        BusinessProfilePicker(selectedType: .constant(.about))
    }
}
