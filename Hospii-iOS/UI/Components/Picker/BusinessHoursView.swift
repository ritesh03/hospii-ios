//
//  BusinessHoursView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/15/21.
//

import Combine
import SwiftDate
import SwiftUI

struct BusinessHour {
    let weekday: Int
    var openHour = Date(year: 2032, month: 12, day: 31, hour: 9, minute: 00, region: .current)
    var closeHour = Date(year: 2032, month: 12, day: 31, hour: 20, minute: 00, region: .current)
    var isOn: Bool = false
    var interval: String? = nil
}

extension BusinessHour {
    static func from(_ response: BusinessHourResponse) -> BusinessHour {
        let openTime = response.openTime.toDate(region: Region.UTC)?.convertTo(region: .current)
        let closeTime = response.closeTime.toDate(region: Region.UTC)?.convertTo(region: .current)

        let date = Date.now.in(region: .current)

        let openHour = Date(year: date.year, month: date.month, day: date.day, hour: openTime?.hour ?? 9, minute: openTime?.minute ?? 00, region: .current)

        let closeHour = Date(year: date.year, month: date.month, day: date.day, hour: closeTime?.hour ?? 20, minute: closeTime?.minute ?? 00, region: .current)

        let interval = "\(openHour.in(region: Region.current).toFormat("hh:mm aa")) - \(closeHour.in(region: Region.current).toFormat("hh:mm aa"))"

        return .init(weekday: response.weekDay,
                     openHour: openHour,
                     closeHour: closeHour,
                     isOn: !response.isClosed,
                     interval: interval)
    }
}

enum ActiveHourPickerSheet: Identifiable {
    var id: String {
        switch self {
        case .openingHour: return "opening"
        case .closedHour: return "closed"
        }
    }

    case openingHour
    case closedHour
}

struct BusinessHoursView: View {
    @Binding var businessHour: BusinessHour

    let dayOfWeek: String

    @State var activeSheet: ActiveHourPickerSheet? {
        didSet {
            isPresentedPicker = activeSheet != nil
        }
    }

    @State var isPresentedPicker: Bool = false

    var body: some View {
        HStack(spacing: 16) {
            Text(dayOfWeek)
                .font(CustomFont.proximaNovaSemibold(withSize: 16))
                .foregroundColor(R.color.text()!.suColor)
                .multilineTextAlignment(.leading)
                .lineLimit(1)
                .minimumScaleFactor(0.1)
                .frame(width: 40)

            HStack {
                Text(businessHour.openHour.in(region: Region.current).toFormat("hh:mm aa"))
                    .font(CustomFont.proximaNovaSemibold(withSize: 16))
                    .foregroundColor(R.color.text()!.suColor)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.leading, 6)
                    .onTapGesture {
                        self.activeSheet = .openingHour
                        self.businessHour.isOn = true
                    }

                Divider()
                    .background(R.color.gray219()!.suColor)
                    .padding(.vertical, 12)

                Text(businessHour.closeHour.in(region: Region.current).toFormat("hh:mm aa"))
                    .font(CustomFont.proximaNovaSemibold(withSize: 16))
                    .foregroundColor(R.color.text()!.suColor)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.trailing, 6)
                    .onTapGesture {
                        self.activeSheet = .closedHour
                        self.businessHour.isOn = true
                    }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(businessHour.isOn ? .clear : R.color.gray242()!.suColor)
            .opacity(businessHour.isOn ? 1 : 0.6)
            .overlay(
                RoundedRectangle(cornerRadius: .infinity)
                    .stroke(R.color.gray219()!.suColor)
            )
            .cornerRadius(.infinity)

            Toggle("", isOn: $businessHour.isOn)
                .toggleStyle(.switch)
                .labelsHidden()
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .frame(height: 56)
        .sheet(item: $activeSheet) { sheet in
            switch sheet {
            case .openingHour:
                TimePicker(currentDate: $businessHour.openHour, activeSheet: $activeSheet)
            case .closedHour:
                TimePicker(currentDate: $businessHour.closeHour, activeSheet: $activeSheet)
            }
        }
    }
}

struct BusinessHoursView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessHoursView(businessHour: .constant(BusinessHour(weekday: 0)), dayOfWeek: "Mon", activeSheet: nil)
    }
}
