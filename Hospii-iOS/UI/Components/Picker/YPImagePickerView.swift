//
//  YPImagePickerView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/22/21.
//

import SwiftUI
import UIKit
import YPImagePicker

@available(iOS 13.0, *)
public struct YPImagePickerView: UIViewControllerRepresentable {
    public typealias UIViewControllerType = YPImagePicker

    /// Image Picker with UIImagePickerController
    /// - Parameters:
    ///   - allowsEditing: does it allow editing
    ///   - sourceType: source
    ///   - delegate: Image Picker Delegate
    public init(allowsEditing: Bool = false,
                selectionLimit: Int = 10,
                delegate: YPImagePickerView.Delegate)
    {
        self.allowsEditing = allowsEditing
        self.selectionLimit = selectionLimit
        self.delegate = delegate
    }

    private let allowsEditing: Bool
    private let selectionLimit: Int
    private let delegate: YPImagePickerView.Delegate

    public func makeUIViewController(context _: UIViewControllerRepresentableContext<YPImagePickerView>) -> YPImagePicker {
        var config = YPImagePickerConfiguration()
        config.screens = [.library, .photo]
        config.startOnScreen = .library
        config.library.maxNumberOfItems = selectionLimit
        config.library.skipSelectionsGallery = true
        config.library.preSelectItemOnMultipleSelection = true
        config.showsPhotoFilters = allowsEditing
        config.colors.bottomMenuItemBackgroundColor = R.color.white()!

        let picker = YPImagePicker(configuration: config)
        var images: [UIImage] = []
        picker.didFinishPicking { items, cancelled in
            if cancelled {
                delegate.isPresented = false
                delegate.didCancel()
                return
            }
            for item in items {
                switch item {
                case let .photo(photo):
                    images.append(photo.image)
                case .video:
                    break
                }
            }
            delegate.isPresented = false
            delegate.didSelect(images)
        }

        return picker
    }

    public func updateUIViewController(_: YPImagePicker, context _: UIViewControllerRepresentableContext<YPImagePickerView>) {}
}

@available(iOS 13.0, *)
public extension YPImagePickerView {
    class Delegate: NSObject {
        public init(isPresented: Binding<Bool>, didCancel: @escaping () -> Void, didSelect: @escaping ([UIImage]) -> Void) {
            _isPresented = isPresented
            self.didCancel = didCancel
            self.didSelect = didSelect
        }

        @Binding var isPresented: Bool
        let didCancel: () -> Void
        let didSelect: ([UIImage]) -> Void
    }
}
