//
//  CroppedPhotoImageView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import RSKImageCropper
import SwiftUI

struct CroppedPhotoImageView: View {
    var selectedImage: UIImage

    @Binding var image: UIImage?

    var cropMode: RSKImageCropMode?

    var body: some View {
        CroppedImagePicker(selectedImage: selectedImage, image: $image, cropMode: cropMode)
    }
}

struct CroppedPhotoImageView_Previews: PreviewProvider {
    static var previews: some View {
        CroppedPhotoImageView(selectedImage: R.image.black()!, image: .constant(R.image.black()!))
    }
}
