//
//  CroppedImagePicker.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import Foundation
import RSKImageCropper
import SwiftUI

class CroppedImagePickerCoordinator: NSObject, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource {
    @Binding var image: UIImage?

    init(image: Binding<UIImage?>) {
        _image = image
    }

    func imageCropViewController(_: RSKImageCropViewController,
                                 didCropImage croppedImage: UIImage,
                                 usingCropRect _: CGRect,
                                 rotationAngle _: CGFloat)
    {
        image = croppedImage
    }

    func imageCropViewControllerDidCancelCrop(_: RSKImageCropViewController) {
        image = nil
    }

    // MARK: - RSKImageCropViewControllerDataSource

    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect {
        let imageViewWidth = controller.view.frame.width - 32
        let imageViewHeight: CGFloat = imageViewWidth * 15 / 34

        let aspectRatio = CGSize(width: imageViewWidth, height: imageViewHeight)
        let viewWidth = controller.view.frame.width
        let viewHeight = controller.view.frame.height

        var maskWidth = viewWidth
        var maskHeight: CGFloat
        repeat {
            maskHeight = maskWidth * aspectRatio.height / aspectRatio.width
            maskWidth -= 1
        } while maskHeight != floor(maskHeight)
        maskWidth += 1
        let maskSize = CGSize(width: maskWidth, height: maskHeight)
        let maskRect = CGRect(x: (viewWidth - maskSize.width) * 0.5, y: (viewHeight - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)

        return maskRect
    }

    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath {
        let rect = controller.maskRect

        let point1 = CGPoint(x: rect.minX, y: rect.maxY)
        let point2 = CGPoint(x: rect.maxX, y: rect.maxY)
        let point3 = CGPoint(x: rect.maxX, y: rect.minY)
        let point4 = CGPoint(x: rect.minX, y: rect.minY)

        let rectangle = UIBezierPath()
        rectangle.move(to: point1)
        rectangle.addLine(to: point2)
        rectangle.addLine(to: point3)
        rectangle.addLine(to: point4)
        rectangle.close()

        return rectangle
    }

    func imageCropViewControllerCustomMovementRect(_ controller: RSKImageCropViewController) -> CGRect {
        if controller.rotationAngle == 0 {
            return controller.maskRect
        } else {
            let maskRect = controller.maskRect
            let rotationAngle = controller.rotationAngle

            var movementRect = CGRect.zero

            movementRect.size.width = maskRect.width * abs(cos(rotationAngle)) + maskRect.height * abs(sin(rotationAngle))
            movementRect.size.height = maskRect.height * abs(cos(rotationAngle)) + maskRect.width * abs(sin(rotationAngle))

            movementRect.origin.x = maskRect.minX + (maskRect.width - movementRect.width) * 0.5
            movementRect.origin.y = maskRect.minY + (maskRect.height - movementRect.height) * 0.5

            movementRect.origin.x = floor(movementRect.minX)
            movementRect.origin.y = floor(movementRect.minY)
            movementRect = movementRect.integral

            return movementRect
        }
    }
}

struct CroppedImagePicker: UIViewControllerRepresentable {
    var selectedImage: UIImage

    @Binding var image: UIImage?

    var cropMode: RSKImageCropMode?

    func updateUIViewController(_: RSKImageCropViewController, context _: UIViewControllerRepresentableContext<CroppedImagePicker>) {}

    func makeCoordinator() -> CroppedImagePickerCoordinator {
        return CroppedImagePickerCoordinator(image: $image)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<CroppedImagePicker>) -> RSKImageCropViewController {
        let picker = RSKImageCropViewController(image: selectedImage, cropMode: cropMode ?? .circle)
        picker.applyMaskToCroppedImage = false
        picker.avoidEmptySpaceAroundImage = true
        picker.delegate = context.coordinator

        if cropMode == .custom {
            picker.dataSource = context.coordinator
        }

        return picker
    }
}
