//
//  UserTypePicker.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import SwiftUI

struct UserTypePicker: View {
    @Binding var selectedType: UserSignUpTypes

    var body: some View {
        HStack(spacing: 0) {
            ForEach(UserSignUpTypes.allCases, id: \.self) { type in
                ZStack {
                    Rectangle()
                        .frame(height: 45)
                        .foregroundColor((selectedType == type) ? .black : .clear)
                        .cornerRadius(.infinity)

                    Button(action: {
                        self.selectedType = type
                    }) {
                        Text(type.title)
                            .font(Font.textFieldLabel)
                            .foregroundColor((selectedType == type) ? .white : .black)
                            .frame(height: 38)
                            .frame(maxWidth: .infinity)
                    }
                    .contentShape(Rectangle())
                    .frame(height: 38)
                    .frame(maxWidth: .infinity)
                }
                .animation(.easeInOut(duration: 0.3), value: selectedType)
            }
        }
        .overlay(RoundedRectangle(cornerRadius: .infinity).stroke(R.color.black()!.suColor))
    }
}

struct UserTypePicker_Previews: PreviewProvider {
    static var previews: some View {
        UserTypePicker(selectedType: .constant(.user))
    }
}
