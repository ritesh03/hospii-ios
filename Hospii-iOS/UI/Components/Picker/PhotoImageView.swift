//
//  PhotoImageView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import SwiftUI

struct PhotoImageView: View {
    @Binding var image: UIImage?

    var body: some View {
        ImagePicker(image: $image)
    }
}

struct PhotoImageView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoImageView(image: .constant(R.image.black()!))
    }
}
