//
//  TimePicker.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import SwiftDate
import SwiftUI

struct TimePicker: View {
    @Binding var currentDate: Date

    @Binding var activeSheet: ActiveHourPickerSheet?

    var body: some View {
        VStack(alignment: .trailing) {
            Spacer()

            DatePicker("", selection: $currentDate, displayedComponents: .hourAndMinute)
                .labelsHidden()
                .datePickerStyle(.wheel)

            Spacer()

            Button(action: {
                self.activeSheet = nil
            }) {
                Text("Done")
                    .padding()
            }
            .padding(.bottom, 48)
        }
    }
}

struct TimePicker_Previews: PreviewProvider {
    static var previews: some View {
        TimePicker(currentDate: .constant(Date(year: 2022, month: 12, day: 31, hour: 9, minute: 00, region: .current)), activeSheet: .constant(.openingHour))
    }
}
