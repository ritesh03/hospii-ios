//
//  LoadingView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/18/22.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        HStack {
            Spacer()

            ActivityIndicatorView().padding()

            Spacer()
        }
        .frame(maxWidth: .infinity)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
