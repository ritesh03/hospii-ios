//
//  CheckBox.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import SwiftUI

struct CheckBox: View {
    @Binding var isChecked: Bool

    var title: String = ""

    let action: (Bool) -> Void

    func toggle() { isChecked = !isChecked }

    var body: some View {
        Button(action: {
            toggle()
            action(isChecked)
        }) {
            HStack {
                Image(systemName: isChecked ? "checkmark.square" : "square")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 20, height: 20)
                Text(title)
            }
        }
    }
}

struct CheckBox_Previews: PreviewProvider {
    static var previews: some View {
        CheckBox(isChecked: .constant(true), title: "") { _ in
        }
    }
}
