//
//  BusinessPreviewView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Kingfisher
import SwiftUI

struct BusinessPreviewView: View {
    let business: UserBusinessResponse

    var body: some View {
        HStack(alignment: .top) {
            KFImage
                .url(URL(string: business.user?.profilePhoto?.imageUrl ?? ""))
                .placeholder {
                    R.image.defaultProfilePhoto()!.suImage
                        .resizable()
                        .clipped()
                        .scaledToFill()
                        .frame(width: 75, height: 75)
                        .cornerRadius(16)
                }
                .setProcessor(ImageProcessors.thumbnailProcessor)
                .resizable()
                .clipped()
                .scaledToFill()
                .frame(width: 75, height: 75)
                .cornerRadius(16)
                .padding(.leading)
                .padding(.trailing, 8)

            VStack(alignment: .leading, spacing: 8) {
                Text("\(business.user?.firstName ?? "") \(business.user?.lastName ?? "")")
                    .style(.textFieldMessage)
                    .lineLimit(1)
                    .padding(.top, 4)

                Label {
                    Text(business.name)
                        .style(.subTxtGray)
                        .lineLimit(1)
                } icon: {
                    R.image.shopIcon()!.suImage
                }

                Label {
                    Text("\(business.userAddress.address) \(business.userAddress.address2 ?? ""), \(business.userAddress.city ?? ""), \(business.userAddress.state ?? "") \(business.userAddress.code ?? "")")
                        .style(.subTxtGray)
                        .padding(.leading, 4)
                        .lineLimit(2)
                } icon: {
                    R.image.sMapIcon()!.suImage
                }
                .padding(.leading, 2)
            }

            Spacer()

            VStack {
                if let distance = business.distance {
                    let miles = (distance * 1.6).roundedString(toPlaces: 1) ?? ""
                    Text("\(miles) miles")
                        .style(.subTxtGray)
                        .padding(.trailing)
                }

                //                Label {
                //                    Text("4.7")
                //                        .style(.subTextSemibold)
                //                        .lineLimit(1)
                //                        .padding(.leading, -4)
                //                } icon: {
                //                    R.image.starIcon()!.suImage
                //                }
                //                .padding(.trailing, 4)
            }
        }
        .padding(.vertical, 12)
    }
}

struct BusinessPreviewView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessPreviewView(business: UserBusinessResponse(id: 1, name: "Fred Chavez", businessAbout: nil, type: nil, status: nil, phoneNumber: nil, phoneNumberExtension: nil, phoneNumberCountryCode: nil, isPhoneNumberVerified: true, coverPhoto: nil, userBusinessHours: [], userAddress: BusinessAddressResponse(address: "New City One Salon", address2: nil, city: "Los Angeles", state: "CA", code: "90250", latitude: nil, longitude: nil), __serviceFilters__: [], user: nil, distance: nil))
    }
}
