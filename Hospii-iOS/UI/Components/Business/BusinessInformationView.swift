//
//  BusinessInformationView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import PhoneNumberKit
import SwiftDate
import SwiftUI

struct BusinessInformationView: View {
    let business: UserBusinessResponse

    let phoneNumberKit: PhoneNumberKit

    @State var isPresentedMapOptionsActionSheet = false

    var body: some View {
        HStack {
            if let hours = business.userBusinessHours, let todayWeekDay = Date().dayNumberOfWeek(), !hours.isEmpty, let hour = hours.first { todayWeekDay == ($0.weekDay + 2) % 7 }, let closeTime = BusinessHour.from(hour).closeHour.in(region: Region.current), let formattedTime = closeTime.toFormat("hh:mm aa"), closeTime.isInFuture, !hour.isClosed {
                VStack {
                    R.image.businessOpenIcon()!.suImage

                    Text("Open until\n\(formattedTime.uppercased())")
                        .style(.subTextSemibold)
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .minimumScaleFactor(0.5)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
            } else {
                VStack {
                    R.image.businessCloseIcon()!.suImage

                    Text("Closed")
                        .style(.subTextSemibold)
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .minimumScaleFactor(0.5)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
            }

            Button(action: {
                isPresentedMapOptionsActionSheet = true
            }, label: {
                VStack {
                    R.image.businessDirectionIcon()!.suImage

                    Text(R.string.localizable.businessInformationViewGetDirectionsText())
                        .style(.subTextSemibold)
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .minimumScaleFactor(0.5)
                }
            })
            .frame(minWidth: 0, maxWidth: .infinity)

            if let phoneNumber = business.phoneNumber, let countryCode = business.phoneNumberCountryCode, let parsedPhoneNumber = try? phoneNumberKit.parse("\(countryCode)\(phoneNumber)") {
                Button(action: {
                    parsedPhoneNumber.call()
                }, label: {
                    VStack {
                        R.image.businessPhoneIcon()!.suImage

                        Text(R.string.localizable.businessInformationViewCallNowText())
                            .style(.subTextSemibold)
                            .multilineTextAlignment(.center)
                            .lineLimit(2)
                            .minimumScaleFactor(0.5)
                    }
                })
                .frame(minWidth: 0, maxWidth: .infinity)
            }
        }
        .padding(.horizontal)
        .padding(.vertical, 10)
        .background(R.color.yellowLight()!.suColor)
        .cornerRadius(22)
        .actionSheet(isPresented: $isPresentedMapOptionsActionSheet) {
            ActionSheet(
                title: Text(R.string.localizable.businessInformationViewGetDirectionsText()),
                buttons: [
                    .default(Text(R.string.localizable.businessInformationViewAppleMapsDirectionsText())) {
                        MapUtils.shared.openAppleMaps(lat: business.userAddress.latitude, lng: business.userAddress.longitude)
                    },
                    .default(Text(R.string.localizable.businessInformationViewGoogleMapsDirectionsText())) {
                        MapUtils.shared.openGoogleMaps(lat: business.userAddress.latitude, lng: business.userAddress.longitude)
                    },
                    .cancel(),
                ]
            )
        }
    }
}

struct BusinessInformationView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessInformationView(business: UserBusinessResponse(id: 0, name: "", businessAbout: nil, type: nil, status: nil, phoneNumber: nil, phoneNumberExtension: nil, phoneNumberCountryCode: nil, isPhoneNumberVerified: false, coverPhoto: nil, userBusinessHours: nil, userAddress: BusinessAddressResponse(address: "", address2: nil, city: nil, state: nil, code: nil, latitude: nil, longitude: nil), __serviceFilters__: nil, user: nil, distance: nil), phoneNumberKit: PhoneNumberKit())
    }
}
