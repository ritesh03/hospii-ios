//
//  RoundedTextEditor.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import SwiftUI

struct RoundedTextEditor: View {
    @Binding var text: String

    var placeHolder: String

    var error: String

    @State var isEditing = false

    init(text: Binding<String>, placeHolder: String, error: String) {
        _text = text
        self.placeHolder = placeHolder
        self.error = error
    }

    var body: some View {
        ZStack(alignment: .topLeading) {
            if #available(iOS 14.0, *) {
                TextEditor(text: $text)
                    .modifier(RoundedTextEditorModifier(error: error))

                Text(placeHolder)
                    .font(CustomFont.proximaNovaSemibold(withSize: 16))
                    .foregroundColor(R.color.gray162()!.suColor)
                    .padding(.top, 19.5)
                    .padding(.leading, 19.5)
                    .opacity(text.isEmpty ? 1 : 0)
                    .allowsHitTesting(false)
            } else {
                TextView(text: $text, isEditing: $isEditing, placeholder: placeHolder)
                    .modifier(RoundedTextEditorModifier(error: error))
            }
        }
        .frame(height: 120)
    }
}

struct RoundedTextEditorModifier: ViewModifier {
    var error: String

    func body(content: Content) -> some View {
        content
            .style(.textFieldMessage)
            // .disableAutocorrection(true)
            .autocapitalization(.sentences)
            .textFieldStyle(.plain)
            .padding(12)
            .overlay(RoundedRectangle(cornerRadius: 20).stroke(!error.isEmpty ? R.color.red()!.suColor : R.color.gray219()!.suColor))
            .accentColor(R.color.yellow()!.suColor)
    }
}

struct RoundedTextEditor_Previews: PreviewProvider {
    static var previews: some View {
        RoundedTextEditor(text: .constant(""), placeHolder: "ex.hospii123@gmail.com", error: "error")
    }
}
