//
//  PrimaryTextEditor.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import SwiftUI

struct PrimaryTextEditor: View {
    @Binding var text: String

    var placeHolder: String

    var title: String

    var error: String

    init(text: Binding<String>, placeHolder: String, title: String, error: String) {
        _text = text
        self.placeHolder = placeHolder
        self.title = title
        self.error = error
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            Text(title)
                .style(.textFieldLabel)

            let showErrorMessage = !error.isEmpty

            RoundedTextEditor(text: $text, placeHolder: placeHolder, error: error)

            if showErrorMessage {
                Text(error)
                    .style(.textFieldErrorMessage)
            }
        }
    }
}

struct PrimaryTextEditor_Previews: PreviewProvider {
    static var previews: some View {
        PrimaryTextEditor(
            text: .constant("test"),
            placeHolder: "ex. hospii123@gmail.com",
            title: "Phone number, username or email",
            error: "The email id not registered with us."
        )
    }
}
