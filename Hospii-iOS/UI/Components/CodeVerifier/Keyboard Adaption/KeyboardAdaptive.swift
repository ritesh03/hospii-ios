import Combine
import SwiftUI

struct KeyboardAdaptive: ViewModifier {
    @State private var bottomPadding: CGFloat = 0

    func body(content: Content) -> some View {
        if #available(iOS 14.0, *) {
            return AnyView(content)
        } else {
            let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow })
            let windowHeight = window?.bounds.height ?? 0
            return AnyView(content
                .padding(.bottom, bottomPadding)
                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                    let keyboardTop = windowHeight - keyboardHeight
                    let focusedInputBottom = UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0
                    let textfieldHeight = UIResponder.currentFirstResponder?.globalFrame?.height ?? 0
                    self.bottomPadding = max(0, focusedInputBottom - keyboardTop + textfieldHeight)
                }.animation(.easeOut(duration: 0.16)))
        }
    }
}

extension View {
    func keyboardAdaptive() -> some View {
        ModifiedContent(content: self, modifier: KeyboardAdaptive())
    }
}
