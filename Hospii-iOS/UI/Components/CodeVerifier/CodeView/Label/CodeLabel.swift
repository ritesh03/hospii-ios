#if canImport(UIKit) && (os(iOS) || targetEnvironment(macCatalyst))
    import SwiftUI

    struct CodeLabel: View {
        @State var labelState: CodeLabelState
        @State var style: SecureCodeStyle
        @State var isError: Error?

        private var lineColor: Color {
            labelState.showingError || isError != nil ? style.errorLineColor : style.normalLineColor
        }

        private var textColor: Color {
            labelState.showingError || isError != nil ? style.errorTextColor : style.normalTextColor
        }

        public var body: some View {
            VStack(spacing: style.carrierSpacing) {
                if !labelState.prompting {
                    Text(labelState.textLabel)
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(textColor)
                        .frame(width: style.labelWidth, height: style.labelHeight, alignment: .center)
                        .style(.textFieldMessage)
                        .style(.textFieldMessage)
                        .keyboardType(.numberPad)
                        .overlay(RoundedRectangle(cornerRadius: 16).stroke(R.color.gray219()!.suColor))
                        .allowsHitTesting(false)
                } else {
                    ZStack {
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(R.color.gray219()!.suColor)
                            .frame(width: style.labelWidth, height: style.labelHeight, alignment: .center)
                            .allowsHitTesting(false)
                        Carrier(height: style.carrierHeight, color: style.carrierColor)
                            .keyboardType(.numberPad)
                            .allowsHitTesting(false)
                    }
                }
            }
        }
    }

    struct CodeLabel_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                CodeLabel(labelState: .filled(text: "2"), style: Styles.defaultStyle, isError: nil)
                CodeLabel(labelState: .prompting, style: Styles.defaultStyle, isError: nil)
                CodeLabel(labelState: .error(text: "3"), style: Styles.defaultStyle, isError: nil)
            }
        }
    }
#endif
