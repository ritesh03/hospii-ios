import Foundation

enum CodeLabelState: Identifiable {
    var id: UUID {
        UUID()
    }

    case error(text: String)
    case filled(text: String)
    case empty
    case prompting

    var textLabel: String {
        switch self {
        case let .filled(text: text), let .error(text: text):
            return text
        default:
            return ""
        }
    }

    var showingError: Bool {
        switch self {
        case .error:
            return true
        default:
            return false
        }
    }

    var prompting: Bool {
        switch self {
        case .prompting:
            return true
        default:
            return false
        }
    }
}
