#if canImport(UIKit) && (os(iOS) || targetEnvironment(macCatalyst))
    import SwiftUI

    struct CodeView: View {
        var fields: [CodeLabelState]
        @State var style: SecureCodeStyle
        @State var isError: Error?

        var body: some View {
            HStack(alignment: .bottom, spacing: style.labelSpacing) {
                ForEach(fields) { labelState in
                    CodeLabel(labelState: labelState, style: self.style, isError: isError)
                }
            }
        }
    }

    struct CodeView_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                CodeView(fields: [.prompting, .empty, .empty, .empty, .empty], style: Styles.defaultStyle, isError: nil)
                CodeView(fields: [.prompting, .empty, .empty, .empty, .empty], style: Styles.defaultStyle, isError: nil).environment(\.colorScheme, .dark)
            }
        }
    }
#endif
