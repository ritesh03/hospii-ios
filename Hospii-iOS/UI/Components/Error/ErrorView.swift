//
//  ErrorView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/13/21.
//

import SwiftUI

struct ErrorView: View {
    let error: Error
    let retryAction: () -> Void

    var body: some View {
        HStack {
            Spacer()

            VStack {
                Text("Something went wrong,\nplease try again.")
                    .style(.listSectionTitle)
                    .multilineTextAlignment(.center)
                    .padding(.top)
                //            Text(error.localizedDescription)
                //                .font(.callout)
                //                .multilineTextAlignment(.center)
                //                .padding(.bottom, 40).padding()
                Button(action: retryAction, label: { Text("Retry").bold() })
                    .padding()
            }

            Spacer()
        }
        .frame(maxWidth: .infinity)
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(error: NSError(domain: "", code: 0, userInfo: [
            NSLocalizedDescriptionKey: "Something went wrong",
        ]),
        retryAction: {})
    }
}
