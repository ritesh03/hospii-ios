//
//  OpeningHoursText.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import SwiftDate
import SwiftUI

struct OpeningHoursText: View {
    @State var businessHour: BusinessHour

    var body: some View {
        HStack {
            Circle()
                .fill(R.color.black()!.suColor)
                .frame(width: 8, height: 8)

            Text(weekdayString(weekday: businessHour.weekday))
                .style(isOpened() ? .subTextBold : .subText)
                .minimumScaleFactor(0.5)
                .frame(width: 80, alignment: .leading)

            if let interval = businessHour.interval {
                Text(interval)
                    .style(.subText)
            }

            Spacer()
        }
    }

    func isOpened() -> Bool {
        if let todayWeekDay = Date().dayNumberOfWeek(), (businessHour.weekday + 2) % 7 == todayWeekDay, businessHour.isOn {
            let closeTime = businessHour.closeHour.in(region: Region.current)
            let openTime = businessHour.openHour.in(region: Region.current)
            if closeTime.isInFuture, !openTime.isInFuture {
                return true
            }
        }
        return false
    }
    
    private func weekdayString(weekday: Int) -> String {
        switch weekday {
        case 0:
            return R.string.localizable.commonDaysOfWeekMonday()
        case 1:
            return R.string.localizable.commonDaysOfWeekTuesday()
        case 2:
            return R.string.localizable.commonDaysOfWeekWednesday()
        case 3:
            return R.string.localizable.commonDaysOfWeekThursday()
        case 4:
            return R.string.localizable.commonDaysOfWeekFriday()
        case 5:
            return R.string.localizable.commonDaysOfWeekSaturday()
        case 6:
            return R.string.localizable.commonDaysOfWeekSunday()
        default:
            return ""
        }
    }
}

struct OpeningHoursText_Previews: PreviewProvider {
    static var previews: some View {
        OpeningHoursText(businessHour: BusinessHour(weekday: 0))
    }
}
