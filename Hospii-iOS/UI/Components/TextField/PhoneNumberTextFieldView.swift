//
//  PhoneNumberTextFieldView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/18/21.
//

import PhoneNumberKit
import SwiftUI
import UIKit

struct PhoneNumberTextFieldView: UIViewRepresentable {
    @Binding var phoneNumber: String
    private let textField = PhoneNumberTextField()

    func makeUIView(context: Context) -> PhoneNumberTextField {
        textField.addTarget(context.coordinator, action: #selector(Coordinator.onTextChange), for: .editingChanged)
        textField.withFlag = true
        textField.font = R.font.proximaNovaSemibold(size: 16)
        textField.withPrefix = true
        textField.withExamplePlaceholder = true
        textField.withDefaultPickerUI = true
        textField.text = phoneNumber
        return textField
    }

    func updateUIView(_: PhoneNumberTextField, context _: Context) {}

    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }

    typealias UIViewType = PhoneNumberTextField

    class Coordinator: NSObject, UITextFieldDelegate {
        var delegate: PhoneNumberTextFieldView

        init(_ delegate: PhoneNumberTextFieldView) {
            self.delegate = delegate
        }

        @objc func onTextChange(textField: UITextField) {
            delegate.phoneNumber = textField.text!
        }
    }
}
