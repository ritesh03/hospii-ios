//
//  MainTextField.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import ResponsiveTextField
import SwiftUI

struct PrimaryTextField: View {
    @Binding var text: String

    var placeHolder: String

    var title: String

    var error: String

    let style: RoundedTextFieldStyle

    let configuration: ResponsiveTextField.Configuration?

    let firstResponderDemand: Binding<FirstResponderDemand?>?

    let returnType: UIReturnKeyType?

    let handleReturn: (() -> Void)?

    init(text: Binding<String>, placeHolder: String, title: String, error: String, style: RoundedTextFieldStyle = .normalTextField, firstResponderDemand: Binding<FirstResponderDemand?>? = nil, configuration: ResponsiveTextField.Configuration? = .noCorrection, returnType: UIReturnKeyType? = .default, handleReturn: (() -> Void)? = nil) {
        _text = text
        self.placeHolder = placeHolder
        self.title = title
        self.error = error
        self.style = style
        self.firstResponderDemand = firstResponderDemand
        self.configuration = configuration
        self.returnType = returnType
        self.handleReturn = handleReturn
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            Text(title)
                .style(.textFieldLabel)

            let showErrorMessage = !error.isEmpty

            RoundedTextField(text: $text, placeHolder: placeHolder, error: error, style: style, firstResponderDemand: firstResponderDemand, configuration: configuration, handleReturn: handleReturn)

            if showErrorMessage {
                Text(error)
                    .style(.textFieldErrorMessage)
            }
        }
    }
}

struct PrimaryTextField_Previews: PreviewProvider {
    static var previews: some View {
        PrimaryTextField(
            text: .constant("test"),
            placeHolder: "ex. hospii123@gmail.com",
            title: "Phone number, username or email",
            error: "The email id not registered with us."
        )
    }
}
