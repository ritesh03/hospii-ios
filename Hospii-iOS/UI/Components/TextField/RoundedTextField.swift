//
//  RoundedTextField.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import ResponsiveTextField
import SwiftUI

enum RoundedTextFieldStyle {
    case normalTextField
    case secureTextField
    case phoneNumberTextField
}

struct RoundedTextField: View {
    @State private var isSecured: Bool = true

    @Binding var text: String

    var placeHolder: String

    var error: String

    let style: RoundedTextFieldStyle

    let configuration: ResponsiveTextField.Configuration?

    let firstResponderDemand: Binding<FirstResponderDemand?>?

    let returnType: UIReturnKeyType?

    let handleReturn: (() -> Void)?

    init(text: Binding<String>, placeHolder: String, error: String, style: RoundedTextFieldStyle = .normalTextField, firstResponderDemand: Binding<FirstResponderDemand?>? = nil, configuration: ResponsiveTextField.Configuration? = nil, returnType: UIReturnKeyType? = .default, handleReturn: (() -> Void)? = nil) {
        _text = text
        self.placeHolder = placeHolder
        self.error = error
        self.style = style
        self.firstResponderDemand = firstResponderDemand
        self.configuration = configuration
        self.returnType = returnType
        self.handleReturn = handleReturn
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            if style == .normalTextField {
                if let configuration = configuration {
                    ResponsiveTextField(
                        placeholder: placeHolder,
                        text: $text,
                        firstResponderDemand: firstResponderDemand,
                        configuration: configuration,
                        handleReturn: {
                            handleReturn?()
                        }
                    )
                    .responsiveKeyboardReturnType(.next)
                    .padding(.horizontal, 20)
                    .frame(height: 56)
                    .modifier(RoundedTextFieldModifier(error: error))
                } else {
                    ResponsiveTextField(
                        placeholder: placeHolder,
                        text: $text,
                        firstResponderDemand: firstResponderDemand,
                        handleReturn: {
                            handleReturn?()
                        }
                    )
                    .responsiveKeyboardReturnType(.next)
                    .padding(.horizontal, 20)
                    .frame(height: 56)
                    .modifier(RoundedTextFieldModifier(error: error))
                }
            } else if style == .secureTextField {
                ZStack(alignment: .trailing) {
                    if let configuration = configuration {
                        ResponsiveTextField(
                            placeholder: placeHolder,
                            text: $text,
                            isSecure: isSecured,
                            firstResponderDemand: firstResponderDemand,
                            configuration: configuration,
                            handleReturn: {
                                handleReturn?()
                            }
                        )
                        .responsiveKeyboardReturnType(.next)
                        .padding(.horizontal, 20)
                        .frame(height: 56)
                        .modifier(RoundedTextFieldModifier(error: error))
                    } else {
                        ResponsiveTextField(
                            placeholder: placeHolder,
                            text: $text,
                            isSecure: isSecured,
                            firstResponderDemand: firstResponderDemand,
                            handleReturn: {
                                handleReturn?()
                            }
                        )
                        .responsiveKeyboardReturnType(.next)
                        .padding(.horizontal, 20)
                        .frame(height: 56)
                        .modifier(RoundedTextFieldModifier(error: error))
                    }

                    Button(action: {
                        isSecured.toggle()
                    }) {
                        self.isSecured ? R.image.eyesSlash()!.suImage : R.image.eyes()!.suImage
                    }
                    .padding(.horizontal, 18)
                    .frame(height: 56)
                }

            } else {
                PhoneNumberTextFieldView(phoneNumber: $text)
                    .padding(20)
                    .frame(height: 56)
                    .modifier(RoundedTextFieldModifier(error: error))
            }
        }
    }
}

struct RoundedTextFieldModifier: ViewModifier {
    var error: String

    func body(content: Content) -> some View {
        content
            .style(.textFieldMessage)
            .responsiveTextFieldFont(R.font.proximaNovaSemibold(size: 16)!)
            .responsiveTextFieldTextColor(R.color.text()!)
            .overlay(RoundedRectangle(cornerRadius: .infinity).stroke(!error.isEmpty ? R.color.red()!.suColor : R.color.gray219()!.suColor))
            .accentColor(R.color.yellow()!.suColor)
    }
}

struct RoundedTextField_Previews: PreviewProvider {
    static var previews: some View {
        RoundedTextField(text: .constant(""), placeHolder: "ex.hospii123@gmail.com", error: "error", style: .secureTextField)
    }
}
