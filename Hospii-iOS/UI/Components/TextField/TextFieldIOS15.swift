//
//  TextFieldIOS15.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/19/21.
//

import SwiftUI

struct TextFieldIOS15: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TextFieldIOS15_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldIOS15()
    }
}
