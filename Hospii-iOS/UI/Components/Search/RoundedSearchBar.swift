//
//  RoundedSearchBar.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/24/21.
//

import SwiftUI

struct RoundedSearchBar: View {
    var image: Image = R.image.searchIcon()!.suImage

    @State var title: String = R.string.localizable.searchViewSearchBarTitle()

    let action: () -> Void

    var body: some View {
        Button(action: action) {
            HStack {
                image
                    .frame(width: 22, height: 22)
                Text(title)
                    .padding(.horizontal, 2)
                Spacer()
            }
        }
        .font(CustomFont.proximaNovaRegular(withSize: 16))
        .padding(14)
        .contentShape(Rectangle())
        .frame(maxWidth: .infinity)
        .foregroundColor(R.color.gray119()!.suColor)
        .background(R.color.gray242()!.suColor)
        .cornerRadius(.infinity)
    }
}

struct RoundedSearchBar_Previews: PreviewProvider {
    static var previews: some View {
        RoundedSearchBar(image: R.image.searchIcon()!.suImage, title: R.string.localizable.searchViewSearchBarTitle()) {}
    }
}
