//
//  RoundedSearchBarTextField.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/18/21.
//

import SwiftUI

@available(iOS 15.0, *)
struct RoundedSearchBarTextField: View {
    @State var isFocused = false

    @Binding var input: String

    var image: Image = R.image.searchIcon()!.suImage

    @Binding var title: String

    @Binding var placeholderColor: Color

    let action: (String) -> Void

    @FocusState private var focused: Bool

    var body: some View {
        HStack {
            image
                .frame(width: 22, height: 22)

            TextField("",
                      text: Binding<String>(
                          get: { self.input },
                          set: {
                              self.input = $0
                              self.action($0)
                          }
                      ))
                      .placeholder(when: input.isEmpty) {
                          Text(title)
                              .font(CustomFont.proximaNovaRegular(withSize: 16))
                              .foregroundColor(placeholderColor)
                      }
                      .focused($focused)
                      .autocapitalization(.none)
                      .padding(.horizontal, 2)
                      .task {
                          DispatchQueue.main.async {
                              if isFocused {
                                  focused = true
                              }
                          }
                      }
                      .submitLabel(.search)

            Spacer()
        }
        .font(CustomFont.proximaNovaRegular(withSize: 16))
        .padding(14)
        .contentShape(Rectangle())
        .frame(maxWidth: .infinity)
        .foregroundColor(R.color.gray119()!.suColor)
        .background(R.color.gray242()!.suColor)
        .cornerRadius(.infinity)
    }
}

@available(iOS 15.0, *)
struct RoundedSearchBarTextField_Previews: PreviewProvider {
    static var previews: some View {
        RoundedSearchBarTextField(input: .constant(""), title: .constant(R.string.localizable.searchViewSearchBarTitle()), placeholderColor: .constant(R.color.gray119()!.suColor)) { _ in }
    }
}
