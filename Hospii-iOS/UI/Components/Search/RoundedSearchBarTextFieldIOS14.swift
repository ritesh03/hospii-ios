//
//  RoundedSearchBarTextFieldIOS14.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/19/21.
//

import Introspect
import SwiftUI

struct RoundedSearchBarTextFieldIOS14: View {
    @State var isFocused = false

    @Binding var input: String

    var image: Image = R.image.searchIcon()!.suImage

    @Binding var title: String

    let action: (String) -> Void

    var body: some View {
        HStack {
            image
                .frame(width: 22, height: 22)

            TextField("",
                      text: Binding<String>(
                          get: { self.input },
                          set: {
                              self.input = $0
                              self.action($0)
                          }
                      ))
                      .placeholder(when: input.isEmpty) {
                          Text(title)
                              .font(CustomFont.proximaNovaRegular(withSize: 16))
                              .foregroundColor(R.color.gray119()!.suColor)
                      }
                      .autocapitalization(.none)
                      .padding(.horizontal, 2)
                      .introspectTextField { textField in
                          textField.returnKeyType = .search
                          DispatchQueue.main.async {
                              if isFocused {
                                  textField.becomeFirstResponder()
                              }
                          }
                      }

            Spacer()
        }
        .font(CustomFont.proximaNovaRegular(withSize: 16))
        .padding(14)
        .contentShape(Rectangle())
        .frame(maxWidth: .infinity)
        .foregroundColor(R.color.gray119()!.suColor)
        .background(R.color.gray242()!.suColor)
        .cornerRadius(.infinity)
    }
}

struct RoundedSearchBarTextFieldIOS14_Previews: PreviewProvider {
    static var previews: some View {
        RoundedSearchBarTextFieldIOS14(input: .constant(""), title: .constant(R.string.localizable.searchViewSearchBarTitle())) { _ in }
    }
}
