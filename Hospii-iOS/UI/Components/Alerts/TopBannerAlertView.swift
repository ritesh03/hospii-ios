//
//  TopBannerAlertView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/21/21.
//

import SwiftUI

struct TopBannerAlertView: View {
    var message: String

    @State var isHidden: Bool = false

    var body: some View {
        Group {
            Text(message)
                .multilineTextAlignment(.center)
                .foregroundColor(R.color.red()!.suColor)
                .padding(21)
                .style(.primaryText)
        }
        .frame(maxWidth: .infinity, alignment: .top)
        .background(R.color.pink()!.suColor.opacity(0.9))
        .onAppear {
            Timer.scheduledTimer(withTimeInterval: 2.5, repeats: false) { _ in
                withAnimation(.easeInOut(duration: 1.5)) {
                    self.isHidden = true
                }
            }
        }
        .opacity(isHidden ? 0 : 1)
    }
}

struct TopBannerAlertView_Previews: PreviewProvider {
    static var previews: some View {
        TopBannerAlertView(message: R.string.localizable.logInViewErrorBannerText())
    }
}
