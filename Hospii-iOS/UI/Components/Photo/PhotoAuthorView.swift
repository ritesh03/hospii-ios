//
//  PhotoAuthorView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/25/21.
//

import Kingfisher
import SwiftDate
import SwiftUI

struct PhotoAuthorView: View {
    @State var user: BusinessOwner?

    @State var createdAt: String?

    var body: some View {
        HStack(spacing: 12) {
            KFImage
                .url(URL(string: user?.profilePhoto?.imageUrl ?? ""))
                .placeholder {
                    R.image.defaultProfilePhoto()!.suImage
                        .resizable()
                        .clipped()
                        .scaledToFit()
                        .frame(width: 40, height: 40)
                        .clipShape(Circle())
                }
                .setProcessor(ImageProcessors.avatarProcessor)
                .resizable()
                .scaledToFill()
                .frame(width: 40, height: 40)
                .clipShape(Circle())

            VStack(alignment: .leading, spacing: 4) {
                Text("\(user?.firstName ?? "") \(user?.lastName ?? "")")
                    .font(CustomFont.proximaNovaSemibold(withSize: 16))
                    .foregroundColor(R.color.text()!.suColor)

                Text(createdAt?.toDate(region: Region.UTC)?.convertTo(region: .current).toFormat("MMM dd yyyy") ?? "")
                    .font(CustomFont.proximaNovaRegular(withSize: 13))
                    .foregroundColor(R.color.gray119()!.suColor)
            }

            Spacer()
        }
    }
}

struct PhotoAuthorView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoAuthorView()
    }
}
