//
//  SimpleRefreshingView.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/15/21.
//

import SwiftUI

struct SimpleRefreshingView: View {
    var body: some View {
        ProgressView()
            .padding()
            .frame(height: 60)
    }
}

struct SimpleRefreshingView_Previews: PreviewProvider {
    static var previews: some View {
        SimpleRefreshingView()
    }
}
