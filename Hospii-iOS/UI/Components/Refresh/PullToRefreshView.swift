import SwiftUI

struct PullToRefreshView<Header, Content, Footer> {
    private let header: Header
    private let footer: Footer

    @Binding var isHeaderRefreshing: Bool
    @Binding var isFooterRefreshing: Bool
    @Binding var isNoMoreData: Bool

    let onHeaderRefresh: (() -> Void)?
    let onFooterRefresh: (() -> Void)?

    private let content: () -> Content

    @State private var headerRefreshData = RefreshData()
    @State private var footerRefreshData = RefreshData()
}

extension PullToRefreshView: View where Header: View, Content: View, Footer: View {
    init(header: Header,
         footer: Footer,
         isHeaderRefreshing: Binding<Bool>?,
         isFooterRefreshing: Binding<Bool>?,
         isNoMoreData: Binding<Bool>?,
         onHeaderRefresh: (() -> Void)?,
         onFooterRefresh: (() -> Void)?,
         @ViewBuilder content: @escaping () -> Content)
    {
        self.header = header
        self.footer = footer
        self.content = content
        _isHeaderRefreshing = isHeaderRefreshing ?? .constant(false)
        _isFooterRefreshing = isFooterRefreshing ?? .constant(false)
        _isNoMoreData = isNoMoreData ?? .constant(false)
        self.onHeaderRefresh = onHeaderRefresh
        self.onFooterRefresh = onFooterRefresh
    }

    var body: some View {
        ZStack(alignment: .top) {
            header
                .opacity(dynamicHeaderOpacity)
                .frame(maxWidth: .infinity)

            GeometryReader { proxy in
                ScrollView {
                    VStack(spacing: 0) {
                        content()
                            .anchorPreference(key: HeaderBoundsPreferenceKey.self, value: .bounds, transform: {
                                [.init(bounds: $0)]
                            })
                            .anchorPreference(key: FooterBoundsPreferenceKey.self, value: .bounds, transform: {
                                [.init(bounds: $0)]
                            })

                        footer
                            .opacity(dynamicFooterOpacity)
                            .frame(maxWidth: .infinity)
                            .anchorPreference(key: FooterBoundsPreferenceKey.self, value: .bounds, transform: {
                                [.init(bounds: $0)]
                            })
                    }
                    .padding(.top, dynamicHeaderPadding)
                }
                .animation(.spring(), value: isHeaderRefreshing)
                .onChange(of: isHeaderRefreshing, perform: { value in
                    if !value {
                        self.headerRefreshData.refreshState = .stopped
                    }
                })
                .onChange(of: isFooterRefreshing, perform: { value in
                    if !value {
                        self.footerRefreshData.refreshState = .stopped
                    }
                })
                .backgroundPreferenceValue(HeaderBoundsPreferenceKey.self) { value -> Color in
                    DispatchQueue.main.async {
                        calculateHeaderRefreshState(proxy, value: value)
                    }
                    return Color.clear
                }
                .backgroundPreferenceValue(FooterBoundsPreferenceKey.self) { value -> Color in
                    DispatchQueue.main.async {
                        calculateFooterRefreshState(proxy, value: value)
                    }
                    return Color.clear
                }
            }
        }
    }

    var dynamicHeaderOpacity: Double {
        if headerRefreshData.refreshState == .invalid {
            return 0.0
        }
        if headerRefreshData.refreshState == .stopped {
            return headerRefreshData.progress
        }
        return 1.0
    }

    var dynamicFooterOpacity: Double {
        if isNoMoreData {
            return 0.0
        }
        if footerRefreshData.refreshState == .invalid {
            return 0.0
        }
        if footerRefreshData.refreshState == .stopped {
            return footerRefreshData.progress
        }
        return 1.0
    }

    var dynamicHeaderPadding: CGFloat {
        return (headerRefreshData.refreshState == .loading) ? 50.0 : 0.0
    }
}

extension PullToRefreshView {
    private func calculateHeaderRefreshState(_ proxy: GeometryProxy, value: [HeaderBoundsPreferenceKey.Item]) {
        guard let bounds = value.first?.bounds, !(header is EmptyView) else {
            return
        }

        // caculate state

        guard headerRefreshData.refreshState != .loading else {
            return
        }

        let headerFrame = proxy[bounds]

        let y = headerFrame.minY
        let threshold: CGFloat = 110.0
        let topDistance: CGFloat = 30.0

        if threshold != headerRefreshData.thresold {
            headerRefreshData.thresold = threshold
        }

        if y >= headerRefreshData.thresold, headerFrame.width == proxy.size.width, headerRefreshData.refreshState == .invalid {
            headerRefreshData.refreshState = .stopped
        }

        var contentOffset = y

        if contentOffset == 0 {
            headerRefreshData.progress = 0.0
        }

        guard contentOffset > topDistance else {
            return
        }

        contentOffset -= topDistance

        if contentOffset <= threshold, headerRefreshData.refreshState == .stopped {
            let oldProgress = headerRefreshData.progress
            let progress = Double(contentOffset / threshold)
            if progress < oldProgress {
                return
            }
            headerRefreshData.progress = (progress >= 1.0) ? 1.0 : progress
        }

        if contentOffset > threshold, headerRefreshData.refreshState == .stopped, headerRefreshData.refreshState != .triggered {
            headerRefreshData.refreshState = .triggered
            headerRefreshData.progress = 1.0
        }

        if contentOffset <= threshold, headerRefreshData.refreshState == .triggered {
            headerRefreshData.refreshState = .loading
            headerRefreshData.progress = 1.0
            isHeaderRefreshing = true
            onHeaderRefresh?()
        }
    }

    private func calculateFooterRefreshState(_ proxy: GeometryProxy, value: [FooterBoundsPreferenceKey.Item]) {
        guard let bounds = value.last?.bounds else {
            return
        }
        guard let contentBounds = value.first?.bounds else {
            return
        }

        guard footerRefreshData.refreshState != .loading else {
            return
        }

        let footerFrame = proxy[bounds]
        let contentFrame = proxy[contentBounds]

        let y = footerFrame.minY
        let threshold = footerFrame.height
        let bottomDistance: CGFloat = 20.0

        let scrollViewHeight = min(proxy.size.height, contentFrame.height)

        if threshold != footerRefreshData.thresold {
            footerRefreshData.thresold = threshold
        }

        if y >= proxy.size.width, footerFrame.width == proxy.size.width, footerRefreshData.refreshState == .invalid {
            footerRefreshData.refreshState = .stopped
        }

        var contentOffset = scrollViewHeight - y

        if contentOffset == 0 {
            footerRefreshData.progress = 0.0
        }

        guard contentOffset > bottomDistance else {
            return
        }

        if isNoMoreData {
            footerRefreshData.refreshState = .stopped
            return
        }

        contentOffset -= bottomDistance

        if contentOffset <= threshold, footerRefreshData.refreshState == .stopped {
            let progress = Double(contentOffset / threshold)
            footerRefreshData.progress = (progress >= 1.0) ? 1.0 : progress
        }

        if contentOffset > threshold, footerRefreshData.refreshState == .stopped, footerRefreshData.refreshState != .triggered {
            footerRefreshData.refreshState = .triggered
            footerRefreshData.progress = 1.0
        }

        if contentOffset <= threshold, footerRefreshData.refreshState == .triggered, footerRefreshData.refreshState != .loading {
            if !isNoMoreData {
                footerRefreshData.refreshState = .loading
                footerRefreshData.progress = 0.0
                isFooterRefreshing = true
                onFooterRefresh?()
            }
        }
    }
}
