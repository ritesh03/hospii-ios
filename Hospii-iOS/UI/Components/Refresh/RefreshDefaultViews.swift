import SwiftUI

struct RefreshDefaultHeader: View {
    @Environment(\.headerRefreshData) private var headerRefreshData

    var body: some View {
        let state = headerRefreshData.refreshState
        if state == .invalid {
            Spacer()
                .padding()
                .frame(height: 60)
        } else {
            ProgressView()
                .padding()
                .frame(height: 60)
        }
    }
}

struct RefreshDefaultFooter: View {
    @Environment(\.footerRefreshData) private var footerRefreshData

    var body: some View {
        let state = footerRefreshData.refreshState
        if state == .invalid {
            Spacer()
                .padding()
                .frame(height: 60)
        } else {
            ProgressView()
                .padding()
                .frame(height: 60)
        }
    }
}
