//
//  NavigationBarModifier.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/14/21.
//

import SwiftUI

struct NavigationBarModifier: ViewModifier {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var title: String

    // @Binding var isPresented: Bool

    func body(content: Content) -> some View {
        content
            .navigationTitle(title)
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: HStack {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }) {
                    R.image.back()!.suImage
                }
                //
                //                Text(title)
                //                    .style(.navTitle)
                //                    .lineLimit(1)
                //                    .frame(maxWidth: 200)
            })
    }
}
