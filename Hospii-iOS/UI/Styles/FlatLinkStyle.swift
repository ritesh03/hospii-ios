//
//  FlatLinkStyle.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/21/21.
//

import SwiftUI

struct FlatLinkStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
    }
}
