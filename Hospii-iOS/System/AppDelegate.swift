
import ChatProvidersSDK
import ChatSDK
import Combine
import CommonUISDK
import CoreStore
import Firebase
import FirebaseMessaging
import IQKeyboardManagerSwift
import Kingfisher
import KingfisherWebP
import Mixpanel
import UIKit
import UserNotifications
import UXCam
import ZendeskCoreSDK

typealias NotificationPayload = [AnyHashable: Any]
typealias FetchCompletion = (UIBackgroundFetchResult) -> Void

@UIApplicationMain
final class AppDelegate: UIResponder,
    UIApplicationDelegate,
    UNUserNotificationCenterDelegate,
    MessagingDelegate
{
    lazy var systemEventsHandler: SystemEventsHandler? = self.systemEventsHandler(UIApplication.shared)

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIScrollView.appearance().keyboardDismissMode = .onDrag
        UITabBar.appearance().barTintColor = R.color.white()

        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.enable = false

        KingfisherManager.shared.defaultOptions += [
            .processor(WebPProcessor.default),
            .cacheSerializer(WebPSerializer.default),
        ]

        let sql = SQLiteStore()
        sql.localStorageOptions = .allowSynchronousLightweightMigration
        try! CoreStoreDefaults.dataStack.addStorageAndWait(sql)

        // Initialize zendesk
        Zendesk.initialize(appId: ApiConstants.ZENDESK_APP_ID,
                           clientId: ApiConstants.ZENDESK_CLIENT_ID,
                           zendeskUrl: ApiConstants.ZENDESK_URL)

        // Configure zendesk
        Chat.initialize(accountKey: ApiConstants.ZENDESK_KEY)
        // Configure zendesk color theme
        CommonTheme.currentTheme.primaryColor = R.color.yellow()!

        // Configure Mixpanel
        Mixpanel.initialize(token: ApiConstants.MIXPANEL_KEY)

        // Configure Firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UIApplication.shared.registerForRemoteNotifications()

        // Configure UXCam for testing will enable once we have an alpha app version
        // since trial starts when we start collecting data
         UXCam.optIntoSchematicRecordings()
         UXCam.start(withKey:"xdl4x4maad5jouy")

        return true
    }

    func application(_: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        systemEventsHandler?.handlePushRegistration(result: .success(deviceToken))
        Messaging.messaging().apnsToken = deviceToken
    }

    func application(_: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        systemEventsHandler?.handlePushRegistration(result: .failure(error))
    }

    func application(_: UIApplication,
                     didReceiveRemoteNotification userInfo: NotificationPayload,
                     fetchCompletionHandler completionHandler: @escaping FetchCompletion)
    {
        systemEventsHandler?
            .appDidReceiveRemoteNotification(payload: userInfo, fetchCompletion: completionHandler)
    }

    private func systemEventsHandler(_ application: UIApplication) -> SystemEventsHandler? {
        return sceneDelegate(application)?.systemEventsHandler
    }

    private func sceneDelegate(_ application: UIApplication) -> SceneDelegate? {
        return application.windows
            .compactMap { $0.windowScene?.delegate as? SceneDelegate }
            .first
    }

    func userNotificationCenter(_: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler _: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo

        debugPrint("in-app push notification: \(userInfo)")
    }

    func userNotificationCenter(_: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let userInfo = response.notification.request.content.userInfo
        debugPrint("push notification: \(userInfo)")
        completionHandler()
    }
}
