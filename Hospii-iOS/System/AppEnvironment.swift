import Combine
import UIKit
import Valet

struct AppEnvironment {
    let container: DIContainer
    let systemEventsHandler: SystemEventsHandler
}

extension AppEnvironment {
    static func bootstrap() -> AppEnvironment {
        let appState = Store<AppState>(AppState())
        let webRepositories = configuredWebRepositories(appState: appState)
        let dbRepositories = configuredDBRepositories(appState: appState)
        let services = configuredServices(appState: appState,
                                          dbRepositories: dbRepositories,
                                          webRepositories: webRepositories)
        let diContainer = DIContainer(appState: appState, services: services)
        let deepLinksHandler = RealDeepLinksHandler(container: diContainer)
        let pushNotificationsHandler = RealPushNotificationsHandler(deepLinksHandler: deepLinksHandler)
        let systemEventsHandler = RealSystemEventsHandler(
            container: diContainer, deepLinksHandler: deepLinksHandler,
            pushNotificationsHandler: pushNotificationsHandler,
            pushTokenWebRepository: webRepositories.pushTokenWebRepository
        )

        // Fetch access tokens from keychain.
        let valet = Valet.valet(with: Identifier(nonEmpty: KeyChainConstants.SERVICE)!, accessibility: .whenUnlocked)

        let accessToken = try? valet.string(forKey: KeyChainConstants.ACCESS_TOKEN)
        appState[\.userData].accessToken = accessToken
        appState[\.userData].refreshToken = try? valet.string(forKey: KeyChainConstants.REFRESH_TOKEN)
        appState[\.userData].expiresIn = try? valet.string(forKey: KeyChainConstants.EXPIRES_IN)
        appState[\.userData].createdAt = try? valet.string(forKey: KeyChainConstants.CREATED_AT)
        appState[\.userData].userId = try? valet.string(forKey: KeyChainConstants.USER_ID)
        appState[\.routing].contentView.accessToken = accessToken

        return AppEnvironment(container: diContainer,
                              systemEventsHandler: systemEventsHandler)
    }

    private static func configuredWebRepositories(appState: Store<AppState>) -> DIContainer.WebRepositories {
        let authWebRepository = AuthWebRepository(appState: appState)
        let usersWebRepository = UsersWebRepository(appState: appState)
        let businessesWebRepository = BusinessesWebRepository(appState: appState)
        let pushTokenWebRepository = PushTokenWebRepository()
        let mediaWebRepository = MediaWebRepository(appState: appState)
        let searchWebRepository = SearchWebRepository(appState: appState)

        return .init(authWebRepository: authWebRepository,
                     usersWebRepository: usersWebRepository,
                     businessesWebRepository: businessesWebRepository,
                     pushTokenWebRepository: pushTokenWebRepository,
                     mediaWebRepository: mediaWebRepository,
                     searchWebRepository: searchWebRepository)
    }

    private static func configuredDBRepositories(appState _: Store<AppState>) -> DIContainer.DBRepositories {
        let authDBRepository = AuthDBRepository()
        let usersDBRepository = UsersDBRepository()
        let businessesDBRepository = BusinessesDBRepository()
        let searchDBRepository = SearchDBRepository()
        return .init(authDBRepository: authDBRepository, usersDBRepository: usersDBRepository, businessesDBRepository: businessesDBRepository, searchDBRepository: searchDBRepository)
    }

    private static func configuredServices(appState: Store<AppState>,
                                           dbRepositories: DIContainer.DBRepositories,
                                           webRepositories: DIContainer.WebRepositories) -> DIContainer.Services
    {
        let authService = RealAuthService(
            webRepository: webRepositories.authWebRepository,
            dbRepository: dbRepositories.authDBRepository,
            appState: appState
        )

        let usersService = RealUsersService(
            webRepository: webRepositories.usersWebRepository,
            dbRepository: dbRepositories.usersDBRepository,
            appState: appState
        )

        let businessesService = RealBusinessesService(
            webRepository: webRepositories.businessesWebRepository,
            dbRepository: dbRepositories.businessesDBRepository,
            appState: appState
        )

        let mediaService = RealMediaService(webRepository: webRepositories.mediaWebRepository, appState: appState)

        let searchService = RealSearchService(webRepository: webRepositories.searchWebRepository, dbRepository: dbRepositories.searchDBRepository, appState: appState)

        let userPermissionsService = RealUserPermissionsService(
            appState: appState, openAppSettings: {
                URL(string: UIApplication.openSettingsURLString).flatMap {
                    UIApplication.shared.open($0, options: [:], completionHandler: nil)
                }
            }
        )

        return .init(authService: authService,
                     usersService: usersService,
                     businessesService: businessesService,
                     userPermissionsService: userPermissionsService,
                     mediaService: mediaService,
                     searchService: searchService)
    }
}

extension DIContainer {
    struct WebRepositories {
        let authWebRepository: AuthWebDataSource
        let usersWebRepository: UsersWebDataSource
        let businessesWebRepository: BusinessesWebDataSource
        let pushTokenWebRepository: PushTokenWebDataSource
        let mediaWebRepository: MediaWebDataSource
        let searchWebRepository: SearchWebDataSource
    }

    struct DBRepositories {
        let authDBRepository: AuthDBDataSource
        let usersDBRepository: UsersDBDataSource
        let businessesDBRepository: BusinessesDBDataSource
        let searchDBRepository: SearchDBDataSource
    }
}

enum ApiConstants {
    static let BASE_URL = "https://api.hospii-beta.com"
    static let MAPBOX_KEY = "pk.eyJ1IjoiY2hlc29uZyIsImEiOiJja3Fld2pzMnUwZGw3MzBuenQ0Y2E1bG1qIn0.Zl7nBKJ_WupZYqB-h-aqHA"
    static let ZENDESK_KEY = "SR4zUNS6yLqNDGaInDQzDkSL7cyQTIj9"
    static let MIXPANEL_KEY = "8d4cef342ce50b0ba93c8988d7512c5a"
    static let SHARE_URL = "https://hospii.page.link/ZCg5"
    static let ZENDESK_APP_ID = "7d7438ac924e38f7cc9d973439f6bd00baf19584d51464cd"
    static let ZENDESK_CLIENT_ID = "mobile_sdk_client_58876b283f58e7d6c73f"
    static let ZENDESK_URL = "https://hospii.zendesk.com"
}

enum KeyChainConstants {
    static let SERVICE = "com.hospii.hospii-iOS"
    static let ACCESS_TOKEN = "access_token"
    static let REFRESH_TOKEN = "refresh_token"
    static let EXPIRES_IN = "expires_in"
    static let CREATED_AT = "created_at"
    static let USER_ID = "user_id"
}
