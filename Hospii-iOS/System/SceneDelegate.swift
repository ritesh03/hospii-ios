import Combine
import Foundation
import SwiftUI
import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var systemEventsHandler: SystemEventsHandler?
    var backgroundTask: UIBackgroundTaskIdentifier?

    func scene(_ scene: UIScene, willConnectTo _: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions)
    {
        let environment = AppEnvironment.bootstrap()
        let contentView = ContentView(viewModel:
            ContentView.ViewModel(container: environment.container))
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)

            window.overrideUserInterfaceStyle = .light

            window.rootViewController = HostingViewController(rootView: contentView)
            self.window = window
            window.makeKeyAndVisible()
        }
        systemEventsHandler = environment.systemEventsHandler
        if !connectionOptions.urlContexts.isEmpty {
            systemEventsHandler?.sceneOpenURLContexts(connectionOptions.urlContexts)
        }
    }

    func scene(_: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        systemEventsHandler?.sceneOpenURLContexts(URLContexts)
    }

    func sceneDidBecomeActive(_: UIScene) {
        systemEventsHandler?.sceneDidBecomeActive()
    }

    func sceneWillResignActive(_: UIScene) {
        systemEventsHandler?.sceneWillResignActive()
    }

    func sceneWillEnterForeground(_: UIScene) {
        endBackgroundTask()
    }

    func sceneDidEnterBackground(_: UIScene) {
        backgroundTask = UIApplication.shared.beginBackgroundTask(withName: "BgTask", expirationHandler: {
            // Ends long-running background task
            self.endBackgroundTask()
        })
    }

    /// Ends long-running background task. Called when app comes to foreground from background.
    private func endBackgroundTask() {
        if let task = backgroundTask {
            UIApplication.shared.endBackgroundTask(task)
            backgroundTask = .invalid
        }
    }
}
