import Combine
import Foundation
import Moya
import SwiftUI
import Valet

protocol AuthService {
    func postLogin(response: LoadableSubject<Tokens>, loginRequest: LoginRequest)

    func postRegister(response: LoadableSubject<Tokens>, request: RegisterRequest)

    func postCheckUsernameAvailability(response: LoadableSubject<CheckUsernameAvilibilityResponse>, request: CheckUsernameAvilibilityRequest)

    func postInitPhoneNumberVerification(response: LoadableSubject<Response>)

    func postVerifyPhoneNumber(response: LoadableSubject<Response>, code: String)

    func putUpdatePassword(response: LoadableSubject<Response>, request: PutPasswordRequest)

    func putResetPassword(response: LoadableSubject<Response>, request: ResetPasswordRequest)

    func postInitiateForgotPasswordVerificationCode(response: LoadableSubject<Response>, request: ForgotPasswordRequest)

    func postCheckForgotPasswordVerificationCode(response: LoadableSubject<ForgotPasswordResponse>, request: ForgotPasswordRequest)
}

struct RealAuthService: AuthService {
    let webRepository: AuthWebDataSource
    let dbRepository: AuthDBDataSource
    let appState: Store<AppState>
    private let valet = Valet.valet(with: Identifier(nonEmpty: KeyChainConstants.SERVICE)!, accessibility: .whenUnlocked)

    init(webRepository: AuthWebDataSource, dbRepository: AuthDBDataSource, appState: Store<AppState>) {
        self.webRepository = webRepository
        self.dbRepository = dbRepository
        self.appState = appState
    }

    func postRegister(response: LoadableSubject<Tokens>, request: RegisterRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        appState[\.routing].mainView.signUpUserType = request.isHairstylist ? .hairstylist : .user

        webRepository
            .postRegister(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .map { response in
                self.saveTokens(tokens: response)
                return response
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postLogin(response: LoadableSubject<Tokens>, loginRequest: LoginRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postLogin(loginRequest: loginRequest)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .map { response in
                self.saveTokens(tokens: response)
                return response
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postCheckUsernameAvailability(response: LoadableSubject<CheckUsernameAvilibilityResponse>, request: CheckUsernameAvilibilityRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        weak var weakAppState = appState

        webRepository
            .postCheckUsernameAvailability(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                weakAppState?[\.userData.signupUsername] = request.username
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postInitPhoneNumberVerification(response: LoadableSubject<Response>) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postInitPhoneNumberVerification()
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postVerifyPhoneNumber(response: LoadableSubject<Response>, code: String) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postVerifyPhoneNumber(code: code)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func putUpdatePassword(response: LoadableSubject<Response>, request: PutPasswordRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .putUpdatePassword(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func putResetPassword(response: LoadableSubject<Response>, request: ResetPasswordRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .putResetPassword(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postInitiateForgotPasswordVerificationCode(response: LoadableSubject<Response>, request: ForgotPasswordRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postInitiateForgotPasswordVerificationCode(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postCheckForgotPasswordVerificationCode(response: LoadableSubject<ForgotPasswordResponse>, request: ForgotPasswordRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postCheckForgotPasswordVerificationCode(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    private func saveTokens(tokens: Tokens) {
        let createdAt = Date().timeIntervalSince1970.description
        appState[\.userData.accessToken] = tokens.accessToken
        appState[\.userData.refreshToken] = tokens.refreshToken
        appState[\.userData.expiresIn] = tokens.expiresIn.description
        appState[\.userData.createdAt] = createdAt

        try? valet.setString(tokens.accessToken, forKey: KeyChainConstants.ACCESS_TOKEN)
        try? valet.setString(tokens.refreshToken, forKey: KeyChainConstants.REFRESH_TOKEN)
        try? valet.setString(tokens.expiresIn.description, forKey: KeyChainConstants.EXPIRES_IN)
        try? valet.setString(createdAt, forKey: KeyChainConstants.CREATED_AT)
    }

    private var requestHoldBackTimeInterval: TimeInterval {
        return ProcessInfo.processInfo.isRunningTests ? 0 : 0.5
    }
}

struct StubCountriesService: AuthService {
    func postRegister(response _: LoadableSubject<Tokens>, request _: RegisterRequest) {}

    func postCheckUsernameAvailability(response _: LoadableSubject<CheckUsernameAvilibilityResponse>, request _: CheckUsernameAvilibilityRequest) {}

    func postLogin(response _: LoadableSubject<Tokens>, loginRequest _: LoginRequest) {}

    func postInitPhoneNumberVerification(response _: LoadableSubject<Response>) {}

    func postVerifyPhoneNumber(response _: LoadableSubject<Response>, code _: String) {}

    func putUpdatePassword(response _: LoadableSubject<Response>, request _: PutPasswordRequest) {}

    func putResetPassword(response _: LoadableSubject<Response>, request _: ResetPasswordRequest) {}

    func postInitiateForgotPasswordVerificationCode(response _: LoadableSubject<Response>, request _: ForgotPasswordRequest) {}

    func postCheckForgotPasswordVerificationCode(response _: LoadableSubject<ForgotPasswordResponse>, request _: ForgotPasswordRequest) {}
}
