//
//  BusinessesService.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import Combine
import Foundation
import Moya
import SwiftUI

protocol BusinessesService {
    func postCreateBusiness(response: LoadableSubject<CreateBusinessResponse>, request: CreateBusinessRequest)

    func putUpdateBusiness(response: LoadableSubject<CreateBusinessResponse>, request: CreateBusinessRequest)

    func postCreateBusinessHours(response: LoadableSubject<Response>, request: CreateBusinessHoursRequest)

    func postCover(response: LoadableSubject<Response>?, imageData: Data, businessId: Int)

    func getServices(response: LoadableSubject<[ServiceResponse]>)

    func getActiveServices(response: LoadableSubject<[ServiceResponse]>)

    func updateActiveServices(response: LoadableSubject<Response>, request: UpdateActiveServicesRequest)

    func getUserBusinessById(response: LoadableSubject<UserBusinessResponse>, businessId: Int)

    func getUserBusinessPhotos(response: LoadableSubject<SearchFeedsResponse>, getPostsRequest: GetPostsRequest)
}

struct RealBusinessesService: BusinessesService {
    let webRepository: BusinessesWebDataSource
    let dbRepository: BusinessesDBDataSource
    let appState: Store<AppState>
    let globalCancelBag = CancelBag()

    init(webRepository: BusinessesWebDataSource, dbRepository: BusinessesDBDataSource, appState: Store<AppState>) {
        self.webRepository = webRepository
        self.dbRepository = dbRepository
        self.appState = appState
    }

    func postCreateBusiness(response: LoadableSubject<CreateBusinessResponse>, request: CreateBusinessRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postCreateBusiness(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postCreateBusinessHours(response: LoadableSubject<Response>, request: CreateBusinessHoursRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postCreateBusinessHours(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func postCover(response: LoadableSubject<Response>?, imageData: Data, businessId: Int) {
        let cancelBag = CancelBag()
        response?.wrappedValue.setIsLoading(cancelBag: response == nil ? globalCancelBag : cancelBag)

        webRepository
            .postCover(imageData: imageData, businessId: businessId)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response?.wrappedValue = $0
            }
            .store(in: response == nil ? globalCancelBag : cancelBag)
    }

    func getServices(response: LoadableSubject<[ServiceResponse]>) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        Just<Void>
            .withErrorType(ResponseError.self)
            .flatMap { _ -> AnyPublisher<[ServiceResponse], ResponseError> in
                self.refrshServices(response: response)
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getActiveServices(response: LoadableSubject<[ServiceResponse]>) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getActiveServices()
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func updateActiveServices(response: LoadableSubject<Response>, request: UpdateActiveServicesRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .upateActiveServices(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func putUpdateBusiness(response: LoadableSubject<CreateBusinessResponse>, request: CreateBusinessRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .putUpdateBusiness(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getUserBusinessById(response: LoadableSubject<UserBusinessResponse>, businessId: Int) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getBusinessById(id: businessId)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getUserBusinessPhotos(response: LoadableSubject<SearchFeedsResponse>, getPostsRequest: GetPostsRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getBusinessPhotos(getPostsRequest: getPostsRequest)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .map {
                if $0.meta.currentPage == 1 {
                    return $0
                } else {
                    return SearchFeedsResponse(data: (response.wrappedValue.value?.data ?? []) + $0.data, meta: $0.meta)
                }
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    private func refrshServices(response _: LoadableSubject<[ServiceResponse]>) -> AnyPublisher<[ServiceResponse], ResponseError> {
        return webRepository
            .getServices()
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .flatMap { [dbRepository] in
                dbRepository.storeServices(services: $0)
            }
            .eraseToAnyPublisher()
    }

    private func refrshActiveServices(response _: LoadableSubject<[ServiceResponse]>) -> AnyPublisher<[ServiceResponse], ResponseError> {
        return webRepository
            .getActiveServices()
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .flatMap { [dbRepository] in
                dbRepository.storeServices(services: $0)
            }
            .eraseToAnyPublisher()
    }

    private var requestHoldBackTimeInterval: TimeInterval {
        return ProcessInfo.processInfo.isRunningTests ? 0 : 0.5
    }
}

struct StubBusinessesService: BusinessesService {
    func postCreateBusiness(response _: LoadableSubject<CreateBusinessResponse>, request _: CreateBusinessRequest) {}

    func postCreateBusinessHours(response _: LoadableSubject<Response>, request _: CreateBusinessHoursRequest) {}

    func postCover(response _: LoadableSubject<Response>?, imageData _: Data, businessId _: Int) {}

    func getServices(response _: LoadableSubject<[ServiceResponse]>) {}

    func getActiveServices(response _: LoadableSubject<[ServiceResponse]>) {}

    func updateActiveServices(response _: LoadableSubject<Response>, request _: UpdateActiveServicesRequest) {}

    func putUpdateBusiness(response _: LoadableSubject<CreateBusinessResponse>, request _: CreateBusinessRequest) {}

    func getUserBusinessById(response _: LoadableSubject<UserBusinessResponse>, businessId _: Int) {}

    func getUserBusinessPhotos(response _: LoadableSubject<SearchFeedsResponse>, getPostsRequest _: GetPostsRequest) {}
}
