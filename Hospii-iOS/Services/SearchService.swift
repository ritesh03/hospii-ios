//
//  SearchService.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/11/21.
//

import Combine
import Foundation
import Moya
import SwiftUI

protocol SearchService {
    func getSuggestions(response: LoadableSubject<SuggestionsResponse>, request: SearchSuggestionsRequest)

    func getTags(response: LoadableSubject<[TagResponse]>, request: SearchSuggestionsRequest)

    func getFeeds(response: LoadableSubject<SearchFeedsResponse>, request: SearchRequest)

    func getSearchResults(response: LoadableSubject<SearchFeedsResponse>, request: SearchRequest)

    func getBusinesses(response: LoadableSubject<SearchBusinessesResponse>, request: SearchRequest)

    func getSortingSuggestions(response: LoadableSubject<[SortingSuggestionResponse]>)
}

struct RealSearchService: SearchService {
    let webRepository: SearchWebDataSource
    let dbRepository: SearchDBDataSource

    let appState: Store<AppState>
    let globalCancelBag = CancelBag()

    init(webRepository: SearchWebDataSource, dbRepository: SearchDBDataSource, appState: Store<AppState>) {
        self.webRepository = webRepository
        self.dbRepository = dbRepository
        self.appState = appState
    }

    private var requestHoldBackTimeInterval: TimeInterval {
        return ProcessInfo.processInfo.isRunningTests ? 0 : 0.5
    }

    func getSuggestions(response: LoadableSubject<SuggestionsResponse>, request: SearchSuggestionsRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getSuggestions(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getTags(response: LoadableSubject<[TagResponse]>, request: SearchSuggestionsRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getTags(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .compactMap { $0.tags }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getFeeds(response: LoadableSubject<SearchFeedsResponse>, request: SearchRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        Just<Void>
            .withErrorType(ResponseError.self)
            .flatMap { _ -> AnyPublisher<SearchFeedsResponse, ResponseError> in
                self.refreshFeeds(response: response, request: request)
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getSearchResults(response: LoadableSubject<SearchFeedsResponse>, request: SearchRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getFeeds(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .map {
                if $0.meta.currentPage == 1 {
                    return $0
                } else {
                    return SearchFeedsResponse(data: (response.wrappedValue.value?.data ?? []) + $0.data, meta: $0.meta)
                }
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getBusinesses(response: LoadableSubject<SearchBusinessesResponse>, request: SearchRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getBusinesses(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .map {
                if $0.meta.currentPage == 1 {
                    return $0
                } else {
                    return SearchBusinessesResponse(data: (response.wrappedValue.value?.data ?? []) + $0.data, meta: $0.meta)
                }
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getSortingSuggestions(response: LoadableSubject<[SortingSuggestionResponse]>) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        Just<Void>
            .withErrorType(ResponseError.self)
            .flatMap { _ -> AnyPublisher<[SortingSuggestionResponse], ResponseError> in
                self.refreshSortingSuggestions(response: response)
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    private func refreshFeeds(response: LoadableSubject<SearchFeedsResponse>, request: SearchRequest) -> AnyPublisher<SearchFeedsResponse, ResponseError> {
        return webRepository
            .getFeeds(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .flatMap { [dbRepository] in
                dbRepository.storeFeeds(feeds: $0)
            }
            .map {
                if $0.meta.currentPage == 1 {
                    return $0
                } else {
                    return SearchFeedsResponse(data: (response.wrappedValue.value?.data ?? []) + $0.data, meta: $0.meta)
                }
            }
            .eraseToAnyPublisher()
    }

    private func refreshSortingSuggestions(response _: LoadableSubject<[SortingSuggestionResponse]>) -> AnyPublisher<[SortingSuggestionResponse], ResponseError> {
        return webRepository
            .getSortingSuggestions()
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .flatMap { [dbRepository] in
                dbRepository.storeSortingSuggestions(sugggestions: $0)
            }
            .eraseToAnyPublisher()
    }
}

struct StubSearchService: SearchService {
    func getSearchResults(response _: LoadableSubject<SearchFeedsResponse>, request _: SearchRequest) {}

    func getSuggestions(response _: LoadableSubject<SuggestionsResponse>, request _: SearchSuggestionsRequest) {}

    func getTags(response _: LoadableSubject<[TagResponse]>, request _: SearchSuggestionsRequest) {}

    func getFeeds(response _: LoadableSubject<SearchFeedsResponse>, request _: SearchRequest) {}

    func getBusinesses(response _: LoadableSubject<SearchBusinessesResponse>, request _: SearchRequest) {}

    func getSortingSuggestions(response _: LoadableSubject<[SortingSuggestionResponse]>) {}
}
