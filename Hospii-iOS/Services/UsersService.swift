//
//  UsersService.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import Combine
import CoreStore
import Foundation
import Moya
import SwiftUI

protocol UsersService {
    func postAvatar(response: LoadableSubject<Void>?, imageData: Data)

    func getProfile(response: LoadableSubject<UserResponse>)

    func putProfile(response: LoadableSubject<UserResponse>, request: UpdateProfileRequest)

    func getPhotos(response: LoadableSubject<SearchFeedsResponse>, request: PageRequest)
}

struct RealUsersService: UsersService {
    let webRepository: UsersWebDataSource
    let dbRepository: UsersDBDataSource
    let appState: Store<AppState>
    let globalCancelBag = CancelBag()

    init(webRepository: UsersWebDataSource, dbRepository: UsersDBDataSource, appState: Store<AppState>) {
        self.webRepository = webRepository
        self.appState = appState
        self.dbRepository = dbRepository
    }

    func postAvatar(response: LoadableSubject<Void>?, imageData: Data) {
        let cancelBag = CancelBag()
        response?.wrappedValue.setIsLoading(cancelBag: response == nil ? globalCancelBag : cancelBag)

        webRepository
            .postAvatar(imageData: imageData)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .flatMap { [dbRepository] media in
                dbRepository.saveProfilePhoto(media: media)
            }
            .sinkToLoadable {
                response?.wrappedValue = $0
            }
            .store(in: response == nil ? globalCancelBag : cancelBag)
    }

    func getProfile(response: LoadableSubject<UserResponse>) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        Just<Void>
            .withErrorType(ResponseError.self)
            .flatMap { _ -> AnyPublisher<Void, ResponseError> in
                self.refreshProfile()
            }
            .flatMap { [dbRepository] _ in
                dbRepository.user()
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    private func refreshProfile() -> AnyPublisher<Void, ResponseError> {
        return webRepository
            .getProfile()
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .flatMap { [dbRepository] in
                dbRepository.store(user: $0)
            }
            .eraseToAnyPublisher()
    }

    func putProfile(response: LoadableSubject<UserResponse>, request: UpdateProfileRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .putProfile(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .flatMap { [dbRepository] in
                dbRepository.store(user: $0)
            }
            .flatMap { [dbRepository] _ in
                dbRepository.user()
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func getPhotos(response: LoadableSubject<SearchFeedsResponse>, request: PageRequest) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .getPhotos(request: request)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .map {
                if $0.meta.currentPage == 1 {
                    return $0
                } else {
                    return SearchFeedsResponse(data: (response.wrappedValue.value?.data ?? []) + $0.data, meta: $0.meta)
                }
            }
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    private var requestHoldBackTimeInterval: TimeInterval {
        return ProcessInfo.processInfo.isRunningTests ? 0 : 0.5
    }
}

struct StubUsersService: UsersService {
    func postAvatar(response _: LoadableSubject<Void>?, imageData _: Data) {}

    func getProfile(response _: LoadableSubject<UserResponse>) {}

    func putProfile(response _: LoadableSubject<UserResponse>, request _: UpdateProfileRequest) {}

    func getPhotos(response _: LoadableSubject<SearchFeedsResponse>, request _: PageRequest) {}
}
