//
//  MediaService.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/1/21.
//

import Combine
import Foundation
import Moya
import SwiftUI

protocol MediaService {
    func postPhoto(cancelBag: CancelBag?, response: LoadableSubject<Response>, imageData: [Data], tags: [String]?, about: String?)

    func postView(response: LoadableSubject<Response>?, postId: Int)

    func deletePhotos(response: LoadableSubject<Response>, ids: [Int])
}

struct RealMediaService: MediaService {
    let webRepository: MediaWebDataSource
    let appState: Store<AppState>
    let globalCancelBag = CancelBag()

    init(webRepository: MediaWebDataSource, appState: Store<AppState>) {
        self.webRepository = webRepository
        self.appState = appState
    }

    func postPhoto(cancelBag: CancelBag? = nil, response: LoadableSubject<Response>, imageData: [Data], tags: [String]?, about: String?) {
        let cancelBag = cancelBag == nil ? CancelBag() : cancelBag
        response.wrappedValue.setIsLoading(cancelBag: cancelBag!)

        webRepository
            .postPhoto(imageData: imageData, tags: tags, about: about)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag!)
    }

    func postView(response: LoadableSubject<Response>?, postId: Int) {
        let cancelBag = CancelBag()
        response?.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .postView(postId: postId)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response?.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    func deletePhotos(response: LoadableSubject<Response>, ids: [Int]) {
        let cancelBag = CancelBag()
        response.wrappedValue.setIsLoading(cancelBag: cancelBag)

        webRepository
            .deletePhotos(ids: ids)
            .ensureTimeSpan(requestHoldBackTimeInterval)
            .sinkToLoadable {
                response.wrappedValue = $0
            }
            .store(in: cancelBag)
    }

    private var requestHoldBackTimeInterval: TimeInterval {
        return ProcessInfo.processInfo.isRunningTests ? 0 : 0.5
    }
}

struct StubMediaService: MediaService {
    func postPhoto(cancelBag _: CancelBag?, response _: LoadableSubject<Response>, imageData _: [Data], tags _: [String]?, about _: String?) {}

    func postView(response _: LoadableSubject<Response>?, postId _: Int) {}

    func deletePhotos(response _: LoadableSubject<Response>, ids _: [Int]) {}
}
