extension DIContainer {
    struct Services {
        let authService: AuthService
        let usersService: UsersService
        let businessesService: BusinessesService
        let userPermissionsService: UserPermissionsService
        let mediaService: MediaService
        let searchService: SearchService

        init(authService: AuthService,
             usersService: UsersService,
             businessesService: BusinessesService,
             userPermissionsService: UserPermissionsService,
             mediaService: MediaService,
             searchService: SearchService)
        {
            self.authService = authService
            self.usersService = usersService
            self.businessesService = businessesService
            self.userPermissionsService = userPermissionsService
            self.mediaService = mediaService
            self.searchService = searchService
        }

        static var stub: Self {
            .init(authService: StubCountriesService(),
                  usersService: StubUsersService(),
                  businessesService: StubBusinessesService(),
                  userPermissionsService: StubUserPermissionsService(),
                  mediaService: StubMediaService(),
                  searchService: StubSearchService())
        }
    }
}
