import Combine
import CoreStore
import SwiftUI

struct AppState: Equatable {
    var userData = UserData()
    var routing = ViewRouting()
    var system = System()
    var permissions = Permissions()
}

extension AppState {
    struct UserData: Equatable {
        var accessToken: String?
        var refreshToken: String?
        var expiresIn: String?
        var createdAt: String?

        var userId: String?

        var signupUsername: String?

        var searchRequest = SearchRequest()

        var feeds: Loadable<SearchFeedsResponse> = {
            let cache = CoreStoreDefaults.dataStack.publishList(From<PostMO>().orderBy(.descending(\.createdAt)).tweak { $0.fetchLimit = 12 })
                .snapshot
                .compactMap { $0.object }
                .map { PostResponse.fromCoreData(post: $0) }

            return cache.isEmpty ? .notRequested : .cache(SearchFeedsResponse(data: cache, meta: PaginationMeta(currentPage: 1, itemCount: cache.count)))
        }()

        var services: Loadable<[ServiceResponse]> = {
            let cache = CoreStoreDefaults.dataStack.publishList(From<ServiceMO>().orderBy(.ascending(\.key)))
                .snapshot
                .compactMap { $0.object }
                .map { ServiceResponse.fromCoreData(service: $0) }

            return cache.isEmpty ? .notRequested : .cache(cache)
        }()

        var sortingSuggestions: Loadable<[SortingSuggestionResponse]> = {
            let cache = CoreStoreDefaults.dataStack.publishList(From<SortingSuggestionMO>().orderBy(.ascending(\.insertOrder)))
                .snapshot
                .compactMap { $0.object }
                .map { SortingSuggestionResponse.fromCoreData(sortingSuggestion: $0) }

            return cache.isEmpty ? .notRequested : .cache(cache)
        }()
    }
}

extension AppState {
    struct ViewRouting: Equatable {
        var contentView = ContentView.Routing()
        var mainView = MainView.Routing()
    }
}

extension AppState {
    struct System: Equatable {
        var isActive: Bool = false
        var keyboardHeight: CGFloat = 0
        let photoFileSizeLimit: Float = 1000.0
    }
}

extension AppState {
    struct Permissions: Equatable {
        var push: Permission.Status = .unknown
    }

    static func permissionKeyPath(for permission: Permission) -> WritableKeyPath<AppState, Permission.Status> {
        let pathToPermissions = \AppState.permissions
        switch permission {
        case .pushNotifications:
            return pathToPermissions.appending(path: \.push)
        }
    }
}

func == (lhs: AppState, rhs: AppState) -> Bool {
    return lhs.userData == rhs.userData &&
        lhs.routing == rhs.routing &&
        lhs.system == rhs.system &&
        lhs.permissions == rhs.permissions
}

extension AppState {
    static var preview: AppState {
        var state = AppState()
        state.system.isActive = true
        return state
    }
}
