//
//  Oauth2MoyaProvider.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/22/21.
//

import Combine
import CombineMoya
import Foundation
import Moya
import Valet

class Oauth2MoyaProvider<Target> where Target: Moya.TargetType {
    private let provider: MoyaProvider<Target>
    private let appState: Store<AppState>?
    init(endpointClosure: @escaping MoyaProvider<Target>.EndpointClosure = MoyaProvider.defaultEndpointMapping,
         requestClosure _: @escaping MoyaProvider<Target>.RequestClosure = MoyaProvider<Target>.defaultRequestMapping,
         stubClosure: @escaping MoyaProvider<Target>.StubClosure = MoyaProvider.neverStub,
         session: Session = MoyaProvider<Target>.defaultAlamofireSession(),
         plugins: [PluginType] = [],
         trackInflights: Bool = false,
         appState: Store<AppState>? = nil)
    {
        provider = MoyaProvider(endpointClosure: endpointClosure,
                                requestClosure: MoyaAuthenticatable<AuthWebRepository.API, Oauth2Authentication>(appState: appState).requestClosure,
                                stubClosure: stubClosure,
                                session: session,
                                plugins: plugins,
                                trackInflights: trackInflights)
        self.appState = appState
    }

    func requestPublisher(_ token: Target) -> AnyPublisher<Moya.Response, MoyaError> {
        let request = provider.requestPublisher(token)
            .mapError { [weak self] error -> MoyaError in
                if error.response?.statusCode == 401, error.response?.request?.headers["Authorization"]?.starts(with: "Bearer") == true {
                    self?.clearUserData()
                }
                return error
            }
            .eraseToAnyPublisher()

        return request
    }

    private func clearUserData() {
        LogoutUtils.logout(appState: appState)
    }
}
