import Combine
import Foundation
import Moya

// MARK: - Protocol

protocol RefreshableAuthentication {
    var isRefreshNeeded: Bool { get }
    var accessToken: String { get }
    func refresh(appState: Store<AppState>?) -> AnyPublisher<Self, MoyaError>

    static func find(appState: Store<AppState>?) -> Self?
}

// MARK: - Error

enum MoyaAuthenticatableError: Error {
    case requestError
}

// MARK: - MoyaAuthenticatable

class MoyaAuthticatableUtils {
    static let shared = MoyaAuthticatableUtils()
    let requestQueue = DispatchQueue(label: "com.hospii.hospii-iOS.requestQueue")
}

/// Provides requestClosure which refresh accessToken automatically if needed.
struct MoyaAuthenticatable<API: TargetType, Authenticatable: RefreshableAuthentication>: PluginType {
    public typealias OnComplete = MoyaProvider<API>.RequestResultClosure

    private let cancelBag = CancelBag()
    private let appState: Store<AppState>?

    init(appState: Store<AppState>? = nil) {
        self.appState = appState
    }

    /// whether resume requests if refreshing token failed.
    public var resumeOnError: Bool = true

    /// Before sending request, execute **finding persisted authentication**, **refreshing found authentication if needed**, **resume other requests** in a serial queue.
    public func requestClosure(endpoint: Endpoint, done: @escaping OnComplete) {
        MoyaAuthticatableUtils.shared.requestQueue.async {
            self.suspend()
            self.authorizeRequestInSerialQueue(endPoint: endpoint, done: done)
        }
    }
}

private extension MoyaAuthenticatable {
    /// find persisted authentication, and if found authentication needs refreshing, execute refreshing, then resume queued other requests.
    func authorizeRequestInSerialQueue(endPoint: Endpoint, done: @escaping OnComplete) {
        guard let request = try? endPoint.urlRequest() else {
            done(.failure(MoyaError.underlying(MoyaAuthenticatableError.requestError, nil)))
            resume()
            return
        }

        if let authentication = Authenticatable.find(appState: appState) {
            if authentication.isRefreshNeeded {
                authentication
                    .refresh(appState: appState)
                    .sink(receiveCompletion: { result in
                        if let error = result.error {
                            done(.failure(MoyaError.underlying(error, nil)))
                            if self.resumeOnError {
                                self.resume()
                            }

                            if error.response?.statusCode == 401 {
                                LogoutUtils.logout(appState: appState)
                            }
                        }
                    }, receiveValue: { _ in
                        done(.success(self.makeAuthorizedRequest(from: request)))
                        self.resume()
                    })
                    .store(in: cancelBag)
            } else {
                done(.success(makeAuthorizedRequest(from: request)))
                resume()
            }
        } else {
            done(.success(makeAuthorizedRequest(from: request)))
            resume()
        }
    }

    func makeAuthorizedRequest(from originalRequest: URLRequest) -> URLRequest {
        var request = originalRequest
        if let authentication = Authenticatable.find(appState: appState) {
            request.setValue(
                "Bearer \(authentication.accessToken)",
                forHTTPHeaderField: "Authorization"
            )
        }
        return request
    }

    func suspend() {
        MoyaAuthticatableUtils.shared.requestQueue.suspend()
    }

    func resume() {
        MoyaAuthticatableUtils.shared.requestQueue.resume()
    }
}
