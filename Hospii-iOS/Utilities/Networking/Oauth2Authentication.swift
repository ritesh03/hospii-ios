//
//  Oauth2Authentication.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/22/21.
//

import Combine
import Foundation
import Moya
import SwiftDate
import Valet

struct Oauth2Authentication: RefreshableAuthentication, Decodable {
    let accessToken: String
    var refreshToken: String!
    var createdAt = Date()
    let expiresIn: Int

    var isRefreshNeeded: Bool {
        let threshold = TimeInterval(expiresIn / 2)
        let result = expiresAt < Date().addingTimeInterval(threshold)
        return result
    }

    static func find(appState: Store<AppState>?) -> Oauth2Authentication? {
        return Oauth2AuthenticationStore.find(appState: appState)
    }

    func refresh(appState: Store<AppState>?) -> AnyPublisher<Oauth2Authentication, MoyaError> {
        let refreshToken = self.refreshToken

        return AuthWebRepository.API
            .sharedProvider
            .requestPublisher(.postRefreshToken(refreshToken: refreshToken ?? ""))
            .map(Oauth2Authentication.self)
            .handleEvents(receiveOutput: { authentication in
                Oauth2AuthenticationStore.save(authentication: authentication, appState: appState)
            })
            .eraseToAnyPublisher()
    }
}

extension Oauth2Authentication {
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case expiresIn = "expires_in"
    }

    var expiresAt: Date {
        return createdAt.addingTimeInterval(TimeInterval(expiresIn))
    }
}

// MARK: - Private Persistent Layer

private enum Oauth2AuthenticationStore {
    static func save(authentication: Oauth2Authentication, appState: Store<AppState>?) {
        let valet = Valet.valet(with: Identifier(nonEmpty: KeyChainConstants.SERVICE)!, accessibility: .whenUnlocked)
        try? valet.setString(authentication.accessToken, forKey: KeyChainConstants.ACCESS_TOKEN)
        try? valet.setString(authentication.refreshToken, forKey: KeyChainConstants.REFRESH_TOKEN)
        try? valet.setString(authentication.expiresIn.description, forKey: KeyChainConstants.EXPIRES_IN)
        try? valet.setString(authentication.createdAt.timeIntervalSince1970.description, forKey: KeyChainConstants.CREATED_AT)

        appState?[\.userData.accessToken] = authentication.accessToken
        appState?[\.userData.refreshToken] = authentication.refreshToken
        appState?[\.userData.expiresIn] = authentication.expiresIn.description
        appState?[\.userData.createdAt] = authentication.createdAt.timeIntervalSince1970.description
    }

    static func find(appState: Store<AppState>?) -> Oauth2Authentication? {
        //        let valet = Valet.valet(with: Identifier(nonEmpty: KeyChainConstants.SERVICE)!, accessibility: .whenUnlocked)
        //
        //        let accessToken = try? valet.string(forKey: KeyChainConstants.ACCESS_TOKEN)
        //
        //        let refreshToken = try? valet.string(forKey: KeyChainConstants.REFRESH_TOKEN)
        //        let createdAt = (try? valet.string(forKey: KeyChainConstants.CREATED_AT)).flatMap { description in
        //            Double(description).flatMap { seconds in
        //                Date(timeIntervalSince1970: seconds)
        //            }
        //        }
        //        let expiresIn = (try? valet.string(forKey: KeyChainConstants.EXPIRES_IN)).flatMap { description in
        //            Int(description)
        //        }
        //
        //        if let accessToken = accessToken,
        //           let refreshToken = refreshToken,
        //           let createdAt = createdAt,
        //           let expiresIn = expiresIn
        //        {
        //            return Oauth2Authentication(
        //                accessToken: accessToken,
        //                refreshToken: refreshToken,
        //                createdAt: createdAt,
        //                expiresIn: expiresIn
        //            )
        //        }
        //        return nil

        let accessToken = appState?[\.userData.accessToken]

        let refreshToken = appState?[\.userData.refreshToken]
        let createdAt = appState?[\.userData.createdAt].flatMap { description in
            Double(description).flatMap { seconds in
                Date(timeIntervalSince1970: seconds)
            }
        }
        let expiresIn = appState?[\.userData.expiresIn].flatMap { description in
            Int(description)
        }

        if let accessToken = accessToken,
           let refreshToken = refreshToken,
           let createdAt = createdAt,
           let expiresIn = expiresIn
        {
            return Oauth2Authentication(
                accessToken: accessToken,
                refreshToken: refreshToken,
                createdAt: createdAt,
                expiresIn: expiresIn
            )
        }
        return nil
    }
}
