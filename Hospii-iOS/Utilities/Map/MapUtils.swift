//
//  MapUtils.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/22/21.
//

import Foundation
import SwiftUI

class MapUtils {
    static let shared = MapUtils()

    func openGoogleMaps(lat: Double?, lng: Double?) {
        guard let lat = lat, let lng = lng else {
            return
        }
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
            if let url = URL(string: "comgooglemaps-x-callback://?daddr=\(lat),\(lng)") {
                UIApplication.shared.open(url, options: [:])
            }
        } else {
            if let urlDestination = URL(string: "https://www.google.co.in/maps/dir/?daddr=\(lat),\(lng)") {
                UIApplication.shared.open(urlDestination)
            }
        }
    }

    func openAppleMaps(lat: Double?, lng: Double?) {
        guard let lat = lat, let lng = lng else {
            return
        }
        let directionsURL = "http://maps.apple.com/?daddr=\(lat),\(lng)"
        guard let url = URL(string: directionsURL) else {
            return
        }
        UIApplication.shared.open(url)
    }
}
