//
// Copyright © Hospii Inc. All rights reserved.
//

import Foundation

enum UsernameValidationError: Error {
    case empty
    case tooShort
    case tooLong
    case oneLetter
    case noSpecialCharacter
    case noPeriodStart
    case noPeriodEnd
    case noTwoPeriodRow
}

enum NameValidationError: Error {
    case empty
    case tooLong
    case invalidCharacter
    case noSpecialCharacter
}

enum EmailValidationError: Error {
    case empty
    case tooShort
    case tooLong
    case invalidFormat
}

enum PhoneNumberValidationError: Error {
    case empty
    case invalidFormat
}

enum PasswordValidationError: Error {
    case empty
    case tooShort
    case tooLong
    case invalidCharacter
    case noLowerCase
    case noUpperCase
    case noNumber
}

enum OneTimeCodeValidationError: Error {
    case empty
    case tooShort
}

enum BusinessNameValidationError: Error {
    case empty
}

enum BusinessAddressValidationError: Error {
    case empty
}

enum ValidationType {
    case firstName
    case lastName
    case email
    case phoneNumber
    case password
    case username
    case oneTimeCode
    case businessName
    case businessAddress
    case businessPhoneNumber
}

struct NameValidation {
    func validate(_ input: String) -> NameValidationError? {
        guard input.count > 0 else {
            return .empty
        }

        guard input.count <= 100 else {
            return .tooLong
        }

        let namePredicate = NSPredicate(format: "SELF MATCHES %@", "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$")
        if namePredicate.evaluate(with: input) {
            return nil
        } else {
            let noSpecialCharactersFormat = "[^,;,@,#,!,$,%,&,*,(,),:,>,<,/,\\,|,+]+"
            let noSpecialCharactersPredicate = NSPredicate(format: "SELF MATCHES %@", noSpecialCharactersFormat)
            if noSpecialCharactersPredicate.evaluate(with: input) {
                return .invalidCharacter
            }
            return .noSpecialCharacter
        }
    }
}

struct EmailValidation {
    func validate(_ input: String) -> EmailValidationError? {
        guard input.count > 0 else {
            return .empty
        }
        // The length of the email address can't exceed 100
        // Without the length check, someone malicious
        // could enter a long email that takes up storage space
        // or even overflows
        guard input.count >= 2 else {
            return .tooShort
        }

        guard input.count <= 250 else {
            return .tooLong
        }

        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,100}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailFormat)
        guard emailPredicate.evaluate(with: input) else {
            return .invalidFormat
        }

        return nil
    }
}

struct PhoneNumberValidation {
    func validate(_ input: String) -> PhoneNumberValidationError? {
        guard input.count > 0 else {
            return .empty
        }
        // US 10 digit only for now
        let phoneNumberFormat = "^\\(\\d{3}\\) \\d{3} - \\d{4}$"
        let phoneNumberPredicate = NSPredicate(format: "SELF MATCHES %@", phoneNumberFormat)
        guard phoneNumberPredicate.evaluate(with: input) else {
            return .invalidFormat
        }

        return nil
    }
}

struct PasswordValidation {
    func validate(_ input: String) -> PasswordValidationError? {
        guard input.count > 0 else {
            return .empty
        }

        // Minimum 8 characters at least 1 uppercase alphabet, 1 lowercase alphabet, 1 number
        // Enforce a limit of 30 characters
        guard input.count >= 8 else {
            return .tooShort
        }

        guard input.count <= 30 else {
            return .tooLong
        }

        let passwordFormat = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,30}$"
        let passwordPredicate = NSPredicate(format: "SELF MATCHES %@", passwordFormat)
        if passwordPredicate.evaluate(with: input) {
            return nil
        } else {
            var hasUpperCaseLetter = false
            var hasLowerCaseLetter = false
            var hasNumber = false

            // Uppercase letter check
            let upperCaseLetterFormat = ".*[A-Z]+.*"
            let upperCaseLetterPredicate = NSPredicate(format: "SELF MATCHES %@", upperCaseLetterFormat)
            if upperCaseLetterPredicate.evaluate(with: input) {
                hasUpperCaseLetter = true
            }

            // Lowercase letter check
            let lowerCaseLetterFormat = ".*[a-z]+.*"
            let lowerCaseLetterPredicate = NSPredicate(format: "SELF MATCHES %@", lowerCaseLetterFormat)
            if lowerCaseLetterPredicate.evaluate(with: input) {
                hasLowerCaseLetter = true
            }

            // Number check
            let numberFormat = ".*[0-9]+.*"
            let numberPredicate = NSPredicate(format: "SELF MATCHES %@", numberFormat)
            if numberPredicate.evaluate(with: input) {
                hasNumber = true
            }

            if hasLowerCaseLetter == false {
                return .noLowerCase
            }

            if hasUpperCaseLetter == false {
                return .noUpperCase
            }

            if hasNumber == false {
                return .noNumber
            }

            return .invalidCharacter
        }
    }
}

struct UsernameValidation {
    func validate(_ input: String) -> UsernameValidationError? {
        guard input.count > 0 else {
            return .empty
        }

        guard input.count >= 3 else {
            return .tooShort
        }

        guard input.count <= 30 else {
            return .tooLong
        }

        let periodStartFormat = "^(?!\\.).+"
        let periodStartPredicate = NSPredicate(format: "SELF MATCHES %@", periodStartFormat)
        guard periodStartPredicate.evaluate(with: input) else {
            return .noPeriodStart
        }

        let periodEndFormat = ".*(?<!\\.)$"
        let periodEndPredicate = NSPredicate(format: "SELF MATCHES %@", periodEndFormat)
        guard periodEndPredicate.evaluate(with: input) else {
            return .noPeriodEnd
        }

        let twoPeriodsInARowFormat = "^((?!\\.\\.).)*$"
        let twoPeriodsInARowPredicate = NSPredicate(format: "SELF MATCHES %@", twoPeriodsInARowFormat)
        guard twoPeriodsInARowPredicate.evaluate(with: input) else {
            return .noTwoPeriodRow
        }

        let usernameOneLetterFormat = ".*[a-zA-Z]+.*"
        let usernameOneLetterPredicate = NSPredicate(format: "SELF MATCHES %@", usernameOneLetterFormat)
        guard usernameOneLetterPredicate.evaluate(with: input) else {
            return .oneLetter
        }

        let usernameFormat = "([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\\.(?!\\.))){0,28}(?:[A-Za-z0-9_]))?)"
        let usernamePredicate = NSPredicate(format: "SELF MATCHES %@", usernameFormat)
        guard usernamePredicate.evaluate(with: input) else {
            return .noSpecialCharacter
        }

        return nil
    }
}

struct OneTimeCodeValidation {
    func validate(_ input: String) -> OneTimeCodeValidationError? {
        guard input.count > 0 else {
            return .empty
        }

        guard input.count >= 6 else {
            return .tooShort
        }

        return nil
    }
}

struct BusinessNameValidation {
    func validate(_ input: String) -> BusinessNameValidationError? {
        guard input.count > 0 else {
            return .empty
        }
        return nil
    }
}

struct BusinessAddressValidation {
    func validate(_ input: String) -> BusinessAddressValidationError? {
        guard input.count > 0 else {
            return .empty
        }
        return nil
    }
}
