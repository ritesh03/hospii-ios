//
//  ParallaxHeader.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/26/21.
//

import SwiftUI

struct ParallaxHeader {
    private static let collapsedImageHeight: CGFloat = 75

    static func getScrollOffset(_ geometry: GeometryProxy) -> CGFloat {
        geometry.frame(in: .global).minY
    }

    static func getOffsetForHeaderImage(_ geometry: GeometryProxy) -> CGFloat {
        let offset = getScrollOffset(geometry)
        let imageHeight: CGFloat = UIScreen.main.bounds.width * 230 / 375
        let sizeOffScreen = imageHeight - collapsedImageHeight

        // if our offset is roughly less than -225 (the amount scrolled / amount off screen)
        if offset < -sizeOffScreen {
            // Since we want 75 px fixed on the screen we get our offset of -225 or anything less than. Take the abs value of
            let imageOffset = abs(min(-sizeOffScreen, offset))

            // Now we can the amount of offset above our size off screen. So if we've scrolled -250px our size offscreen is -225px we offset our image by an additional 25 px to put it back at the amount needed to remain offscreen/amount on screen.
            return imageOffset - sizeOffScreen
        }

        // Image was pulled down
        if offset > 0 {
            return -offset
        }

        return 0
    }

    static func getHeightForHeaderImage(_ geometry: GeometryProxy) -> CGFloat {
        let offset = getScrollOffset(geometry)
        let imageHeight = geometry.size.height

        if offset > 0 {
            return imageHeight + offset
        }

        return imageHeight
    }
}
