//
//  Processors.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/5/21.
//

import Foundation
import Kingfisher
import UIKit

class ImageProcessors {
    static let avatarProcessor = DownsamplingImageProcessor(size: CGSize(width: 512, height: 512))

    static let thumbnailProcessor = DownsamplingImageProcessor(size: CGSize(width: 512, height: 512))

    static let gridProcessor = DownsamplingImageProcessor(size: CGSize(width: 256, height: 256))
}
