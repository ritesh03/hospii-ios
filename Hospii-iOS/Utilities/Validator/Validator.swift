//
//  Validator.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 1/5/22.
//

import Foundation
import PhoneNumberKit

class Validator {
    static let shared = Validator()

    private lazy var phoneNumberKit = PhoneNumberKit()

    func validate(password: String?) -> String {
        if let passwordValidation = password?.validateText(type: .password) as? PasswordValidationError {
            let errorMessage: String
            switch passwordValidation {
            case .empty:
                errorMessage = R.string.localizable.passwordValidationErrorEmpty()
            case .invalidCharacter:
                errorMessage = R.string.localizable.passwordValidationErrorInvalidCharacter()
            case .noNumber:
                errorMessage = R.string.localizable.passwordValidationErrorNoNumber()
            case .noLowerCase:
                errorMessage = R.string.localizable.passwordValidationErrorNoLowerCase()
            case .noUpperCase:
                errorMessage = R.string.localizable.passwordValidationErrorNoUpperCase()
            case .tooLong:
                errorMessage = R.string.localizable.passwordValidationErrorTooLong()
            case .tooShort:
                errorMessage = R.string.localizable.passwordValidationErrorTooShort()
            }
            return errorMessage
        } else {
            return ""
        }
    }

    func validate(email: String?) -> String {
        if let emailValidation = email?.validateText(type: .email) as? EmailValidationError {
            let errorMessage: String
            switch emailValidation {
            case .empty:
                errorMessage = R.string.localizable.emailValidationErrorEmpty()
            case .tooShort:
                errorMessage = R.string.localizable.emailValidationErrorTooShort()
            case .tooLong:
                errorMessage = R.string.localizable.emailValidationErrorTooLong()
            case .invalidFormat:
                errorMessage = R.string.localizable.emailValidationErrorInvalidFormat()
            }
            return errorMessage
        } else {
            return ""
        }
    }

    func validate(phoneNumber: String?) -> String {
        let errorMessage: String

        if let phoneNumber = phoneNumber, !phoneNumber.isEmpty {
            if !phoneNumberKit.isValidPhoneNumber(phoneNumber) {
                errorMessage = R.string.localizable.phoneNumberValidationErrorInvalidFormat()
            } else {
                errorMessage = ""
            }
        } else {
            errorMessage = R.string.localizable.phoneNumberValidationErrorEmpty()
        }

        return errorMessage
    }
}
