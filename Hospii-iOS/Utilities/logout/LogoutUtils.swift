//
//  LogoutUtils.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/31/21.
//

import Alamofire
import CoreStore
import Foundation
import Valet

class LogoutUtils {
    static func logout(appState: Store<AppState>?) {
        let valet = Valet.valet(with: Identifier(nonEmpty: KeyChainConstants.SERVICE)!, accessibility: .whenUnlocked)
        try? valet.removeAllObjects()

        appState?[\.userData].accessToken = nil
        appState?[\.userData].refreshToken = nil
        appState?[\.userData].expiresIn = nil
        appState?[\.userData].createdAt = nil
        appState?[\.userData].userId = nil
        appState?[\.userData].signupUsername = nil
        appState?[\.routing].contentView.accessToken = nil

        Alamofire.Session.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }

        // Delete
        CoreStoreDefaults.dataStack.perform(
            asynchronous: { transaction in
                try transaction.deleteAll(From<UserMO>())
                try transaction.deleteAll(From<MediaMO>())
                try transaction.deleteAll(From<BusinessMO>())
                try transaction.deleteAll(From<ServiceMO>())
            }, completion: { _ in
            }
        )
    }
}
