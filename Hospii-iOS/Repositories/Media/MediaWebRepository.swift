//
//  MediaWebRepository.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/1/21.
//

import Combine
import Foundation
import Moya

protocol MediaWebDataSource: WebDataSource {
    func postPhoto(imageData: [Data], tags: [String]?, about: String?) -> AnyPublisher<Response, ResponseError>

    func postView(postId: Int) -> AnyPublisher<Response, ResponseError>

    func deletePhotos(ids: [Int]) -> AnyPublisher<Response, ResponseError>
}

struct MediaWebRepository: MediaWebDataSource {
    let provider: MoyaProvider<API>

    init(appState: Store<AppState>) {
        provider = MoyaProvider<API>(endpointClosure: { (target: API) -> Endpoint in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer \(appState[\.userData.accessToken] ?? "")"])
        }, plugins: [])
    }

    func postPhoto(imageData: [Data], tags: [String]?, about: String?) -> AnyPublisher<Response, ResponseError> {
        provider.requestPublisher(.postPhoto(imageData: imageData, tags: tags, about: about))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func postView(postId: Int) -> AnyPublisher<Response, ResponseError> {
        provider.requestPublisher(.postView(mediaId: postId))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func deletePhotos(ids: [Int]) -> AnyPublisher<Response, ResponseError> {
        provider.requestPublisher(.deletePhotos(ids: ids))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }
}

extension MediaWebRepository {
    enum API {
        case postPhoto(imageData: [Data], tags: [String]?, about: String?)
        case deletePhotos(ids: [Int])
        case postView(mediaId: Int)
    }
}

extension MediaWebRepository.API: TargetType {
    var baseURL: URL {
        URL(string: ApiConstants.BASE_URL)!
    }

    var path: String {
        switch self {
        case .postPhoto:
            return "/media/photo"
        case .deletePhotos:
            return "/media/photos"
        case .postView:
            return "/views/media"
        }
    }

    var method: Moya.Method {
        switch self {
        case .postPhoto, .postView:
            return .post
        case .deletePhotos:
            return .delete
        }
    }

    var task: Task {
        switch self {
        case let .postPhoto(imageData, tags, about):
            var multipartFormData = [MultipartFormData]()
            for (index, image) in imageData.enumerated() {
                multipartFormData.append(MultipartFormData(provider: .data(image), name: "image_\(index)", fileName: "upload_photo_\(index).jpeg", mimeType: "image/jpeg"))
            }

            if let tags = tags {
                for tag in tags {
                    let tagData = String(tag).data(using: String.Encoding.utf8)!
                    multipartFormData.append(MultipartFormData(provider: .data(tagData), name: "tags"))
                }
            }

            if let about = about {
                let aboutData = about.data(using: String.Encoding.utf8)!
                multipartFormData.append(MultipartFormData(provider: .data(aboutData), name: "about"))
            }
            return .uploadMultipart(multipartFormData)
        case let .postView(mediaId):
            return .requestParameters(parameters: ["postId": mediaId], encoding: JSONEncoding.default)
        case let .deletePhotos(ids):
            return .requestParameters(parameters: ["ids": ids], encoding: JSONEncoding.default)
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return ["Accept": "application/json"]
    }

    var validationType: Moya.ValidationType {
        return .successAndRedirectCodes
    }
}
