//
//  FirebaseRepository.swift
//  Proguides
//
//  Created by Eric Cheng on 7/21/21.
//  Copyright © 2021 glggaming. All rights reserved.
//

import Firebase
import Foundation

protocol FirebaseWebDataSource: WebDataSource {
    func fetchFirebaseToken(completion: ((String) -> Void)?)
}

class FirebaseWebRepository: FirebaseWebDataSource {
    func fetchFirebaseToken(completion: ((String) -> Void)?) {
        Messaging.messaging().token { token, error in
            // Only send the firebase token to the server when the user is logged in.
            if let error = error {
                debugPrint("Error fetching FCM registration token: \(error)")
            } else if let token = token {
                debugPrint("FCM registration token: \(token)")
                completion?(token)
            }
        }
    }
}
