//
//  SearchDBRepository.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/12/21.
//

import Combine
import CoreData
import CoreStore
import Moya

protocol SearchDBDataSource {
    func storeFeeds(feeds: SearchFeedsResponse) -> AnyPublisher<SearchFeedsResponse, ResponseError>
    func feeds() -> AnyPublisher<[PostResponse], Never>

    func storeSortingSuggestions(sugggestions: [SortingSuggestionResponse]) -> AnyPublisher<[SortingSuggestionResponse], ResponseError>
}

struct SearchDBRepository: SearchDBDataSource {
    func storeSortingSuggestions(sugggestions: [SortingSuggestionResponse]) -> AnyPublisher<[SortingSuggestionResponse], ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction in
                var order = 0
                let sugggestions = sugggestions.map { suggestion -> SortingSuggestionResponse in
                    var suggestion = suggestion
                    suggestion.insertOrder = order
                    order = order + 1
                    return suggestion
                }
                _ = try? transaction.importUniqueObjects(Into<SortingSuggestionMO>(), sourceArray: sugggestions)

                try transaction.deleteAll(From<SortingSuggestionMO>().where(format: "NOT suggestion IN %@", sugggestions.map { $0.suggestion }))
            }
            .mapError { ResponseError.dbError(error: $0) }
            .map { _ in
                sugggestions
            }
            .eraseToAnyPublisher()
    }

    func storeFeeds(feeds: SearchFeedsResponse) -> AnyPublisher<SearchFeedsResponse, ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction in
                if feeds.meta.currentPage == 1 {
                    _ = try? transaction.deleteAll(From<PostMO>())
                }
                _ = try? transaction.importUniqueObjects(Into<PostMO>(), sourceArray: feeds.data)
            }
            .mapError { ResponseError.dbError(error: $0) }
            .map { _ in
                feeds
            }
            .eraseToAnyPublisher()
    }

    func feeds() -> AnyPublisher<[PostResponse], Never> {
        return CoreStoreDefaults.dataStack.publishList(
            From<PostMO>()
                .orderBy(.ascending(\.key)))
            .reactive
            .snapshot(emitInitialValue: true)
            .compactMap {
                $0
                    .compactMap { $0.object }
                    .compactMap {
                        PostResponse.fromCoreData(post: $0)
                    }
            }
            .eraseToAnyPublisher()
    }
}
