//
//  SearchWebRepository.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 12/11/21.
//

import Combine
import CoreMedia
import Foundation
import Moya

protocol SearchWebDataSource: WebDataSource {
    func getSuggestions(request: SearchSuggestionsRequest) -> AnyPublisher<SuggestionsResponse, ResponseError>

    func getTags(request: SearchSuggestionsRequest) -> AnyPublisher<SuggestionsResponse, ResponseError>

    func getFeeds(request: SearchRequest) -> AnyPublisher<SearchFeedsResponse, ResponseError>

    func getBusinesses(request: SearchRequest) -> AnyPublisher<SearchBusinessesResponse, ResponseError>

    func getSortingSuggestions() -> AnyPublisher<[SortingSuggestionResponse], ResponseError>
}

struct SearchWebRepository: SearchWebDataSource {
    let provider: Oauth2MoyaProvider<API>

    init(appState: Store<AppState>) {
        print((appState[\.userData.accessToken] ?? ""))
        provider = Oauth2MoyaProvider<API>(endpointClosure: { (target: API) -> Endpoint in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer \(appState[\.userData.accessToken] ?? "")"])
        }, plugins: [], appState: appState)
    }

    func getSuggestions(request: SearchSuggestionsRequest) -> AnyPublisher<SuggestionsResponse, ResponseError> {
        return provider.requestPublisher(.getSuggestions(request: request))
            .map(SuggestionsResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getTags(request: SearchSuggestionsRequest) -> AnyPublisher<SuggestionsResponse, ResponseError> {
        return provider.requestPublisher(.getTags(request: request))
            .map(SuggestionsResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getFeeds(request: SearchRequest) -> AnyPublisher<SearchFeedsResponse, ResponseError> {
        return provider.requestPublisher(.getFeeds(request: request))
            .map(SearchFeedsResponse.self)
            .mapError {
                return ResponseError.networkError(error: $0)
            }
            .eraseToAnyPublisher()
    }

    func getBusinesses(request: SearchRequest) -> AnyPublisher<SearchBusinessesResponse, ResponseError> {
        return provider.requestPublisher(.getBusinesses(request: request))
            .map(SearchBusinessesResponse.self)
            .mapError {
                return ResponseError.networkError(error: $0)
            }
            .eraseToAnyPublisher()
    }

    func getSortingSuggestions() -> AnyPublisher<[SortingSuggestionResponse], ResponseError> {
        return provider.requestPublisher(.getSortingSuggestions)
            .map([SortingSuggestionResponse].self)
            .mapError {
                return ResponseError.networkError(error: $0)
            }
            .eraseToAnyPublisher()
    }
}

extension SearchWebRepository {
    enum API {
        case getSuggestions(request: SearchSuggestionsRequest)
        case getTags(request: SearchSuggestionsRequest)
        case getFeeds(request: SearchRequest)
        case getBusinesses(request: SearchRequest)
        case getSortingSuggestions
    }
}

extension SearchWebRepository.API: TargetType {
    var baseURL: URL {
        URL(string: ApiConstants.BASE_URL)!
    }

    var path: String {
        switch self {
        case .getSuggestions:
            return "/search/suggestions"
        case .getTags:
            return "/search/tags"
        case .getFeeds:
            return "/search/feeds"
        case .getBusinesses:
            return "/search/businesses"
        case .getSortingSuggestions:
            return "/search/sorting-suggestions"
        }
    }

    var method: Moya.Method {
        switch self {
        case .getSuggestions, .getTags, .getFeeds, .getBusinesses, .getSortingSuggestions:
            return .get
        }
    }

    var task: Task {
        switch self {
        case let .getSuggestions(request):
            var parameters: [String: Any] = [:]
            parameters["keyword"] = request.keyword
            if let latitude = request.latitude, let longitude = request.longitude {
                parameters["latitude"] = latitude
                parameters["longitude"] = longitude
            }
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case let .getTags(request):
            return .requestParameters(parameters: ["keyword": request.keyword], encoding: URLEncoding.default)
        case let .getFeeds(request):
            var parameters: [String: Any] = [:]
            parameters["page"] = request.page
            parameters["pageSize"] = 18
            if !request.tags.isEmpty {
                parameters["tags"] = request.tags.joined(separator: ",")
            }
            if !request.services.isEmpty {
                parameters["services"] = request.services.joined(separator: ",")
            }
            if let latitude = request.latitude, let longitude = request.longitude {
                parameters["latitude"] = latitude
                parameters["longitude"] = longitude
            }
            if let sort = request.sort {
                parameters["sort"] = sort
            }
            parameters["range"] = request.range
            print(parameters)
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case let .getBusinesses(request):
            var parameters: [String: Any] = [:]
            parameters["page"] = request.page
            if !request.tags.isEmpty {
                parameters["tags"] = request.tags.joined(separator: ",")
            }
            if !request.services.isEmpty {
                parameters["services"] = request.services.joined(separator: ",")
            }
            if let latitude = request.latitude, let longitude = request.longitude {
                parameters["latitude"] = latitude
                parameters["longitude"] = longitude
            }
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case .getSortingSuggestions:
            return .requestPlain
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return ["Accept": "application/json"]
    }

    var validationType: Moya.ValidationType {
        return .successAndRedirectCodes
    }
}
