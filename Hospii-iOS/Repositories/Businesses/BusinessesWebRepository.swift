//
//  BusinessesWebRepository.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import Combine
import Foundation
import Moya

protocol BusinessesWebDataSource: WebDataSource {
    func getBusinessById(id: Int) -> AnyPublisher<UserBusinessResponse, ResponseError>

    func getBusinessPhotos(getPostsRequest: GetPostsRequest) -> AnyPublisher<SearchFeedsResponse, ResponseError>

    func postCreateBusiness(request: CreateBusinessRequest) -> AnyPublisher<CreateBusinessResponse, ResponseError>

    func putUpdateBusiness(request: CreateBusinessRequest) -> AnyPublisher<CreateBusinessResponse, ResponseError>

    func postCreateBusinessHours(request: CreateBusinessHoursRequest) -> AnyPublisher<Response, ResponseError>

    func postCover(imageData: Data, businessId: Int) -> AnyPublisher<Response, ResponseError>

    func getServices() -> AnyPublisher<[ServiceResponse], ResponseError>

    func getActiveServices() -> AnyPublisher<[ServiceResponse], ResponseError>

    func upateActiveServices(request: UpdateActiveServicesRequest) -> AnyPublisher<Response, ResponseError>
}

struct BusinessesWebRepository: BusinessesWebDataSource {
    let provider: Oauth2MoyaProvider<API>

    init(appState: Store<AppState>) {
        provider = Oauth2MoyaProvider<API>(endpointClosure: { (target: API) -> Endpoint in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer \(appState[\.userData.accessToken] ?? "")"])
        }, plugins: [], appState: appState)
    }

    func postCreateBusiness(request: CreateBusinessRequest) -> AnyPublisher<CreateBusinessResponse, ResponseError> {
        return provider.requestPublisher(.postCreateBusiness(request: request))
            .map(CreateBusinessResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func putUpdateBusiness(request: CreateBusinessRequest) -> AnyPublisher<CreateBusinessResponse, ResponseError> {
        return provider.requestPublisher(.putUpdateBusiness(request: request))
            .map(CreateBusinessResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func postCreateBusinessHours(request: CreateBusinessHoursRequest) -> AnyPublisher<Response, ResponseError> {
        return provider.requestPublisher(.postCreateBusinessHours(request: request))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func postCover(imageData: Data, businessId: Int) -> AnyPublisher<Response, ResponseError> {
        return provider.requestPublisher(.postCover(imageData: imageData, businessId: businessId))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getServices() -> AnyPublisher<[ServiceResponse], ResponseError> {
        return provider.requestPublisher(.getServices)
            .map([ServiceResponse].self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getActiveServices() -> AnyPublisher<[ServiceResponse], ResponseError> {
        return provider.requestPublisher(.getActiveServices)
            .map([ServiceResponse].self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func upateActiveServices(request: UpdateActiveServicesRequest) -> AnyPublisher<Response, ResponseError> {
        return provider.requestPublisher(.upateActiveServices(request: request))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getBusinessById(id: Int) -> AnyPublisher<UserBusinessResponse, ResponseError> {
        return provider.requestPublisher(.getBusinessById(id: id))
            .map(UserBusinessResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getBusinessPhotos(getPostsRequest: GetPostsRequest) -> AnyPublisher<SearchFeedsResponse, ResponseError> {
        return provider.requestPublisher(.getBusinessPhotos(getPostsRequest: getPostsRequest))
            .map(SearchFeedsResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }
}

extension BusinessesWebRepository {
    enum API {
        case postCreateBusiness(request: CreateBusinessRequest)

        case putUpdateBusiness(request: CreateBusinessRequest)

        case postCreateBusinessHours(request: CreateBusinessHoursRequest)

        case postCover(imageData: Data, businessId: Int)

        case getServices

        case getBusinessById(id: Int)

        case getBusinessPhotos(getPostsRequest: GetPostsRequest)

        case getActiveServices

        case upateActiveServices(request: UpdateActiveServicesRequest)
    }
}

extension BusinessesWebRepository.API: TargetType {
    var baseURL: URL {
        URL(string: ApiConstants.BASE_URL)!
    }

    var path: String {
        switch self {
        case let .getBusinessById(id):
            return "/business/\(id)"
        case let .getBusinessPhotos(getPostsRequest):
            return "/business/\(getPostsRequest.business.id)/photos"
        case .postCreateBusiness:
            return "/business"
        case .putUpdateBusiness:
            return "/business"
        case .postCreateBusinessHours:
            return "/business/hours"
        case .postCover:
            return "/business/cover-photo"
        case .getServices:
            return "/services"
        case .getActiveServices:
            return "/services/active"
        case .upateActiveServices:
            return "/services/active"
        }
    }

    var method: Moya.Method {
        switch self {
        case .postCreateBusiness, .postCreateBusinessHours, .postCover:
            return .post
        case .getServices, .getActiveServices, .getBusinessById, .getBusinessPhotos:
            return .get
        case .upateActiveServices, .putUpdateBusiness:
            return .put
        }
    }

    var task: Task {
        switch self {
        case .getBusinessById:
            return .requestPlain
        case let .getBusinessPhotos(getPostsRequest):
            return .requestParameters(parameters: ["page": getPostsRequest.page], encoding: URLEncoding.default)
        case let .postCreateBusiness(request):
            return .requestJSONEncodable(request)
        case let .postCreateBusinessHours(request):
            return .requestJSONEncodable(request)
        case let .putUpdateBusiness(request):
            return .requestJSONEncodable(request)
        case let .postCover(imageData, businessId):
            let businessIdData = String(businessId).data(using: String.Encoding.utf8)!

            var multipartFormData = [MultipartFormData]()
            multipartFormData.append(MultipartFormData(provider: .data(imageData), name: "file", fileName: "business_cover.jpeg", mimeType: "image/jpeg"))
            multipartFormData.append(MultipartFormData(provider: .data(businessIdData), name: "businessId"))

            return .uploadMultipart(multipartFormData)
        case .getServices:
            return .requestPlain
        case .getActiveServices:
            return .requestPlain
        case let .upateActiveServices(request):
            return .requestJSONEncodable(request)
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return ["Accept": "application/json"]
    }

    var validationType: Moya.ValidationType {
        return .successAndRedirectCodes
    }
}
