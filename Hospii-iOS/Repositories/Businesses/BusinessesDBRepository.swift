//
//  BusinessesDBRepository.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/6/21.
//

import Combine
import CoreData
import CoreStore
import Moya

protocol BusinessesDBDataSource {
    func storeServices(services: [ServiceResponse]) -> AnyPublisher<[ServiceResponse], ResponseError>
    func services() -> AnyPublisher<[ServiceResponse], ResponseError>
}

struct BusinessesDBRepository: BusinessesDBDataSource {
    func store(business: UserBusinessResponse) -> AnyPublisher<Void, ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction in
                _ = try? transaction.importUniqueObject(Into<BusinessMO>(), source: business)
            }
            .mapError { ResponseError.dbError(error: $0) }
            .eraseToAnyPublisher()
    }

    func storeServices(services: [ServiceResponse]) -> AnyPublisher<[ServiceResponse], ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction in
                _ = try? transaction.importUniqueObjects(Into<ServiceMO>(), sourceArray: services)
            }
            .mapError { ResponseError.dbError(error: $0) }
            .map { _ in
                services
            }
            .eraseToAnyPublisher()
    }

    func services() -> AnyPublisher<[ServiceResponse], ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction -> [ServiceMO]? in
                try? transaction.fetchAll(From<ServiceMO>().orderBy(.ascending(\.key)))
            }
            .mapError {
                ResponseError.dbError(error: $0)
            }
            .compactMap { $0 }
            .compactMap {
                $0.map { ServiceResponse.fromCoreData(service: $0) }
            }
            .eraseToAnyPublisher()
    }
}
