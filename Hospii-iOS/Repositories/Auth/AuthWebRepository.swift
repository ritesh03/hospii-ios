import Combine
import Foundation
import Moya

protocol AuthWebDataSource: WebDataSource {
    func postRegister(request: RegisterRequest) -> AnyPublisher<Tokens, MoyaError>

    func postLogin(loginRequest: LoginRequest) -> AnyPublisher<Tokens, MoyaError>

    func postCheckUsernameAvailability(request: CheckUsernameAvilibilityRequest) -> AnyPublisher<CheckUsernameAvilibilityResponse, MoyaError>

    func postRefreshToken(refreshToken: String) -> AnyPublisher<Tokens, MoyaError>

    func postInitPhoneNumberVerification() -> AnyPublisher<Response, MoyaError>

    func postVerifyPhoneNumber(code: String) -> AnyPublisher<Response, MoyaError>

    func putUpdatePassword(request: PutPasswordRequest) -> AnyPublisher<Response, ResponseError>

    func putResetPassword(request: ResetPasswordRequest) -> AnyPublisher<Response, ResponseError>

    func postInitiateForgotPasswordVerificationCode(request: ForgotPasswordRequest) -> AnyPublisher<Response, ResponseError>

    func postCheckForgotPasswordVerificationCode(request: ForgotPasswordRequest) -> AnyPublisher<ForgotPasswordResponse, ResponseError>
}

struct AuthWebRepository: AuthWebDataSource {
    let provider: MoyaProvider<API>

    init(appState: Store<AppState>) {
        provider = MoyaProvider<API>(endpointClosure: { (target: API) -> Endpoint in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer \(appState[\.userData.accessToken] ?? "")"])
        }, plugins: [])
    }

    func postRegister(request: RegisterRequest) -> AnyPublisher<Tokens, MoyaError> {
        return provider.requestPublisher(.postRegister(request: request))
            .map(Tokens.self)
            .eraseToAnyPublisher()
    }

    func postLogin(loginRequest: LoginRequest) -> AnyPublisher<Tokens, MoyaError> {
        return provider.requestPublisher(.postLogin(request: loginRequest))
            .map(Tokens.self)
            .eraseToAnyPublisher()
    }

    func postCheckUsernameAvailability(request: CheckUsernameAvilibilityRequest) -> AnyPublisher<CheckUsernameAvilibilityResponse, MoyaError> {
        return provider.requestPublisher(.postCheckUsernameAvailability(request: request))
            .map(CheckUsernameAvilibilityResponse.self)
            .eraseToAnyPublisher()
    }

    func postRefreshToken(refreshToken: String) -> AnyPublisher<Tokens, MoyaError> {
        return provider.requestPublisher(.postRefreshToken(refreshToken: refreshToken))
            .map(Tokens.self)
            .eraseToAnyPublisher()
    }

    func postInitPhoneNumberVerification() -> AnyPublisher<Response, MoyaError> {
        return provider.requestPublisher(.postInitPhoneNumberVerification)
            .eraseToAnyPublisher()
    }

    func postVerifyPhoneNumber(code: String) -> AnyPublisher<Response, MoyaError> {
        return provider.requestPublisher(.postVerifyPhoneNumber(code: code))
            .eraseToAnyPublisher()
    }

    func putUpdatePassword(request: PutPasswordRequest) -> AnyPublisher<Response, ResponseError> {
        return provider.requestPublisher(.putUpdatePassword(request: request))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func putResetPassword(request: ResetPasswordRequest) -> AnyPublisher<Response, ResponseError> {
        return provider.requestPublisher(.putResetPassword(request: request))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func postInitiateForgotPasswordVerificationCode(request: ForgotPasswordRequest) -> AnyPublisher<Response, ResponseError> {
        return provider.requestPublisher(.postInitiateForgotPasswordVerificationCode(request: request))
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func postCheckForgotPasswordVerificationCode(request: ForgotPasswordRequest) -> AnyPublisher<ForgotPasswordResponse, ResponseError> {
        return provider.requestPublisher(.postCheckForgotPasswordVerificationCode(request: request))
            .map(ForgotPasswordResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }
}

extension AuthWebRepository {
    enum API {
        case postLogin(request: LoginRequest)
        case postRegister(request: RegisterRequest)
        case postCheckUsernameAvailability(request: CheckUsernameAvilibilityRequest)
        case postRefreshToken(refreshToken: String)
        case postInitPhoneNumberVerification
        case postVerifyPhoneNumber(code: String)
        case putUpdatePassword(request: PutPasswordRequest)
        case putResetPassword(request: ResetPasswordRequest)
        case postInitiateForgotPasswordVerificationCode(request: ForgotPasswordRequest)
        case postCheckForgotPasswordVerificationCode(request: ForgotPasswordRequest)
    }
}

extension AuthWebRepository.API: TargetType {
    var baseURL: URL {
        URL(string: ApiConstants.BASE_URL)!
    }

    var path: String {
        switch self {
        case .postLogin:
            return "/auth/tokens"
        case .postRegister:
            return "/auth/register"
        case .postCheckUsernameAvailability:
            return "/auth/check-username-availability"
        case .postRefreshToken:
            return "/auth/refresh-tokens"
        case .postInitPhoneNumberVerification:
            return "/sms/initiate-verification-code"
        case .postVerifyPhoneNumber:
            return "/sms/check-verification-code"
        case .postInitiateForgotPasswordVerificationCode:
            return "/sms/initiate-forgot-password-code"
        case .postCheckForgotPasswordVerificationCode:
            return "/sms/check-forgot-password-code"
        case .putUpdatePassword:
            return "/auth/password"
        case .putResetPassword:
            return "/auth/reset-password"
        }
    }

    var method: Moya.Method {
        switch self {
        case .postLogin, .postCheckUsernameAvailability, .postRegister, .postRefreshToken, .postInitPhoneNumberVerification, .postVerifyPhoneNumber, .postInitiateForgotPasswordVerificationCode, .postCheckForgotPasswordVerificationCode:
            return .post
        case .putUpdatePassword, .putResetPassword:
            return .put
        }
    }

    var task: Task {
        switch self {
        case let .postLogin(request):
            return .requestJSONEncodable(request)
        case let .postCheckUsernameAvailability(request):
            return .requestJSONEncodable(request)
        case let .postRegister(request):
            return .requestJSONEncodable(request)
        case let .postRefreshToken(refreshToken):
            return .requestParameters(parameters: ["refreshToken": refreshToken], encoding: JSONEncoding.default)
        case .postInitPhoneNumberVerification:
            return .requestPlain
        case let .postVerifyPhoneNumber(code):
            return .requestParameters(parameters: ["code": code], encoding: JSONEncoding.default)
        case let .postInitiateForgotPasswordVerificationCode(request):
            return .requestJSONEncodable(request)
        case let .postCheckForgotPasswordVerificationCode(request):
            return .requestJSONEncodable(request)
        case let .putUpdatePassword(request):
            return .requestJSONEncodable(request)
        case let .putResetPassword(request):
            return .requestJSONEncodable(request)
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return ["Accept": "application/json"]
    }

    var validationType: Moya.ValidationType {
        return .successAndRedirectCodes
    }
}

extension AuthWebRepository.API {
    static var sharedProvider: MoyaProvider<AuthWebRepository.API> = {
        let authenticatable = MoyaAuthenticatable<AuthWebRepository.API, Oauth2Authentication>()
        return MoyaProvider<AuthWebRepository.API>(
            // requestClosure: authenticatable.requestClosure
        )
    }()
}
