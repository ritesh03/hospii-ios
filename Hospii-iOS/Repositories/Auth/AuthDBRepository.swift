import Combine
import CoreData

protocol AuthDBDataSource {}

struct AuthDBRepository: AuthDBDataSource {}
