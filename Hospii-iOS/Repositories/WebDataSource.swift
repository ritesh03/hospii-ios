import Combine
import Foundation
import Moya

protocol WebDataSource {
    // We can pass testing flag here.
}

extension WebDataSource {
    func makeProvider<T>(bearer: String) -> MoyaProvider<T> where T: TargetType {
        let endpointClosure = { (target: T) -> Endpoint in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer \(bearer)"])
        }
        return MoyaProvider<T>(endpointClosure: endpointClosure, plugins: [])
    }

    func makeProvider<T>() -> MoyaProvider<T> where T: TargetType {
        return MoyaProvider<T>(plugins: [])
    }
}
