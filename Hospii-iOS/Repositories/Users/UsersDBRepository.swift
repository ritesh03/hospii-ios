//
//  UsersDBRepository.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 11/3/21.
//

import Combine
import CoreData
import CoreStore
import Moya

protocol UsersDBDataSource {
    func store(user: UserResponse) -> AnyPublisher<Void, ResponseError>
    func user() -> AnyPublisher<UserResponse, ResponseError>
    func saveProfilePhoto(media: MediaResponse?) -> AnyPublisher<Void, ResponseError>
}

struct UsersDBRepository: UsersDBDataSource {
    func store(user: UserResponse) -> AnyPublisher<Void, ResponseError> {
        //        return CoreStoreDefaults.dataStack.reactive
        //            .importUniqueObject(Into<User>(), source: user)
        //            .mapError( {
        //                $0 as Error
        //            })
        //            .map { _ in Void() }
        //            .eraseToAnyPublisher()
        //

        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction in
                _ = try? transaction.importUniqueObject(Into<UserMO>(), source: user)
            }
            .mapError { ResponseError.dbError(error: $0) }
            .eraseToAnyPublisher()
    }

    func storeUserBusiness(business: UserBusinessResponse) -> AnyPublisher<Void, ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction in
                _ = try? transaction.importUniqueObject(Into<BusinessMO>(), source: business)
            }
            .mapError { ResponseError.dbError(error: $0) }
            .eraseToAnyPublisher()
    }

    func user() -> AnyPublisher<UserResponse, ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction -> UserMO? in
                try? transaction.fetchOne(From<UserMO>())
            }
            .mapError {
                ResponseError.dbError(error: $0)
            }
            .compactMap { $0 }
            .compactMap {
                UserResponse.fromCoreData(user: $0)
            }
            .eraseToAnyPublisher()
    }

    func saveProfilePhoto(media: MediaResponse?) -> AnyPublisher<Void, ResponseError> {
        return CoreStoreDefaults.dataStack.reactive
            .perform { transaction in
                if let media = media {
                    let user = try? transaction.fetchOne(From<UserMO>())
                    user?.addPhoto(media)
                }
            }
            .mapError { ResponseError.dbError(error: $0) }
            .eraseToAnyPublisher()
    }
}
