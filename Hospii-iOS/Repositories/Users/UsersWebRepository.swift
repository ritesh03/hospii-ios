//
//  UserWebRepository.swift
//  Hospii-iOS
//
//  Created by Eric Cheng on 10/19/21.
//

import Combine
import CoreMedia
import Foundation
import Moya

protocol UsersWebDataSource: WebDataSource {
    func postAvatar(imageData: Data) -> AnyPublisher<MediaResponse, ResponseError>

    func getProfile() -> AnyPublisher<UserResponse, ResponseError>

    func putProfile(request: UpdateProfileRequest) -> AnyPublisher<UserResponse, ResponseError>

    func getPhotos(request: PageRequest) -> AnyPublisher<SearchFeedsResponse, ResponseError>
}

struct UsersWebRepository: UsersWebDataSource {
    let provider: Oauth2MoyaProvider<API>

    init(appState: Store<AppState>) {
        provider = Oauth2MoyaProvider<API>(endpointClosure: { (target: API) -> Endpoint in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer \(appState[\.userData.accessToken] ?? "")"])
        }, plugins: [], appState: appState)
    }

    func postAvatar(imageData: Data) -> AnyPublisher<MediaResponse, ResponseError> {
        return provider.requestPublisher(.postAvatar(imageData: imageData))
            .map(MediaResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getProfile() -> AnyPublisher<UserResponse, ResponseError> {
        return provider.requestPublisher(.getProfile)
            .map(UserResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func putProfile(request: UpdateProfileRequest) -> AnyPublisher<UserResponse, ResponseError> {
        return provider.requestPublisher(.putProfile(request: request))
            .map(UserResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }

    func getPhotos(request: PageRequest) -> AnyPublisher<SearchFeedsResponse, ResponseError> {
        return provider.requestPublisher(.getPhotos(request: request))
            .map(SearchFeedsResponse.self)
            .mapError { ResponseError.networkError(error: $0) }
            .eraseToAnyPublisher()
    }
}

extension UsersWebRepository {
    enum API {
        case postAvatar(imageData: Data)
        case getProfile
        case getPhotos(request: PageRequest)
        case putProfile(request: UpdateProfileRequest)
    }
}

extension UsersWebRepository.API: TargetType {
    var baseURL: URL {
        URL(string: ApiConstants.BASE_URL)!
    }

    var path: String {
        switch self {
        case .postAvatar:
            return "/users/me/avatar"
        case .getProfile:
            return "/users/me"
        case .putProfile:
            return "/users/me"
        case .getPhotos:
            return "/users/me/photos"
        }
    }

    var method: Moya.Method {
        switch self {
        case .postAvatar:
            return .post
        case .getProfile, .getPhotos:
            return .get
        case .putProfile:
            return .put
        }
    }

    var task: Task {
        switch self {
        case let .postAvatar(imageData):
            var multipartFormData = [MultipartFormData]()
            multipartFormData.append(MultipartFormData(provider: .data(imageData), name: "file", fileName: "user_avatar.jpeg", mimeType: "image/jpeg"))
            return .uploadMultipart(multipartFormData)
        case .getProfile:
            return .requestPlain
        case let .putProfile(request):
            return .requestJSONEncodable(request)
        case let .getPhotos(request):
            var parameters: [String: Any] = ["page": request.page]
            if let pageSize = request.pageSize {
                parameters["pageSize"] = pageSize
            }
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return ["Accept": "application/json"]
    }

    var validationType: Moya.ValidationType {
        return .successAndRedirectCodes
    }
}
