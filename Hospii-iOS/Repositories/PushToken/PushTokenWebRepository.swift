import Combine
import Foundation

protocol PushTokenWebDataSource: WebDataSource {}

struct PushTokenWebRepository: PushTokenWebDataSource {}
