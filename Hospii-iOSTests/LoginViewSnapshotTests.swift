//
//  LoginViewSnapshotTests.swift
//  Hospii-iOSTests
//
//  Created by Chesong Lee on 10/29/21.
//

import FBSnapshotTestCase
@testable import Hospii_iOS
import SwiftUI
import XCTest

class LoginViewSnapshotTests: FBSnapshotTestCase {
    override func setUp() {
        super.setUp()
        recordMode = false
        fileNameOptions = [.screenScale, .screenSize]
    }

    func test_login_view() {
        let viewModel = LoginView.ViewModel(container: .defaultValue)
        let view = LoginView(viewModel: viewModel)
        let viewController = UIHostingController(rootView: view)
        viewController.view.frame = CGRect(x: 0, y: 0, width: 320, height: 693)
        viewController.view.drawHierarchy(in: viewController.view.bounds, afterScreenUpdates: true)
        FBSnapshotVerifyView(viewController.view, perPixelTolerance: 0.02, overallTolerance: 0.02)
        FBSnapshotVerifyLayer(viewController.view.layer)
    }
}
